package com.lootsie.sdk.utils.image;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import com.android.volley.toolbox.ImageLoader;
import com.lootsie.sdk.utils.DiskLruCache;
import com.lootsie.sdk.BuildConfig;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import java.io.*;

/**
 * Implementation of DiskLruCache by Jake Wharton
 * modified from http://stackoverflow.com/questions/10185898/using-disklrucache-in-android-4-0-does-not-provide-for-opencache-method
 */
public class DiskLruImageCache implements ImageLoader.ImageCache {

    private DiskLruCache mDiskCache;
    private Bitmap.CompressFormat mCompressFormat = Bitmap.CompressFormat.JPEG;
    private static int IO_BUFFER_SIZE = 8*1024;
    private int mCompressQuality = 70;
    private static final int APP_VERSION = 1;
    private static final int VALUE_COUNT = 1;

    private static final String TAG = "DiskLruImageCache";

    public DiskLruImageCache(Context context,String uniqueName, int diskCacheSize,
                             Bitmap.CompressFormat compressFormat, int quality ) {
        DebugLog("DiskLruImageCache: name: %s", uniqueName);

        try {
            final File diskCacheDir = getDiskCacheDir(context, uniqueName );
            mDiskCache = DiskLruCache.open( diskCacheDir, APP_VERSION, VALUE_COUNT, diskCacheSize );
            mCompressFormat = compressFormat;
            mCompressQuality = quality;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean writeBitmapToFile(Bitmap bitmap, DiskLruCache.Editor editor )
            throws IOException, FileNotFoundException {
        DebugLog("writeBitmapToFile");

        OutputStream out = null;
        try {
            out = new BufferedOutputStream( editor.newOutputStream( 0 ), IO_BUFFER_SIZE );
            return bitmap.compress( mCompressFormat, mCompressQuality, out );
        } finally {
            if ( out != null ) {
                out.close();
            }
        }
    }

    private File getDiskCacheDir(Context context, String uniqueName) {

        final String cachePath = context.getCacheDir().getPath();
        return new File(cachePath + File.separator + uniqueName);
    }

    @Override
    public void putBitmap( String key, Bitmap data ) {
        DebugLog("putBitmap: %s", key);

        DiskLruCache.Editor editor = null;
        String encodedKey = String.valueOf(key.hashCode());
        try {
            editor = mDiskCache.edit( encodedKey );
            if ( editor == null ) {
                return;
            }

            if( writeBitmapToFile( data, editor ) ) {
                mDiskCache.flush();
                editor.commit();
                DebugLog("image put on disk cache " + key);
                
            } else {
                editor.abort();
                DebugLog("ERROR on: image put on disk cache " + key );
                
            }
        } catch (IOException e) {
        	DebugLog("ERROR on: image put on disk cache " + key );
            
            try {
                if ( editor != null ) {
                    editor.abort();
                }
            } catch (IOException ignored) {
            }
        }

    }

    @Override
    public Bitmap getBitmap( String key ) {

        long freeMemoryBytes = Runtime.getRuntime().freeMemory();
        DebugLog("getBitmap: freeMemory: %d bitmap: %s", freeMemoryBytes, key);

        Bitmap bitmap = null;

        if (freeMemoryBytes > LootsieGlobals.MINIMUM_MEMORY_THRESHOLD) {
            DiskLruCache.Snapshot snapshot = null;
            try {
                String encodedKey = String.valueOf(key.hashCode());
                snapshot = mDiskCache.get(encodedKey);
                if (snapshot == null) {
                    return null;
                }
                final InputStream in = snapshot.getInputStream(0);
                if (in != null) {
                    final BufferedInputStream buffIn =
                            new BufferedInputStream(in, IO_BUFFER_SIZE);
                    bitmap = BitmapFactory.decodeStream(buffIn);
                }
            } catch (IOException e) {
                e.printStackTrace();
//        } catch ( OutOfMemoryError ex) {
//            Log.e(TAG, "getBitmap: Error: " + ex.getMessage());
//            ex.printStackTrace();
            } finally {
                if (snapshot != null) {
                    snapshot.close();
                }
            }

            DebugLog(bitmap == null ? "" : "image read from disk " + key);
        } else {
            Log.w(TAG,"getBitmap: WARNING low memory!");
        }

        return bitmap;

    }

    public boolean containsKey( String key ) {

        boolean contained = false;
        DiskLruCache.Snapshot snapshot = null;
        try {
            snapshot = mDiskCache.get( key );
            contained = snapshot != null;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if ( snapshot != null ) {
                snapshot.close();
            }
        }

        return contained;

    }

    public void clearCache() {
        DebugLog("clearCache");

        try {
            mDiskCache.delete();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    public File getCacheFolder() {
        return mDiskCache.getDirectory();
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (com.lootsie.sdk.utils.LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}