package com.lootsie.sdk.callbacks;

import com.lootsie.sdk.utils.LootsieGlobals;

/**
 * Created by jerrylootsie on 7/15/15.
 */
public interface IUpdateUserAccountCallback {
    public void onUpdateUserAccountSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT mesg);
    public void onUpdateUserAccountFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT mesg);
}
