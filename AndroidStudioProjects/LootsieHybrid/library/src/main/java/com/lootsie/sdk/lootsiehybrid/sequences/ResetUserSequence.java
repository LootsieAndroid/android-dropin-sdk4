package com.lootsie.sdk.lootsiehybrid.sequences;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IInitializationCallback;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.callbacks.ISignOutCallback;
import com.lootsie.sdk.device.Device;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.net.Headers;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.netutil.NetworkCommandQueue;
import com.lootsie.sdk.netutil.RESTDeleteUserSessionTask;
import com.lootsie.sdk.netutil.RESTPostUserSessionGuestTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.PreferencesHandler;

public class ResetUserSequence {
    
    private static String appKey = "";
    
    private static LootsieAccountManager lootsieAccountManager = null;

    private static String TAG = "Lootsie ResetUserSequence";

    private static String locationJsonStr = null;

    private static ISignOutCallback callback;

    //public static RESTDeleteUserSessionTask restDeleteUserSessionTask = null;
    public static IGenericAsyncTask<String> restDeleteUserSessionTask = null;

    public ResetUserSequence() {


    }

    /**
     * we initialize the resttasks here, and check because they may already have been initialized as mock objects earlier for testing
     */
    private static void init() {

        if (restDeleteUserSessionTask == null) {
            restDeleteUserSessionTask = new RESTDeleteUserSessionTask();
        }
    }

    /**
     * we need to create a new task every time because a task can only be executed once!
     */
    private static void cleanup() {
        restDeleteUserSessionTask = null;
    }

    // TODO: add resetusercallback types later
    public static void start(String inputAppKey, ISignOutCallback inputCallback) {
        appKey = inputAppKey;

        callback = inputCallback;
        
        lootsieAccountManager = LootsieEngine.getInstance().getLootsieAccountManager();
        
        // signout functionality

        init();

        // make sure Headers.SESSION_TOKEN, Headers.USER_TOKEN, Headers.APP_SECRET are set properly in RestClient
        if ((DataModel.userSessionToken == null) || (DataModel.apiSessionToken == null)) {
            Logs.e(TAG, "ResetUserSequence: signout failure - user session token or api session token is invalid!");
            return;
        }

        IRESTCallback restCallback = new IRESTCallback() {
            @Override
            public void restResult(RestResult result) {
            DebugLog("ResetUserSequence: RESTDeleteUserSessionTask: status: " + result.status + " result: " + result.content);

            // broken call
            // if ((result.status == 200) || (result.status == 204)) {
            if ((result.status == 200) || (result.status == 204) || (result.status == 0)) {
                 // operation happened normally

                // clear out user session token
                PreferencesHandler.clearPref(LootsieGlobals.KEY_USER_SESSION_TOKEN, LootsieEngine.getInstance().getApplicationContext());

                // remove usage from Restclient
                RestClient.removeHeader(Headers.USER_TOKEN);

                //lootsieAccountManager.logout();
                lootsieAccountManager.removeAccount();

                DataModel.user = null;

                // create a new guest user
                //PostUserSessionGuest();

                class InternalInitCallback implements IInitializationCallback {
                    ISignOutCallback nestedCB = null;

                    public InternalInitCallback(ISignOutCallback inputCB) {
                        nestedCB = inputCB;
                    }

                    @Override
                    public void onInitSuccess() {
                        DebugLog("InternalInitCallback: onInitSuccess");

                        if (nestedCB != null) {
                            nestedCB.onSignOutSuccess();
                        }
                    }

                    @Override
                    public void onInitFailure() {
                        DebugLog("InternalInitCallback: onInitFailure");

                        if (nestedCB != null) {
                            nestedCB.onSignOutFailure();
                        }
                    }
                }

                InternalInitCallback initCallback = new InternalInitCallback(callback);
                //LootsieEngine.getInstance().init(LootsieEngine.getInstance().getApp(), LootsieEngine.getInstance().getAppId());
                LootsieEngine.getInstance().forceInit(LootsieEngine.getInstance().getAppId(), initCallback);
            } else {
                callback.onSignOutFailure();
            }

            // may have got 403 if stuff wasn't cleaned out properly last time!

            cleanup();
                
            }
        };

        DebugLog("ResetUserSequence: DeleteUserSession");
        restDeleteUserSessionTask.setCallback(restCallback);
        restDeleteUserSessionTask.executeTask();
        
    }
    
    

//    public static void PostUserSessionGuest() {
//
//        String authToken = PreferencesHandler.getString(LootsieGlobals.KEY_USER_SESSION_TOKEN, LootsieEngine.getInstance().getApplicationContext());
//        if (authToken != null) {
//            DebugLog("ResetUserSequence: cached user session token");
//            DebugLog("ResetUserSequence: user session token: " + authToken);
//
//            DataModel.userSessionToken = authToken;
//            RestClient.addHeader(Headers.USER_TOKEN, authToken);
//
//            //PostUserLocation();
//
//        } else {
//            DebugLog("ResetUserSequence: PostUserSessionGuest");
//
//            IRESTCallback restCallback = new IRESTCallback() {
//                @Override
//                public void restResult(RestResult result) {
//                    ResetUserSequence.PostUserSessionGuestResult(result);
//                }
//            };
//
//            // augment network stats with post user session tag
//            DataModel.updateNetwork(LootsieApi.SIGN_IN_GUEST_URL.toString());
//            new RESTPostUserSessionGuestTask(restCallback).execute();
//        }
//
//    }


//    public static void PostUserSessionGuestResult(RestResult RestResult) {
//        if (RestResult.status == 200) {
//            try {
//                JSONObject json = new JSONObject(RestResult.content);
//                String authToken = json.getString(Headers.USER_TOKEN);
//
//                if (LootsieGlobals.debugLevel > 0)
//                    Log.v(TAG, "ResetUserSequence: PostUserSessionGuestResult: user session token: " + authToken);
//
//                // save user session token somewhere it can be restored from
//                PreferencesHandler.saveString(LootsieGlobals.KEY_USER_SESSION_TOKEN, authToken, LootsieEngine.getInstance().getApplicationContext());
//
//                DataModel.userSessionToken = authToken;
//                RestClient.addHeader(Headers.USER_TOKEN, authToken);
//
//                //PostUserLocation();
//                if (callback != null) callback.onSignOutSuccess();
//                
//            } catch (JSONException ex) {
//                Logs.e(TAG, "ResetUserSequence: PostUserSessionGuestResult: exception: " + ex.getMessage());
//                if (callback != null) callback.onSignOutFailure();
//            }
//        } else {
//            if (callback != null) callback.onSignOutFailure();
//        }
//    }
    
    
    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
           Logs.v(TAG, String.format(msg, args));
        }
    }    
}
