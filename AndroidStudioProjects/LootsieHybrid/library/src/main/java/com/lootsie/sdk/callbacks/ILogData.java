package com.lootsie.sdk.callbacks;

/**
 * ILogData: Enable passing log events back from the SDK to the LEG (Lootsie Enabled Game)
 * @author jerrylootsie
 *
 */
public interface ILogData {
    public void onLogEvent(String data);
}
