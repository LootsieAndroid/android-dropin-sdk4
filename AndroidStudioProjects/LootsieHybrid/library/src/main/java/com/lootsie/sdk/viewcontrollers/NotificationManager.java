package com.lootsie.sdk.viewcontrollers;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Display;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;

import android.os.Handler;

import android.util.DisplayMetrics;


import com.lootsie.sdk.lootsiehybrid.LOOTSIE_NOTIFICATION_CONFIGURATION;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.model.Achievement;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.RUtil;
import com.lootsie.sdk.utils.FontUtil;

import com.lootsie.sdk.viewcontrollers.base.BaseNotificationManager;

import java.util.ArrayList;

/**
 * Created by jerrylootsie on 2/13/15.
 */
public class NotificationManager extends BaseNotificationManager {

    private static String TAG = "Lootsie NotificationManager";

    private static ArrayList<View> mViewQueue = new ArrayList<View>();
    private static ArrayList<ViewGroup> mViewGroupQueue = new ArrayList<ViewGroup>();

    private static ViewGroup activityViewGroup = null;

    private static int mWidth = 0;
    private static int mHeight = 0;

    private static final String ERROR_CONTEXTNOTACTIVITY = "The Context that you passed was not an Activity!";


    public NotificationManager(Context context) {
        this.mContext = context;
    }

    @Override
    public void updateContext(Context context) {
        this.mContext = context;
    }

    @Override
    public void showNotification(String title, String message) {
        DebugLog("showNotification: title: %s message: %s", title, message);

        final View pop = LayoutInflater.from(mContext).inflate(RUtil.getLayoutId(mContext, "com_lootsie_notification"), null);
        FontUtil.applyFont(pop, mContext);

        TextView tv = (TextView) pop.findViewById(RUtil.getId(mContext, "achievementTitle"));
        tv.setText(title);

        tv = (TextView) pop.findViewById(RUtil.getId(mContext, "achievementText"));
        tv.setVisibility(View.VISIBLE);
        tv.setText(message);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tv.getLayoutParams();
        params.setMargins(0, 10, 0, 0); //substitute parameters for left, top, right, bottom
        tv.setLayoutParams(params);

        // from PopupManager
        showPopup(mContext, pop, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DebugLog("showNotification(title,message): showPopup: OnClickListener");

                LootsieEngine.getInstance().getCallback().onLootsieBarExpanded();

                LOOTSIE_NOTIFICATION_CONFIGURATION notifyConfig = LootsieEngine.getInstance().getNotificationConfiguration();
                DebugLog("showNotification(title,message): showPopup: OnClickListener: LOOTSIE_NOTIFICATION_CONFIGURATION: %s", notifyConfig.toString());
                
                if (notifyConfig != LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_customPage) {
	                
	                Intent intent = new Intent(mContext, LootsiePageBorderActivity.class);
	//                Bundle extras = new Bundle();
	//                extras.putString("LOOTSIE_NOTIFICATION_CONFIGURATION",notifyConfig.toString());
	//                intent.putExtras(extras);
	        		//intent.putExtra("LOOTSIE_NOTIFICATION_CONFIGURATION",notifyConfig);                
	                intent.putExtra("LOOTSIE_NOTIFICATION_CONFIGURATION",notifyConfig.toString());
	                
	                
	                mContext.startActivity(intent);
                }
                
            }
        });

    }


    @Override
    public void showNotification(Achievement achievement, int lpEarned) {
        DebugLog("showNotification: achievement: %s", achievement.name);

        final View pop = LayoutInflater.from(mContext).inflate(RUtil.getLayoutId(mContext, "com_lootsie_notification"), null);
        FontUtil.applyFont(pop, mContext);


        ImageView ianLeftImage = (ImageView) pop.findViewById(RUtil.getId(mContext, "ianLeftImage"));
        if (ianLeftImage != null) {
            ianLeftImage.setBackgroundColor(DataModel.app.background_color);
        } else {
            Logs.e(TAG, "showNotification: ianLeftImage not found!");
        }

        TextView tv = (TextView) pop.findViewById(RUtil.getId(mContext, "pointsText"));
        if (tv != null) {
            String lpEarnedStr = String.valueOf(lpEarned);
            tv.setText(lpEarnedStr);
            tv.setTextColor(DataModel.app.text_color);
        } else {
            Logs.e(TAG, "showNotification: ian pointsText not found!");
        }


        if (LootsieGlobals.debugLevel > 0) {
            DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
            switch(metrics.densityDpi){
                case DisplayMetrics.DENSITY_LOW:
                    Logs.v(TAG, "showNotification: low density screen");
                    break;
                case DisplayMetrics.DENSITY_MEDIUM:
                    Logs.v(TAG, "showNotification: medium density screen");
                    break;
                case DisplayMetrics.DENSITY_HIGH:
                    Logs.v(TAG, "showNotification: high density screen");
                    break;
            }

            Configuration config = Resources.getSystem().getConfiguration();
            if((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                    Configuration.SCREENLAYOUT_SIZE_XLARGE)
            {
                Logs.v(TAG, "showNotification: screen layout size XLARGE");
            } else if((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                    Configuration.SCREENLAYOUT_SIZE_LARGE)
            {
                Logs.v(TAG, "showNotification: screen layout size LARGE");
            } else if((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                    Configuration.SCREENLAYOUT_SIZE_NORMAL)
            {
                Logs.v(TAG, "showNotification: screen layout size NORMAL");
            } else if((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                    Configuration.SCREENLAYOUT_SIZE_SMALL)
            {
                Logs.v(TAG, "showNotification: screen layout size SMALL");
            }
        }



//        if (Globals.DEBUG_LEVEL > 1) Logs.v(TAG, "SubmitScoreTask: lootsie_notification_text_size:" + mContext.getResources().getDimension(RUtil.getDimenId(mContext, "lootsie_notification_text_size")));

//        if (Globals.DEBUG_LEVEL > 1) Logs.v(TAG, "SubmitScoreTask: achievement_reached_title_size:" + mContext.getResources().getDimension(RUtil.getDimenId(mContext, "achievement_reached_title_size")));


        Resources res = mContext.getResources();

        tv = (TextView) pop.findViewById(RUtil.getId(mContext, "pointsType"));
        if (tv != null) {
            tv.setTextColor(DataModel.app.text_color);
        } else {
            Logs.e(TAG, "showNotification: ian pointsType not found!");
        }

        tv = (TextView) pop.findViewById(RUtil.getId(mContext, "achievementTitle"));
        if (tv != null) {
            tv.setTextColor(DataModel.app.text_color);
            tv.setText(achievement.name);
        } else {
            Logs.e(TAG, "showNotification: ian achievementTitle not found!");
        }

        tv = (TextView) pop.findViewById(RUtil.getId(mContext, "achievementText"));
        if (tv != null) {
            tv.setVisibility(View.VISIBLE);
            tv.setText(achievement.description);
            tv.setTextColor(DataModel.app.text_color);

//        if (Globals.DEBUG_LEVEL > 0) Logs.v(TAG, "SubmitScoreTask: achievement_reached_text_size:" + mContext.getResources().getDimension(RUtil.getDimenId(mContext, "achievement_reached_text_size")));

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tv.getLayoutParams();
            params.setMargins(0, 10, 0, 0); //substitute parameters for left, top, right, bottom
            tv.setLayoutParams(params);
        } else {
            Logs.e(TAG, "showNotification: ian text not found!");
        }



        // from PopupManager
        showPopup(mContext, pop, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DebugLog("showNotification: showPopup: OnClickListener");

                // metrics: AchievementReachedClicked
//                if (PreferencesHandler.getBool(Constants.SETTINGS_FILE_APPLICATION, Constants.SDK_TRACKING_ENABLED, Lootsie.getInstance().getContext())) {
//                    SDK_TrackingAsyncTask task = new SDK_TrackingAsyncTask();
//                    task.execute("AchievementReachedClicked");
//                }

                LootsieEngine.getInstance().getCallback().onLootsieBarExpanded();
                
                LOOTSIE_NOTIFICATION_CONFIGURATION notifyConfig = LootsieEngine.getInstance().getNotificationConfiguration();
                DebugLog("showNotification: showPopup: OnClickListener: LOOTSIE_NOTIFICATION_CONFIGURATION: %s", notifyConfig.toString());
                
                if (notifyConfig != LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_customPage) {
                
                	Activity a = (Activity) LootsieEngine.getInstance().getActivityContext();
                	LootsieEngine.showRewardsPage(a);
                }

            }
        });

        //return true;

    }


    private static void removePopup() {

        try {
            DebugLog("removePopup: mViewQueue: %d mViewGroupQueue: %d", mViewQueue.size(), mViewGroupQueue.size());

            if (mViewQueue.size() > 0) {
                if (mViewGroupQueue.size() > 0) {

                    ViewGroup tempViewGroup = mViewGroupQueue.get(0);
                    mViewGroupQueue.remove(0);

                    View currentView = mViewQueue.get(0);
                    mViewQueue.remove(0);
                    if ((currentView != null) && (currentView.isShown())) {
                        //activityViewGroup.removeView(currentView);
                        tempViewGroup.removeView(currentView);
                    }

                    activityViewGroup.removeView(tempViewGroup);
                }
            }
        } catch (Exception e) {
            Logs.e(TAG, "PopupViewManager: removePopup: removeView: exception: " + e.getMessage());
            e.printStackTrace();
        }


    }


    @SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static void getDisplayDimensions_HoneyComb(Display display) {
        Rect displaySizeRect = new Rect();
        display.getRectSize(displaySizeRect);

        mWidth = displaySizeRect.width();
        mHeight = displaySizeRect.height();
    }


    private static void showPopup(final Context context, final View pop, final View.OnClickListener l) {
        //check context
        if (context == null) {
            Logs.e(TAG, "showPopup: invalid context!");
            return;
        }
        if (!(context instanceof Activity)) {
            throw new IllegalArgumentException(ERROR_CONTEXTNOTACTIVITY);
        }

        DebugLog("showPopup");

        // lookup resource from string
        // RUtil.getLayoutId -> getResourceId -> context.getResources().getIdentifier
        // lookup string from resourceid?
//        ((LootsieImpl) Lootsie.getInstance()).getCallback().onNotification("APPEAR:" +
//                context.getResources().getResourceEntryName(pop.getId()));

        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getDisplayDimensions_HoneyComb(display);
        } else {
            mWidth = display.getWidth();
            mHeight = display.getHeight();
        }

//        final int mWidth = (int) context.getResources().getDimension(RUtil.getDimenId(context, "notification_width"));
//        int height = (int) context.getResources().getDimension(RUtil.getDimenId(context, "notification_height"));
//        final ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams((int) mWidth, pop.getHeight());

        //View rootView = ((Activity) context).getParent().getWindow().getDecorView().findViewById(android.R.id.content);
        //activityViewGroup = (ViewGroup) ((ViewGroup) ((Activity) context).findViewById(android.R.id.content)).getChildAt(0);
        activityViewGroup = (ViewGroup) ((Activity) context).findViewById(android.R.id.content);
//        activityViewGroup = (ViewGroup) ((Activity) context).findViewById(R.id.content);


        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, "NotificationManager: showPopup: activityViewGroup: " + activityViewGroup.toString());
            Logs.v(TAG, "NotificationManager: showPopup: activity: " + ((Activity) context).getTitle());
            Logs.v(TAG, "NotificationManager: showPopup: width: " + mWidth + " height: " + mHeight);

            if (activityViewGroup instanceof RelativeLayout) {
                Logs.v(TAG, "NotificationManager: showPopup: activity: instanceof RelativeLayout");
            } else if (activityViewGroup instanceof android.widget.TableLayout) {
                Logs.v(TAG, "NotificationManager: showPopup: activity: instanceof TableLayout");
            } else if (activityViewGroup instanceof android.widget.LinearLayout) {
                Logs.v(TAG, "NotificationManager: showPopup: activity: instanceof LinearLayout");
            } else if (activityViewGroup instanceof android.widget.FrameLayout) {
                Logs.v(TAG, "NotificationManager: showPopup: activity: instanceof FrameLayout");
            }
        }


        final Animation animDisappear = AnimationUtils.loadAnimation(context, RUtil.getAnim(context, "com_lootsie_slide_out_top"));
        animDisappear.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                DebugLog("animationListener: onAnimationStart");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                DebugLog("animationListener: onAnimationEnd");

                removePopup();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                DebugLog("animationListener: onAnimationRepeat");
            }
        });

        // if there is another popup there presently, remove it!
        if (mViewQueue.size() > 0) {
            ((Activity) context).runOnUiThread(
                    new Runnable() {
                        public void run() {
                            DebugLog("NotificationManager: removeView:");
                            removePopup();
                        }
                    });
        }


        try {
            ((Activity) context).runOnUiThread(
                    new Runnable() {
                        public void run() {
                            //stuff that updates ui
                            try {
                                DebugLog("showPopup: addView: mWidth: %d", mWidth);

                                // activityViewGroup is a framelayout
                                // all children of framelayout are pegged to the top left corner :(
                                // so I make sure lootsie_notification is inside a relativelayout which expands to fill width
                                RelativeLayout viewGroupRelativeLayout = new RelativeLayout(context);
                                RelativeLayout.LayoutParams viewGroupRelLayLP = new RelativeLayout.LayoutParams(
//                                        (mWidth < 0) ? LayoutParams.MATCH_PARENT : mWidth,
                                        //LayoutParams.MATCH_PARENT,
                                        mWidth,
                                        LayoutParams.WRAP_CONTENT);
                                viewGroupRelLayLP.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
//                                viewGroupRelLayLP.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
                                viewGroupRelLayLP.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                                viewGroupRelLayLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                                viewGroupRelativeLayout.setLayoutParams(viewGroupRelLayLP);

//                                viewGroupRelativeLayout.setBackgroundColor(Color.GREEN);  // for testing

                                RelativeLayout.LayoutParams popLP = new RelativeLayout.LayoutParams(
//                                        (mWidth < 0) ? LayoutParams.MATCH_PARENT : mWidth,
                                        //LayoutParams.MATCH_PARENT,
                                        mWidth,
                                        LayoutParams.WRAP_CONTENT);
                                popLP.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
//                                popLP.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
                                //popLP.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.FALSE);
                                popLP.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                                popLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

                                pop.setLayoutParams(popLP);
                                pop.setMinimumWidth(mWidth);  // actually does something, but messes up alignment!
//                                pop.setBackgroundColor(Color.RED); // for testing

//                                viewGroupRelativeLayout.addView(pop, popLP);
                                viewGroupRelativeLayout.addView(pop);


                                activityViewGroup.addView(viewGroupRelativeLayout, viewGroupRelLayLP);

                                mViewGroupQueue.add(viewGroupRelativeLayout);
                                mViewQueue.add(pop);

                                final Animation animAppear = AnimationUtils.loadAnimation(context, RUtil.getAnim(context, "com_lootsie_bounce"));
                                pop.startAnimation(animAppear);

                            } catch (Exception e) {
                                Logs.e(TAG, "NotificationManager: showPopup: addView: exception: " + e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    });



        } catch (Exception e) {
            Logs.e(TAG, "NotificationManager: showPopup: addView: exception: " + e.getMessage());
            e.printStackTrace();
        }

        final int delay = 3000; // milliseconds
        //delay = PreferencesHandler.getInt("application", Constants.SDK_UI_ACHIEVEMENTS_HIDE_DELAY, 10000, context);
        final Handler h = new Handler();
        final Runnable animDisappearCallback = new Runnable() {
            @Override
            public void run() {

                ((Activity) context).runOnUiThread(
                        new Runnable() {
                            public void run() {
                                if (pop.getParent() != null)
                                    pop.startAnimation(animDisappear);
                            }
                        });
            }
        };


        h.postDelayed(animDisappearCallback, delay);

        try {
            pop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (LootsieGlobals.debugLevel > 0) {
                        Logs.v(TAG, "NotificationManager: showPopup: onClick");
                    }

                    if (l != null) {
                        l.onClick(v);
                    }

                    h.removeCallbacks(animDisappearCallback);
                    if (pop.getParent() != null) {
                        pop.startAnimation(animDisappear);
                    }
                }
            });
        } catch (Exception e) {
            Logs.e(TAG, "NotificationManager: showPopup: setOnClickListener: exception: " + e.getMessage());
            e.printStackTrace();
        }

    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}
