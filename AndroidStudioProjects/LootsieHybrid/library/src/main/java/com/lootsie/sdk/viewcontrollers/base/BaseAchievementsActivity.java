package com.lootsie.sdk.viewcontrollers.base;

import com.lootsie.sdk.model.Achievement;
import com.lootsie.sdk.model.AchievementListEntryModel;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jerrylootsie on 3/2/15.
 */
public class BaseAchievementsActivity extends UpdatableListActivity {

    private static String TAG = "Lootsie BaseAchievementsActivity";

    protected List<AchievementListEntryModel> getModel() {
        List<AchievementListEntryModel> list = new ArrayList<AchievementListEntryModel>();


        if (DataModel.app.achievements == null) {
            Logs.e(TAG, "AchievementsActivity: app achievements is null!");
            return list;
        }

        if (DataModel.app.achievements != null) {
        	DebugLog("getModel achievments: " + DataModel.app.achievements.size());
        } else {
        	DebugLog("getModel achievments: is null!");
        }
        	

        // create a hash lookup table based on achievement id for lookup later
        HashMap<String, AchievementListEntryModel> achievementHash = new HashMap<String, AchievementListEntryModel>();

        if (DataModel.app.achievements != null) {
	        for (int i = 0; i< DataModel.app.achievements.size(); i++) {
	            Achievement achievement = DataModel.app.achievements.get(i);
	            AchievementListEntryModel achievementListEntry = new AchievementListEntryModel(achievement);
	            list.add(achievementListEntry);
	
	            achievementHash.put(achievement.id, achievementListEntry);
	        }
        }
        
        // TODO: dereference user achievements against app achievements to indicate successful achievements by the user
        if (DataModel.user.achievements != null) {
	        for (int userIndex = 0; userIndex < DataModel.user.achievements.size(); userIndex++) {
	            Achievement userAchievement = DataModel.user.achievements.get(userIndex);
	
	            if (achievementHash.containsKey(userAchievement.id)) {
	                AchievementListEntryModel achievementListEntry = achievementHash.get(userAchievement.id);
	                achievementListEntry.setAchievementReached(true);
	            }
	        }
        }
        
        // Initially select one of the items
        list.get(1).setSelected(true);
        return list;
    }

    /**
     * a convenience function for the main Lootsie class to update activities when changes occur ]
     * see: Lootsie : setUpdatableListActivity, updateCurrentListActivity,
     *      UpdatableListActivity : updateActivityFromBgThread -> updateActivity
     */
    @Override
    public void updateActivity() {
    	DebugLog("updateActivity:");
    }
    
    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }        
}
