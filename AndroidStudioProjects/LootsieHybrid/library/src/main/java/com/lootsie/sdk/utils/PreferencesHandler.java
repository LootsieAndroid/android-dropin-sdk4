package com.lootsie.sdk.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * PreferencesHandler
 *
 * @author Aliaksandr Salnikau
 */
public class PreferencesHandler {
    public static void saveString(String key, String value, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(key, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void saveString(String file, String key, String value, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(file, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getString(String key, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(key, android.content.Context.MODE_PRIVATE);
        return settings.getString(key, null);
    }

    public static String getString(String file, String key, Context ctx) {
        return getString(file, key, null, ctx);
    }

    public static String getString(String file, String key, String defaultValue, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(file, android.content.Context.MODE_PRIVATE);
        return settings.getString(key, defaultValue);
    }

    public static void saveBool(String key, boolean value, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(key, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void saveBool(String file, String key, boolean value, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(file, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBool(String key, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(key, android.content.Context.MODE_PRIVATE);
        return settings.getBoolean(key, false);
    }

    public static boolean getBool(String file, String key, Context ctx) {
        return getBool(file, key, false, ctx);
    }

    public static boolean getBool(String file, String key, boolean defaultValue, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(file, android.content.Context.MODE_PRIVATE);
        return settings.getBoolean(key, defaultValue);
    }

    public static void saveInt(String file, String key, int value, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(file, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void saveInt(String key, int value, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(key, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getInt(String key, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(key, android.content.Context.MODE_PRIVATE);
        return settings.getInt(key, 0);
    }

    public static int getInt(String file, String key, Context ctx) {
        return getInt(file, key, 0, ctx);
    }

    public static int getInt(String file, String key, int defaultValue, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(file, android.content.Context.MODE_PRIVATE);
        return settings.getInt(key, defaultValue);
    }

    public static void saveLong(String key, long value, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(key, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);

        editor.apply();
        editor.commit();
    }

    public static long getLong(String key, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(key, android.content.Context.MODE_PRIVATE);
        return settings.getLong(key, 0);
    }

    public static void clearPref(String key, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(key, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.commit();
    }

    public static void clearPref(String file, String key, Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(file, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.commit();
    }
}
