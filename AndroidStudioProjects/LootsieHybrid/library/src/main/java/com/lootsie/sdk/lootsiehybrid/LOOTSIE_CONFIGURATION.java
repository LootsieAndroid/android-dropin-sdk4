package com.lootsie.sdk.lootsiehybrid;

import android.annotation.SuppressLint;

/**
 * Created by jerrylootsie on 3/6/15.
 */
public enum LOOTSIE_CONFIGURATION {
    DEFAULT,
    ENABLE_IN_GAME_HOOK_WINDOW,
    DISABLE_IN_GAME_HOOK_WINDOW,
    DISABLE_CACHE,
    ENABLE_CACHE;
    //; is required here.

    @SuppressLint("DefaultLocale")
    @Override public String toString() {
        //only capitalize the first letter
        String s = super.toString();
        return s.substring(0, 1) + s.substring(1).toLowerCase();
    }
}