package com.lootsie.sdk.viewcontrollers.base;

import android.content.Context;

import com.lootsie.sdk.model.Achievement;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;


/**
 * Created by jerrylootsie on 3/2/15.
 */
public class BaseNotificationManager {

    private static String TAG = "Lootsie BaseNotificationManager";

    protected Context mContext;

    public BaseNotificationManager() {

    }

    public BaseNotificationManager(Context context) {
        this.mContext = context;
    }

    public void updateContext(Context context) {
        this.mContext = context;
    }
    public void showNotification(Achievement achievement, int lpEarned) {
        DebugLog("showNotification");
    }

    public void showNotification(String title, String message) {
    	DebugLog("showNotification:");

    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }    
}
