package com.lootsie.sdk.callbacks;

/**
 * Created by jerrylootsie on 7/9/15.
 */
public interface IGenericAsyncTask<Params> {

    public void setCallback(IRESTCallback restCallback);
    public void executeTask(Params... params);
}
