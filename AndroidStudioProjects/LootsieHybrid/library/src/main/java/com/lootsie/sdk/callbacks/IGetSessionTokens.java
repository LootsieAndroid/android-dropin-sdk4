package com.lootsie.sdk.callbacks;

/**
 * Created by jerrylootsie on 2/9/15.
 */
//ASDK-81 Android SDK Call to give developer session tokens.
public interface IGetSessionTokens {

    /**
     *  Lootsie will call back this developer implemented method when Lootsie
     *  failed to get session tokens
     */
    public void onLootsieGetSessionTokensFailed();

    /**
     * Lootsie will call back this developer implemented method when Lootsie
     * successfully got the session tokens.
     *
     * @param jsonStr
     */
    public void onLootsieGetSessionTokensSucess(String jsonStr);
}
