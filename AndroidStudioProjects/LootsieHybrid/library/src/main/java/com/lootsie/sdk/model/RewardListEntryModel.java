package com.lootsie.sdk.model;

/**
 * Created by jerrylootsie on 2/19/15.
 */
public class RewardListEntryModel {

    private boolean selected;
    private String name;
    private Reward reward;

    public boolean detailsVisible = false;
    public boolean redeemVisible = false;

    public RewardViewHolder rewardViewHolder = null;

    public RewardListEntryModel(Reward inputReward) {
        this.reward = inputReward;
        this.name = reward.name;

        selected = false;
    }

    public RewardListEntryModel(String name) {
        this.name = name;
        selected = false;
    }

    public Reward getReward() {
        return reward;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}