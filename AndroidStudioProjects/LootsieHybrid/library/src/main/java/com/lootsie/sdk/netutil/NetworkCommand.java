package com.lootsie.sdk.netutil;

import android.annotation.SuppressLint;

import com.lootsie.sdk.callbacks.IAchievementReached;

/**
 * Created by jerrylootsie on 3/6/15.
 * used for queuing network commands to the Lootsie API servers
 */
public class NetworkCommand {

    public enum LOOTSIE_COMMAND_TYPE {
        ACHIEVEMENT_REACHED,
        SESSION_METRIC;
        @SuppressLint("DefaultLocale")
        @Override public String toString() {
            //only capitalize the first letter
            String s = super.toString();
            return s.substring(0, 1) + s.substring(1).toLowerCase();
        }
    }

    public LOOTSIE_COMMAND_TYPE commandType;

    public String achievementId = null;
    public IAchievementReached callback = null;



    public NetworkCommand(LOOTSIE_COMMAND_TYPE inputCmdType, String inputAchievementId, IAchievementReached inputCallback) {

        this.commandType = inputCmdType;
        this.achievementId = inputAchievementId;
        this.callback = inputCallback;

    }

}
