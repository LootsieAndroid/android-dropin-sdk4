package com.lootsie.sdk.netutil;

import android.os.AsyncTask;
import android.util.Log;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import java.util.Map;

/**
 * Created by jerrylootsie on 2/10/15.
 */

/*
    GET https://api-v2.lootsie.com/v2/settings
    &sdk_version=v2
    &platform=Unity
    &device=deviceModelStr
    &firmware=operatingSystemStr
    &language=languageStr
    &country=regionNameStr
    X-Lootsie-App-Secret
    X-Lootsie-API-Session-Token
    ** note: receiving sdk.enabled flag = false can disable the SDK
 */
public class RESTGetSettingsTask extends AsyncTask< Map<String, Object>, Void, RestResult> implements IGenericAsyncTask<Map<String, Object>> {

    private final static String TAG = "Lootsie.RESTGetSettingsTask";

    IRESTCallback callback = null;

    public RESTGetSettingsTask() {

    }

    public RESTGetSettingsTask(IRESTCallback restCallback) {
        callback = restCallback;
    }

    private RestResult processError(Exception e) {
        RestResult response = new RestResult();
        String errorResponse = e.getMessage();
        if (errorResponse == null) {
            e.printStackTrace();
        } else {
//            response.status = e.code;
            Logs.e(TAG, "RESTGetSettingsTask: processError: getMessage: " + e.getMessage());
        }
        return response;
    }

    @Override
    protected RestResult doInBackground( Map<String, Object> ... params) {

        if (params == null || params.length == 0) {
            return null;
        }

        RestResult response = new RestResult();
        try {
            // pull stuff from Device.java
            Map<String, Object> map = params[0];

            response.content = (String) RestClient.doGet(LootsieApi.SETTINGS_URL.getUri(), map);
            response.status = 200;

            if (LootsieGlobals.debugLevel > 0) Logs.v(TAG, "RESTGetSettingsTask:" + response.content);

        } catch (Exception e) {
            Logs.e(TAG, "RESTGetSettingsTask: processError: getMessage: " + e.getMessage());
        }
        return response;
    }



    @Override
    protected void onPostExecute(final RestResult restResult) {
        if (callback != null) {
            callback.restResult(restResult);
        }
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(Map<String, Object>... params) {
        execute(params);
    }
}