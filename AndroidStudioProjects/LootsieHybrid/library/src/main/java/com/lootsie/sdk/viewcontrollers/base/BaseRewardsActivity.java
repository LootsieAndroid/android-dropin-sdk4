package com.lootsie.sdk.viewcontrollers.base;

import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.model.RewardListEntryModel;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jerrylootsie on 3/2/15.
 */
public class BaseRewardsActivity extends UpdatableListActivity {

    private static String TAG = "Lootsie BaseRewardsActivity";

    /**
     * Get the rewards list from the DataModel - retrieved from Lootsie API servers
     * @return
     */
    protected List<RewardListEntryModel> getModel() {
        List<RewardListEntryModel> list = new ArrayList<RewardListEntryModel>();

        if (DataModel.userRewards == null) {
            Logs.e(TAG, "RewardsActivity: userRewards is null!");
            return list;
        }

        DebugLog("getModel Rewards: " + DataModel.userRewards.rewards.size());

        for (int i = 0; i< DataModel.userRewards.rewards.size(); i++) {
            Reward reward = DataModel.userRewards.rewards.get(i);
            list.add(new RewardListEntryModel(reward));
        }

        // Initially select one of the items
        list.get(1).setSelected(true);
        return list;
    }

    protected RewardListEntryModel get(String s) {
        return new RewardListEntryModel(s);
    }

    /**
     * a convenience function for the main Lootsie class to update activities when changes occur ]
     * see: Lootsie : setUpdatableListActivity, updateCurrentListActivity,
     *      UpdatableListActivity : updateActivityFromBgThread -> updateActivity
     */
    @Override
    public void updateActivity() {
        DebugLog("updateActivity:");
    }
    
    
    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }     
}
