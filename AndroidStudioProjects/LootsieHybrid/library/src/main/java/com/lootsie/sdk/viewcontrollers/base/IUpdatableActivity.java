package com.lootsie.sdk.viewcontrollers.base;

/**
 * Created by jerrylootsie on 3/2/15.
 */
public interface IUpdatableActivity {

    public void setUpdatable(UpdatableActivity activity);
    public void updateCurrentActivity();

}
