package com.lootsie.sdk.lootsiehybrid.sequences;


import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.netutil.RESTGetUserAccountTask;
import com.lootsie.sdk.netutil.RESTPostUserSessionEmailOnlyTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.callbacks.IMergePointsCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jerrylootsie on 5/12/15.
 */
public class MergePointsSequence {

    private static final String TAG = "Lootsie MergePointsSequence";

    private static LootsieAccountManager lootsieAccountManager = null;

    private static IMergePointsCallback callback = null;

    //public static RESTPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = null;
    public static IGenericAsyncTask<String> restPostUserSessionEmailOnlyTask = null;

    //public static RESTGetUserAccountTask restGetUserAccountTask = null;
    public static IGenericAsyncTask<String> restGetUserAccountTask = null;

    /**
     * we initialize the resttasks here, and check because they may already have been initialized as mock objects earlier for testing
     */
    private static void init() {

        if (restPostUserSessionEmailOnlyTask == null) {
            restPostUserSessionEmailOnlyTask = new RESTPostUserSessionEmailOnlyTask();
        }

        if (restGetUserAccountTask == null) {
            restGetUserAccountTask = new RESTGetUserAccountTask();
        }

    }

    /**
     * we need to create a new task every time because a task can only be executed once!
     */
    private static void cleanup() {
        restPostUserSessionEmailOnlyTask = null;
        restGetUserAccountTask = null;
    }

    public static void start(String emailStr, IMergePointsCallback inputCB) {

        DebugLog("MergePointsSequence: start - email: " + emailStr);

        callback = inputCB;
        
        lootsieAccountManager = LootsieEngine.getInstance().getLootsieAccountManager();

        init();

        // TODO: ignore checks since I just want to test callback return values for now
        //if (DataModel.user.isGuest) {
        // check validity of user as well as guest check!
        if ((!DataModel.user.isGuest) && (DataModel.user.email != null)) {
        	if (callback != null) callback.onMergePointsFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.MERGE_POINTS_SEQUENCE_FAILURE_ALREADY_REGISTERED);
            cleanup();
        } else {
        	registerUser(emailStr);
        }


    }

    public static void registerUser(String emailStr) {

        /*
        POST https://api-v2.lootsie.com/v2/user/session/emailonly
        X-Lootsie-App-Secret
        X-Lootsie-User-Session-Token
        {“email”,”testuser@domain.com”}
        */

        if (isEmailValid(emailStr)) {

            DataModel.user.email = emailStr;

            String jsonStr = "{\"email\": \"" + emailStr + "\"}";

            IRESTCallback restCallback = new IRESTCallback() {
                @Override
                public void restResult(RestResult result) {
                    MergePointsSequence.PostUserSessionEmailOnlyResult(result);
                }
            };

            restPostUserSessionEmailOnlyTask.setCallback(restCallback);
            restPostUserSessionEmailOnlyTask.executeTask(jsonStr);
        } else {
            // ASDK-402 Android Hybrid: No error message when trying to redeem to an invalid email address.
            // don't bother sending it to the server, we know the email is wrong.
        	if (callback != null) callback.onMergePointsFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.MERGE_POINTS_SEQUENCE_FAILURE_INVALID_EMAIL);
            cleanup();
        }

    }

    public static void PostUserSessionEmailOnlyResult(RestResult result) {
        //if ((result.status == 200) || (result.status == 204) || (result.status == 409)) {
    	if ((result.status == 200) || (result.status == 204)) {

            if (result.content != null) {
                DebugLog("MergePointsSequence: PostUserSessionEmailOnlyResult: " + result.content);
            } else {
                DebugLog("MergePointsSequence: PostUserSessionEmailOnlyResult: null response");
            }

            // save the updated user email info
            LootsieEngine.getInstance().getLootsieAccountManager().saveUserInfo(DataModel.user);

            MergePointsSequence.GetUserAccount();

        } else {
            // responseCode: 409
            // Note: When making this call with a session token belonging to a registered user, it will return 409
            if (result.content != null) {
                DebugLog("MergePointsSequence: PostUserSessionEmailOnlyResult[" + result.status + "]: " + result.content);

            	// {"errors": [{"field": "achievement_ids", "message": "LP maxed"}]}
            	// try to parse results
            	if (result.content != null) {
            		try {
            			JSONObject jobj = new JSONObject(result.content);
            			JSONArray errorsArr = jobj.getJSONArray("errors");
            			JSONObject errObj = errorsArr.getJSONObject(0);
            			String mesg = errObj.getString("message");
            			
            			if (mesg.equalsIgnoreCase("LP maxed")) {
            				//mesg = "Sorry; you have reached the periodic limit. Come back tomorrow!";
            				if (callback != null) callback.onMergePointsFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.MERGE_POINTS_SEQUENCE_FAILURE_LP_MAXED);
            			} else {
                            // mesg = "Register Email Failure: \n" + mesg;
            				if (callback != null) callback.onMergePointsFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.MERGE_POINTS_SEQUENCE_FAILURE_REGISTER_EMAIL);
            			}
            			
            			
            			
            		} catch (JSONException ex) {
            			Logs.e(TAG, "MergePointsSequence: PostUserSessionEmailOnlyResult: jsonexception: " + ex.getMessage());

                        // mesg = "Register Email Failure: \n" + mesg;
            			if (callback != null) callback.onMergePointsFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.MERGE_POINTS_SEQUENCE_FAILURE_REGISTER_EMAIL);
                        cleanup();
            		}
            		
            	} else {
            	
            		// ASDK-402 Android Hybrid: No error message when trying to redeem to an invalid email address.
            		if (callback != null) callback.onMergePointsFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.MERGE_POINTS_SEQUENCE_FAILURE_REGISTER_EMAIL);
                    cleanup();
            	}
            } else {
                DebugLog("MergePointsSequence: PostUserSessionEmailOnlyResult[" + result.status + "]: null response");

                // ASDK-402 Android Hybrid: No error message when trying to redeem to an invalid email address.
                if (callback != null) callback.onMergePointsFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.MERGE_POINTS_SEQUENCE_FAILURE_REGISTER_EMAIL);
                cleanup();
            }

        }
    }

    static Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    private static boolean isEmailValid(String email) {
        Matcher m = emailPattern.matcher(email);
        return m.matches();
    }

    public static void GetUserAccount() {

        DebugLog("MergePointsSequence: GetUserAccount");

        User user = null;
//        if (!DataModel.updateNetwork(LootsieApi.USER_ACCOUNT_URL.toString())) {
        // use account manager to save and restore data -possibly from another app
//        user = lootsieAccountManager.getUser();
//        }



        IRESTCallback restCallback = new IRESTCallback() {
            @Override
            public void restResult(RestResult result) {
            MergePointsSequence.GetUserAccountResult(result);
            }
        };

        restGetUserAccountTask.setCallback(restCallback);
        restGetUserAccountTask.executeTask("test");



    }

    public static void GetUserAccountResult(RestResult RestResult) {
        if (RestResult.status == 200) {
        	// "Register Email Success"
        	if (callback != null) callback.onMergePointsSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.MERGE_POINTS_SEQUENCE_SUCCESS);
        	 
            try {
                JSONObject json = new JSONObject(RestResult.content);

                // store this stuff in the data model
                if (DataModel.user == null) {
                    DataModel.user = new User();
                }
                DataModel.user.parseFromJSONUserAccountInfo(json);

                // use account manager to save and restore data
                DebugLog("UserAccount: %s", DataModel.user.getDisplayName());
                lootsieAccountManager.createUser(DataModel.user, DataModel.userSessionToken);
                if (LootsieGlobals.debugLevel > 0) DataModel.user.print();

                LootsieEngine.getInstance().updateCurrentActivitiesAndFragments();

            } catch (JSONException ex) {
                Logs.e(TAG, "GetUserAccountResult: exception: " + ex.getMessage());
                cleanup();
            }
        } else {
            // register failure
            // "Register success: \nError getting account data.\n Please try again later."
        	 if (callback != null) callback.onMergePointsFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.MERGE_POINTS_SEQUENCE_FAILURE_REGISTER_EMAIL);
        }

        cleanup();
    }
    
    //public static Toast makeText(Context context, CharSequence text, int duration)
//    private static void SafeToastMakeText(CharSequence text, int duration) {
//        if (LootsieEngine.getInstance().getActivityContext() != null) {
//        	Toast.makeText(LootsieEngine.getInstance().getActivityContext(), text, duration).show();
//        }
//    }    

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (com.lootsie.sdk.utils.LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}
