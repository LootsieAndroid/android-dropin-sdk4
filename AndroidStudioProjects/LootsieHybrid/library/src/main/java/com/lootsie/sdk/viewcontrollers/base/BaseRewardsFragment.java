package com.lootsie.sdk.viewcontrollers.base;

import android.annotation.TargetApi;
import android.os.Build;

import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.model.RewardListEntryModel;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jerrylootsie on 3/2/15.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
//public class BaseRewardsFragment extends UpdatableListFragment {
public class BaseRewardsFragment extends UpdatableFragment {

    static private String TAG = "Lootsie BaseRewardsFragment";
    static List<RewardListEntryModel> list = new ArrayList<RewardListEntryModel>();

    //static HashMap<String, Reward> rewardHashMap = new HashMap<String, Reward>();
    static HashMap<String, RewardListEntryModel> rewardListEntryModelHashMap = new HashMap<String, RewardListEntryModel>();

    protected List<RewardListEntryModel> getModel() {

        if (DataModel.userRewards == null) {
            Logs.e(TAG, "RewardsActivity: userRewards is null!");
            return list;
        }

        DebugLog("getModel Rewards: " + DataModel.userRewards.rewards.size());

        // may be initialized multiple times, so don't rescan the list, but update it instead?
        // because samsung resets the view when bringing up the keyboard - ugh!
        if (list.size() == 0) {
            // new list
            updateList();
        } else {
            // update the list
            boolean listUpdateNeeded = false;

            // an entry removed, or added
            if (DataModel.userRewards.rewards.size() != list.size() ) {
                listUpdateNeeded = true;
            }

            // detect a new entry added, or replaced
            for (int i = 0; (i < DataModel.userRewards.rewards.size()) && !listUpdateNeeded; i++) {
                Reward reward = DataModel.userRewards.rewards.get(i);
                if (!rewardListEntryModelHashMap.containsKey(reward.id)) {
                    listUpdateNeeded = true;
                }
            }

            if (listUpdateNeeded) {
                updateList();
            }
            
            // ASDK-441 test empty rewards list
            // Initially select one of the items
            list.get(1).setSelected(true);            
        }


        return list;
    }

    private void updateList() {

        rewardListEntryModelHashMap.clear();
        list.clear();

        for (int i = 0; i < DataModel.userRewards.rewards.size(); i++) {
            Reward reward = DataModel.userRewards.rewards.get(i);
            RewardListEntryModel rewardListEntryModel = new RewardListEntryModel(reward);
            list.add(rewardListEntryModel);

            // for checking of additions, subtractions later
            rewardListEntryModelHashMap.put(reward.id, rewardListEntryModel);
        }

    }

    protected RewardListEntryModel get(String s) {
        return new RewardListEntryModel(s);
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }   
}
