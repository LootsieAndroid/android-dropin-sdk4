package com.lootsie.sdk.netutil;

import android.os.AsyncTask;
//import android.util.Log;

import java.util.Map;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.Logs;

/**
 * Created by jerrylootsie on 2/9/15.
 */



/*
    POST https://api-v2.lootsie.com/v2/api/session
    ?latlng=-1.0,-1.0
    &sdk_version=v2
    &platform=Unity
    &device=deviceModelStr
    &firmware=operatingSystemStr
    &language=languageStr
    &country=regionNameStr
    X-Lootsie-App-Secret
 */
public class RESTGetSessionTask extends AsyncTask< Map<String, Object>, Void, RestResult> implements IGenericAsyncTask<Map<String, Object>> {

    private final static String TAG = "Lootsie.RESTGetSessionTask";

    IRESTCallback callback = null;

    public RESTGetSessionTask() {
    }

    public RESTGetSessionTask(IRESTCallback restCallback) {
        callback = restCallback;
    }

    private RestResult processError(Exception e) {
        RestResult response = new RestResult();
        String errorResponse = e.getMessage();
        if (errorResponse == null) {
            e.printStackTrace();
        } else {
//            response.status = e.code;
            Logs.e(TAG,"RESTGetSessionTask: processError: getMessage: " + e.getMessage());
        }
        return response;
    }

    @Override
    protected RestResult doInBackground( Map<String, Object> ... params) {

        if (params == null || params.length == 0) {
            return null;
        }

        RestResult response = new RestResult();
        try {
            // pull stuff from Device.java
            Map<String, Object> map = params[0];

            response = RestClient.doPost(LootsieApi.INIT_API_SESSION_URL.getUri(), map);

            if (LootsieGlobals.debugLevel > 0) Logs.v(TAG, "RESTGetSessionTask:" + response.content);

        } catch (Exception e) {
            Logs.e(TAG, "RESTGetSessionTask: processError: getMessage: " + e.getMessage());
        }
        return response;
    }



    @Override
    protected void onPostExecute(final RestResult restResult) {
        if (callback != null) {
            callback.restResult(restResult);
        }
    }

    @Override
    public void executeTask(Map<String, Object>... map) {
        execute(map);
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }
}