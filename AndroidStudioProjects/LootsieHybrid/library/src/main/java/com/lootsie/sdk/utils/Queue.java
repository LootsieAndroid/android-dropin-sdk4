package com.lootsie.sdk.utils;

/**
 * Created by jerrylootsie on 2/13/15.
 */
public interface Queue {

    // return the size of the queue
    public int size();

    public boolean isEmpty();

    public boolean isFull();

    // insert an element into the queue
    public void enqueue(Object obj) throws QueueFullException;

    // removes an element from the queue
    public Object dequeue() throws QueueEmptyException;

    public String getBufferDump();


    public class QueueFullException extends RuntimeException {

        public QueueFullException() {
            super();
        }

        public QueueFullException(String message) {
            super(message);
        }

        public QueueFullException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public class QueueEmptyException extends RuntimeException {

        public QueueEmptyException() {
            super();
        }

        public QueueEmptyException(String message) {
            super(message);
        }

        public QueueEmptyException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
