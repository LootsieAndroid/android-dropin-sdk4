package com.lootsie.sdk.viewcontrollers;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


import com.lootsie.sdk.R;
import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.RUtil;
import com.lootsie.sdk.viewcontrollers.base.BaseVersionInfoFragment;

import java.util.ArrayList;

/**
 * Created by jerrylootsie on 3/2/15.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class VersionInfoFragment extends BaseVersionInfoFragment {

    static private String TAG = "Lootsie VersionInfoFragment";

    static TextView applicationText = null;
    static TextView platformText = null;
    static TextView deviceText = null;
    static TextView firmwareText = null;
    static TextView languageText = null;
    static TextView countryText = null;
    static TextView locationText = null;
    static TextView idfaText = null;
    static TextView idfvText = null;

    static TextView nativeSDKVersionText = null;

    static TextView dataCachedText = null;
    static TextView apiURLText = null;

    static ListView versionInfoListView = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        DebugLog("onCreateView");

        //View view = inflater.inflate(R.layout.com_lootsie_versioninfo_fragment, container, false);
        View view = inflater.inflate(RUtil.getLayoutId(this.getActivity().getApplicationContext(), "com_lootsie_versioninfo_fragment"), container, false);

        device = LootsieEngine.getInstance().getDevice();

        // can't find anything when first created - rely on onResume call instead
        //updateEntries();

        view.setTag("versionInfoFragment");

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        DebugLog("onResume");

        updateEntries();
    }

    @Override
    public void updateFragment() {
    	DebugLog("updateFragment: " + String.valueOf(DataModel.user.totalLp) + " " + DataModel.currency_abbreviation);

        updateEntries();
    }

    
    private void updateEntries() {

        if (this.getActivity() == null) {
            DebugLog("updateEntries: activity is null!");
            return;
        }


        //applicationText = (TextView) this.getActivity().findViewById(R.id.applicationText);
        //applicationText = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "applicationText"));
        //int applicationTextId = getResources().getIdentifier("applicationText", "id", this.getActivity().getPackageName());
        //applicationText = (TextView) this.getActivity().findViewById(applicationTextId);
        applicationText = (TextView) this.getActivity().findViewById(RUtil.getResourceId(this.getActivity().getApplicationContext(), "applicationText", "id", this.getActivity().getPackageName()));
        if (applicationText != null) {
            //applicationText.setText("Application: " + DataModel.app.name);
            applicationText.setText(DataModel.app.name);
        } else {
            Logs.e(TAG, "updateEntries: applicationText not found!");
        }

        //platformText = (TextView) this.getActivity().findViewById(R.id.platformText);
        platformText = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "platformText"));
        if (platformText != null) {
            //platformText.setText("Platform: " + device.getPlatform());
            platformText.setText(device.getPlatform());
        } else {
            Logs.e(TAG, "updateEntries: platformText not found!");
        }

        //deviceText = (TextView) this.getActivity().findViewById(R.id.deviceText);
        deviceText = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "deviceText"));
        if (deviceText != null) {
            //deviceText.setText("Device: " + device.getDevice());
            deviceText.setText(device.getDevice());
        } else {
            Logs.e(TAG, "updateEntries: deviceText not found!");
        }

        //firmwareText = (TextView) this.getActivity().findViewById(R.id.firmwareText);
        firmwareText = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "firmwareText"));
        if (firmwareText != null) {
            //firmwareText.setText("Firmware: " + device.getFirmware());
            firmwareText.setText(device.getFirmware());
        } else {
            Logs.e(TAG, "updateEntries: firmwareText not found!");
        }

        //languageText = (TextView) this.getActivity().findViewById(R.id.languageText);
        languageText = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "languageText"));
        if (languageText != null) {
            //languageText.setText("Language: " + device.getLanguage());
            languageText.setText(device.getLanguage());
        } else {
            Logs.e(TAG, "updateEntries: languageText not found!");
        }

        //countryText = (TextView) this.getActivity().findViewById(R.id.countryText);
        countryText = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "countryText"));
        if (countryText != null) {
            //countryText.setText("Country: " + device.getCountry());
            countryText.setText(device.getCountry());
        } else {
            Logs.e(TAG, "updateEntries: countryText not found!");
        }

        //locationText = (TextView) this.getActivity().findViewById(R.id.locationText);
        locationText = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "locationText"));
        if (locationText != null) {
            // ASDK-400
            //locationText.setText("Location: " + device.getLocation());
            //locationText.setText("Location: " + LootsieEngine.getInstance().getLocationStr());
            locationText.setText(LootsieEngine.getInstance().getLocationStr());
        } else {
            Logs.e(TAG, "updateEntries: locationText not found!");
        }

        //idfaText = (TextView) this.getActivity().findViewById(R.id.idfaText);
        idfaText = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "idfaText"));
        if (idfaText != null) {
            //idfaText.setText("idfa: " + device.getIdfa());
            idfaText.setText(device.getIdfa());
        } else {
            Logs.e(TAG, "updateEntries: idfaText not found!");
        }

        //idfvText = (TextView) this.getActivity().findViewById(R.id.idfvText);
        idfvText = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "idfvText"));
        if (idfvText != null) {
            //idfvText.setText("idfv: " + device.getIdfv());
            idfvText.setText(device.getIdfv());
        } else {
            Logs.e(TAG, "updateEntries: idfvText not found!");
        }

        // TODO: template?

        // TODO: notification

        // nativeSDKVersion
        //nativeSDKVersionText = (TextView) this.getActivity().findViewById(R.id.nativeSDKVersionText);
        nativeSDKVersionText = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "nativeSDKVersionText"));
        if (nativeSDKVersionText != null) {
            //nativeSDKVersionText.setText("NativeSDKVersion: " + LootsieGlobals.SDK_VERSION);
            nativeSDKVersionText.setText(LootsieGlobals.SDK_VERSION);
        } else {
            Logs.e(TAG, "updateEntries: nativeSDKVersionText not found!");
        }

        // TODO: webapp version

        // dataCachedText
        dataCachedText = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(),"dataCachedText"));
        if (dataCachedText != null) {
            dataCachedText.setText("dataCached: "  + String.valueOf(DataModel.networkCacheEnabled) + " : " + String.valueOf(LootsieEngine.getInstance().getCacheDuration()));
        } else {
            Logs.e(TAG, "updateEntries: dataCachedText not found!");
        }


        //apiURLText = (TextView) this.getActivity().findViewById(R.id.apiURLText);
        apiURLText = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "apiURLText"));
        if (apiURLText != null) {
            //apiURLText.setText("API URL: " + LootsieApi.BASE_API_URL);
            apiURLText.setText(LootsieApi.BASE_API_URL);
        } else {
            Logs.e(TAG, "updateEntries: apiURLText not found!");
        }

        // debug caching
        versionInfoListView = (ListView) this.getActivity().findViewById((RUtil.getId(this.getActivity(), "versionInfoListView")));
        if (versionInfoListView != null) {
            if (LootsieGlobals.debugLevel > 1) {
                ArrayList<String> cacheValues = DataModel.getNetworkStats();

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(),
                        android.R.layout.simple_list_item_1, android.R.id.text1, cacheValues);

                // Assign adapter to ListView
                versionInfoListView.setAdapter(adapter);

            } else {
                versionInfoListView.setVisibility(View.GONE);
            }
        }
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }     
}
