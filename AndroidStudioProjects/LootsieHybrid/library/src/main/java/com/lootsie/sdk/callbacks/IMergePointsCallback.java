package com.lootsie.sdk.callbacks;

import com.lootsie.sdk.utils.LootsieGlobals;

public interface IMergePointsCallback {
    public void onMergePointsSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result);
    public void onMergePointsFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result);
}
