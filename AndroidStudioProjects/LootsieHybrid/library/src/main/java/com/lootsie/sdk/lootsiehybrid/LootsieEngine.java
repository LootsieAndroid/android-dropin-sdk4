package com.lootsie.sdk.lootsiehybrid;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.widget.Toast;
import android.graphics.Bitmap;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.callbacks.IUpdateUserAccountCallback;
import com.lootsie.sdk.device.Device;
import com.lootsie.sdk.device.ScreenOrientationManager;
import com.lootsie.sdk.lootsiehybrid.sequences.AchievementReachedSequence;
import com.lootsie.sdk.lootsiehybrid.sequences.InitSequence;
import com.lootsie.sdk.lootsiehybrid.sequences.MergePointsSequence;
import com.lootsie.sdk.lootsiehybrid.sequences.RedemptionSequence;
import com.lootsie.sdk.lootsiehybrid.sequences.UpdateAchievementPageSequence;
import com.lootsie.sdk.lootsiehybrid.sequences.UpdateUserAccountSequence;
import com.lootsie.sdk.lootsiehybrid.sequences.ResetUserSequence;
import com.lootsie.sdk.model.Achievement;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.model.User;


import com.lootsie.sdk.callbacks.IAchievementReached;
import com.lootsie.sdk.callbacks.IMergePointsCallback;
import com.lootsie.sdk.callbacks.IRedeemReward;
import com.lootsie.sdk.callbacks.IInitializationCallback;
import com.lootsie.sdk.callbacks.IGetSessionTokens;
import com.lootsie.sdk.callbacks.ILogData;
import com.lootsie.sdk.callbacks.ISignOutCallback;
import com.lootsie.sdk.callbacks.IUpdateAchievementsCallback;

import com.lootsie.sdk.net.Headers;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.netutil.NetworkCommand;
import com.lootsie.sdk.netutil.NetworkCommandQueue;
import com.lootsie.sdk.netutil.RESTDeleteUserSessionTask;
import com.lootsie.sdk.netutil.RESTPostUserLocationTask;
import com.lootsie.sdk.netutil.TrackingSessionAsyncTask;

import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.LootsieGlobals.LOOTSIE_LOGGING_LEVEL;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.PreferencesHandler;
import com.lootsie.sdk.viewcontrollers.LootsiePageBorderActivity;
import com.lootsie.sdk.utils.image.ImageCacheManager;

import com.lootsie.sdk.viewcontrollers.base.IUpdatableActivity;
import com.lootsie.sdk.viewcontrollers.base.IUpdatableFragment;
import com.lootsie.sdk.viewcontrollers.base.IUpdatableListActivity;
import com.lootsie.sdk.viewcontrollers.base.IUpdatableListFragment;

import com.lootsie.sdk.viewcontrollers.base.MessageView;
import com.lootsie.sdk.viewcontrollers.base.UpdatableActivity;
import com.lootsie.sdk.viewcontrollers.base.UpdatableListActivity;
import com.lootsie.sdk.viewcontrollers.base.UpdatableListFragment;
import com.lootsie.sdk.viewcontrollers.base.UpdatableFragment;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by jerrylootsie on 2/9/15.
 */
public class LootsieEngine implements IUpdatableActivity, IUpdatableFragment, IUpdatableListActivity, IUpdatableListFragment  {

    private static final String TAG = "Lootsie Library";

    // Lootsie window position to appear
    public static final String DEFAULT_POSITION ="default";
    public static final String TOP_LEFT_POSITION ="topLeft";
    public static final String TOP_RIGHT_POSITION ="topRight";
    public static final String BOTTOM_LEFT_POSITION ="bottomLeft";
    public static final String BOTTOM_RIGHT_POSITION ="bottomRight";
    public static final String CENTER_LEFT_POSITION ="centerLeft";
    public static final String CENTER_RIGHT_POSITION ="centerRight";

    // Lootsie Rendering Modes
    public static final int RENDER_PORTRAIT = 1;
    public static final int RENDER_LANDSCAPE = 2;
    public static final int RENDER_DEFAULT = 4;


    private static int DISK_IMAGECACHE_SIZE = 1024 * 1024 * 10;
    private static Bitmap.CompressFormat DISK_IMAGECACHE_COMPRESS_FORMAT = Bitmap.CompressFormat.PNG;
    private static int DISK_IMAGECACHE_QUALITY = 100;  //PNG is lossless so quality is ignored but must be provided



    // Used by Lootsie manager without its instance
    private static Application lootsieAppcontext = null;

    private static Activity lootsieActivityContext = null;

    private static LootsieEngine lootsieEngineInstance = null;

    private static final String APP_KEY = "appkey";

    private static String appId = null;

    private static LootsieAccountManager lootsieAccountManager = null;

    private static Device device = null;

    private static boolean sdkInitialized = false;

    private static boolean gpsEnabled = true;
    
    private static boolean sdkShowing = false;

    private static InternalInitCallback interalCB = null;

    private static IAchievementReached achievementCB = null;
    
    private static IRedeemReward redeemCB = null;

    private static ScreenOrientationManager screenOrientationMgr = null;

    private static String locationStr = "";

    private static int cacheDuration = LootsieGlobals.NETWORK_CACHE_TIME_SECONDS;

    // ASDK-366 Android-SDK4 queueing achievements/commands waiting for initialization (Android)
    private static ArrayList<NetworkCommand> commandQueue = new ArrayList<NetworkCommand>();
    private static Thread commandThread = null;
    private static NetworkCommandQueue runnableQueue = null;


    // updating android activity from a background thread
    // http://cogitolearning.co.uk/?p=793
    private UpdatableActivity currentActivity = null;
    private UpdatableListActivity currentListActivity = null;
    //private UpdatableListFragment currentListFragment = null;
    private HashMap<String, UpdatableListFragment> listFragmentMap = new HashMap<String, UpdatableListFragment>();
    private UpdatableFragment currentFragment = null;

    private static LOOTSIE_NOTIFICATION_CONFIGURATION notificationConfiguration = LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_rewardsPage;
    
    /**
     * request queue
     */
    private static RequestQueue mRequestQueue;


    // to get data back from init sequence
    static class InternalInitCallback implements IInitializationCallback {
        IInitializationCallback nestedCB = null;

        public InternalInitCallback(IInitializationCallback inputCB) {
            nestedCB = inputCB;
        }

        @Override
        public void onInitSuccess() {
            DebugLog("InternalInitCallback: onInitSuccess");
            sdkInitialized = true;

            // ASDK-366
            // kick off the thread to send out the queued commands
            DebugLog("Start processing the command queue");
            runnableQueue = new NetworkCommandQueue(commandQueue);
            commandThread = new Thread(runnableQueue);
            commandThread.start();


            if (nestedCB != null) {
                nestedCB.onInitSuccess();
            }
        }

        @Override
        public void onInitFailure() {
            DebugLog("InternalInitCallback: onInitFailure");
            sdkInitialized = false;

            if (nestedCB != null) {
                nestedCB.onInitFailure();
            }
        }
    }
    
    static class InternalRedeemRewardCallback implements IRedeemReward {
    	IRedeemReward nestedCB = null;

    	public InternalRedeemRewardCallback(IRedeemReward inputCB) {
    		nestedCB = inputCB;
    	}
    	
		@Override
		public void onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result, String json) {
            DebugLog("InternalRedeemRewardCallback: onRedeemFailed: %s : %s", result.toString(), json);

            switch(result) {
                case REDEMPTION_SEQUENCE_FAILURE:
                    lootsieMessageDialog("Uh oh!", "Looks like you don't have enough points for this one.");
                    break;
                case REDEMPTION_SEQUENCE_FAILURE_INVALID_EMAIL:
                    lootsieMessageDialog("Uh oh!", "Invalid email address.");
                    break;
                case REDEMPTION_SEQUENCE_FAILURE_REGISTER_EMAIL:
                    lootsieMessageDialog("Uh oh!", "Unable to register.");
                    break;
                case REDEMPTION_SEQUENCE_FAILURE_NOT_AFFORDABLE:
                    lootsieMessageDialog("Uh oh!", "Looks like you don't have enough points for this one.");
                    break;
                case REDEMPTION_SEQUENCE_FAILURE_REDEMPTION_LIMIT:
                    lootsieMessageDialog("Uh oh!", "Redemption limit reached.");
                    break;

                default:
                    lootsieMessageDialog("Uh oh!", "Looks like you don't have enough points for this one.");
                    break;
            }

            if (nestedCB != null) {
                nestedCB.onRedeemFailed(result, json);
            }		
			
		}

		@Override
		public void onRedeemSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result, String json) {
            DebugLog("InternalRedeemRewardCallback: onRedeemSuccess: %s : %s", result.toString(), json);

            switch(result) {
                case REDEMPTION_SEQUENCE_SUCCESS:
                    lootsieMessageDialog("Congrats!", "Now check your inbox for reward details.");
                    break;
                default:
                    lootsieMessageDialog("Congrats!", "Now check your inbox for reward details.");
                    break;
            }

            LootsieEngine.getInstance().updateCurrentActivitiesAndFragments();

            if (nestedCB != null) {
                nestedCB.onRedeemSuccess(result, json);
            }			
		}
    	
    	
    }

    static class InternalAchievementReachedCallback implements IAchievementReached {
        IAchievementReached nestedCB = null;

        public InternalAchievementReachedCallback(IAchievementReached inputCB) {
            nestedCB = inputCB;
        }

        @Override
        public void onLootsieBarClosed() {
            DebugLog("InternalAchievementReachedCallback: onLootsieBarClosed");

            LootsieEngine.getInstance().sdkShowing = false;
            
            if (nestedCB != null) {
                nestedCB.onLootsieBarClosed();
            }
        }

        @Override
        public void onLootsieBarExpanded() {
            DebugLog("InternalAchievementReachedCallback: onLootsieBarExpanded");

            LootsieEngine.getInstance().updateCurrentActivitiesAndFragments();

            if (nestedCB != null) {
                nestedCB.onLootsieBarExpanded();
            }
        }

        @Override
        public void onLootsieFailed(String mesg) {
            DebugLog("InternalAchievementReachedCallback: onLootsieFailed: " + mesg);

            if (nestedCB != null) {
                nestedCB.onLootsieFailed(mesg);
            }
        }

        @Override
        public void onLootsieSuccess() {
            DebugLog("InternalAchievementReachedCallback: onLootsieSuccess");

            if (nestedCB != null) {
                nestedCB.onLootsieSuccess();
            }
        }

        @Override
        public void onNotificationData(String json) {
            DebugLog("InternalAchievementReachedCallback: onNotificationData: %s", json);

            if (nestedCB != null) {
                nestedCB.onNotificationData(json);
            }
        }
    }

	/*
	 * This method registers LEG applications to Lootsie engine. Once registered
	 * successfully Lootsie engine will run as part of the calling
	 * application and lasts until the apps life cycle. This method MUST be
	 * called only ONCE at the start of the app
	 */
    public static void init(Application legApp, String appKey, IInitializationCallback callback) {


        try {
            sdkInitialized = false;

            // version check
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                Logs.e(TAG,"Lootsie SDK isn't supported on versions below 4.0 ICE_CREAM_SANDWICH");

                if (callback != null) {
                    callback.onInitFailure();
                }
                return;
            }

//            LootsieManager.init(legApp, appKey, callback);
            // We should be able to retrieve all these LEG parameters at any time
            // in case if want to restart lootsie.
            lootsieAppcontext = legApp;

            // Save app key in preference
            setApplicationKey(appKey);

            // Already running? check
            if(lootsieEngineInstance != null)
            {
                DebugLog("Lootsie has already been initialized");   
                return;
            } else {
            	lootsieEngineInstance = new LootsieEngine();
            }
            
            forceInit(appKey, callback);
            
        }catch (Exception e) {
            Logs.e(TAG, "init:Some Unexpected exception during init()");
            e.printStackTrace();
        }
    }

    /**
     * for testing purposes
     */
    public static LootsieEngine testInit() {

        // Already running? check
        if(lootsieEngineInstance != null)
        {
            DebugLog("Lootsie has already been initialized");
        } else {
            lootsieEngineInstance = new LootsieEngine();
        }

        return lootsieEngineInstance;
    }

    /**
     * for testing purposes
     * @param inputAccountManager
     */
    public static void setLootsieAccountManager(LootsieAccountManager inputAccountManager) {
        lootsieAccountManager = inputAccountManager;
    }


    public static void setApplicationContext(Application legApp) {
        lootsieAppcontext = legApp;
    }

    /**
     * forceInit can be used by init, also in the ResetUserSequence
     * @param appKey
     * @param callback
     */
    public static void forceInit(String appKey, IInitializationCallback callback) {

        try {
            sdkInitialized = false;    
    
            
            lootsieEngineInstance.appId = appKey;
            lootsieAccountManager = new LootsieAccountManager(lootsieAppcontext);

            // inititalize all lootsie component
            lootsieEngineInstance.interalCB = new InternalInitCallback(callback);

            mRequestQueue = Volley.newRequestQueue(lootsieAppcontext);

            /**
             * Create the image cache. Uses Memory Cache by default. Change to Disk for a Disk based LRU implementation.
             */
            ImageCacheManager.getInstance().init(lootsieAppcontext,
                    mRequestQueue,
                    lootsieAppcontext.getPackageCodePath()
                    , DISK_IMAGECACHE_SIZE
                    , DISK_IMAGECACHE_COMPRESS_FORMAT
                    , DISK_IMAGECACHE_QUALITY
                    , ImageCacheManager.CacheType.DISK);


            DataModel.init();

            // Lootsie Webview Screen orientation tracker
            screenOrientationMgr = new ScreenOrientationManager();

            // get device info, that we'll need for GetSession call later
            device = new Device(lootsieEngineInstance);

            // default location services are enabled


            // device check
            if (!device.build(gpsEnabled)) {
                Logs.e(TAG,"Lootsie SDK isn't supported on this device!");

                if (callback != null) {
                    callback.onInitFailure();
                }
                return;
            }


            InitSequence.start(device, appKey, interalCB);


        }catch (Exception e) {
            Logs.e(TAG, "init:Some Unexpected exception during init()");
            e.printStackTrace();
        }
    }

    public static void init(Application legApp, String appKey) {
        try {
            LootsieEngine.init(legApp, appKey, null);
        }catch (Exception e) {
            Logs.e(TAG, "init:Some Unexpected exception during init()");
            e.printStackTrace();
        }
    }

    static class InternalSignOutCallback implements ISignOutCallback {

		@Override
		public void onSignOutSuccess() {
			DebugLog("LootsieEngine: SignOutSuccess");				
		}

		@Override
		public void onSignOutFailure() {
			DebugLog("LootsieEngine: SignOutFailure");				
		}
		
	}
    
    /**
     * log out the current user, and login a new guest user
     * @return
     */
    public static boolean signOut() {
    	InternalSignOutCallback signoutCallback = new InternalSignOutCallback();
    	
    	return signOut(signoutCallback);
    }
    
    /**
     * log out the current user, and login a new guest user
     * @return
     */
    public static boolean signOut(ISignOutCallback signoutCallback) {    	
        try {
            ResetUserSequence.start(LootsieEngine.getInstance().appId, signoutCallback);
        }catch (Exception e) {
            Logs.e(TAG, "signOut:Some Unexpected exception during init()");
            e.printStackTrace();
        }
        return false;
    }    

    // ASDK-355 Android-SDK4 setLogLevel - optional enable or disable debugging
    public static void setLogLevel(LOOTSIE_LOGGING_LEVEL loglevel) {
        LootsieGlobals.setLogLevel(loglevel);
    }

    /**
     * for getting logging data from engine to display in a texview in real time
     * @param callback
     */
    public static void setLogCallback(ILogData callback) {

        Logs.setLogCallback(callback);
    }

    /**
     * This method returns true if Lootsie is running otherwise returns false
     *
     */
    public static boolean isRunning() {
        try {
            return sdkInitialized;
        }catch (Exception e) {
            Logs.e(TAG, "isRunning:Some Unexpected exception during isRunning()");
            e.printStackTrace();
        }
        return false;
    }

    /**
     * getUserRewards
     * get the list of rewards available to the user in this app
     * @return
     */
    public static ArrayList<Reward> getUserRewards() {
    	
        if (!sdkInitialized) {
            DebugLog("getUserRewards: sdk is disabled!");
            return null;
        }    	
    	
        List<Reward> rewards = DataModel.userRewards.rewards;
        if (LootsieGlobals.debugLevel > 0) {
	        for (int i = 0; i < rewards.size(); i++) {
	            Reward r = rewards.get(i);
	            DebugLog("reward[%d] %s", i, r.toString());
	        }
        }        
        
        // copy this to another list to return data, to keep stuff from getting corrupted/messed up
        ArrayList<Reward> rewardsCopy = new ArrayList<Reward>(rewards);

        return rewardsCopy;        
    }
    
    
    /**
     * getAppAchievements
     * get the list of achievements available in the app
     */
    public static ArrayList<Achievement> getAppAchievements() {

        if (!sdkInitialized) {
            DebugLog("getAppAchievements: sdk is disabled!");
            return null;
        }

        List<Achievement> achievements = DataModel.app.achievements;
        if ((LootsieGlobals.debugLevel > 0) && (achievements != null)){
	        for (int i = 0; i < achievements.size(); i++) {
	            Achievement a = achievements.get(i);
	            DebugLog("achievement[%d] %s", i, a.toString());
	        }
        }
        
        // copy this to another list to return data, to keep stuff from getting corrupted/messed up
        ArrayList<Achievement> achievementsCopy = new ArrayList<Achievement>(achievements);

        return achievementsCopy;
    }


    /**
     * This will display Lootsie dialog window on top of the existing activity
     * @param achievementId as set on Developer portal
     * @param windowPosition where to appear on the screen
     * 			Lootsie.DEFAULT_POSITION - appears in default position
     *		    Lootsie.TOP_LEFT_POSITION - appears from top left
     *		    Lootsie.TOP_RIGHT_POSITION - appears from top right
     *		    Lootsie.BOTTOM_LEFT_POSITION - appears from bottom left
     *		    Lootsie.BOTTOM_RIGHT_POSITION - appears from bottom right
     *		    Lootsie.CENTER_LEFT_POSITION - appears from centre left
     *		    Lootsie.CENTER_RIGHT_POSITION - appear from center Right
     *
     * @param callback object or null
     */
    public static void AchievementReached(Activity a, String achievementId, String windowPosition, IAchievementReached callback) {
        if (!sdkInitialized) {
//            DebugLog("AchievementReached: sdk is disabled!");
//            if (callback != null) {
//                callback.onLootsieFailed();
//            }

            // ASDK-366
            // queue the achievement
            DebugLog("Queuing the achievement: %s", achievementId);
            lootsieEngineInstance.lootsieActivityContext = a;
            NetworkCommand command = new NetworkCommand(NetworkCommand.LOOTSIE_COMMAND_TYPE.ACHIEVEMENT_REACHED, achievementId, callback);
            commandQueue.add(command);

            return;
        }


        try {
            lootsieEngineInstance.lootsieActivityContext = a;
            lootsieEngineInstance.achievementCB = new InternalAchievementReachedCallback(callback);

            // kick off the achievement reached sequence
            AchievementReachedSequence.start(achievementId, lootsieEngineInstance.achievementCB);
        }catch (Exception e) {
            Logs.e(TAG, "ARwithCb:Some Unexpected exception during AchievementReached()");
            e.printStackTrace();

            if (callback != null) {
                callback.onLootsieFailed("");
            }
        }
    }

    // ASDK-366
    public void clearCmdQ()
    {
        // clear commands
        commandQueue.clear();
    }

    /**
     * This will display Lootsie dialog window on top of the existing activity
     * @param achievementId as set on Developer portal
     */
    public static void AchievementReached(Activity a, String achievementId) {
        if (!sdkInitialized) {
            DebugLog("AchievementReached: sdk is disabled!");
            return;
        }

        DebugLog("AchievementReached: %s", achievementId);

        try {
            lootsieEngineInstance.lootsieActivityContext = a;
            lootsieEngineInstance.achievementCB = new InternalAchievementReachedCallback(null);

            // kick off the achievement reached sequence
            AchievementReachedSequence.start(achievementId, lootsieEngineInstance.achievementCB);
        }catch (Exception e) {
            Logs.e(TAG, "AR:Some Unexpected exception during AchievementReached()");
            e.printStackTrace();
        }
    }

    public static void redeemReward(String emailStr, String rewardIdStr, IRedeemReward callback ) {
    	lootsieEngineInstance.redeemCB = new InternalRedeemRewardCallback(callback);
    
    	redeemReward(emailStr, rewardIdStr);
    }
    
    
    public static void redeemReward(String emailStr, String rewardIdStr) {
        if (!sdkInitialized) {
            DebugLog("redeemReward: sdk is disabled!");
            return;
        }

        DebugLog("redeemReward: email: %s id: %s", emailStr, rewardIdStr);

        try {
            if (lootsieEngineInstance.redeemCB == null) {
                lootsieEngineInstance.redeemCB = new InternalRedeemRewardCallback(null);
            }
        	
        	
        	// try to find reward using rewardidstr in list of rewards
        	ArrayList<Reward> rewards = DataModel.userRewards.rewards;
        	Reward targetReward = null;
        	
        	boolean foundReward = false;
        	for (int i = 0; (i < rewards.size()) && !foundReward; i++) {
        		Reward reward = rewards.get(i);
        		if (reward.id.equalsIgnoreCase(rewardIdStr)) {
        			targetReward = reward;
        			foundReward = true;
        		}
        	}

        	if (foundReward) {
        		 LootsieEngine.getInstance().redeemReward(emailStr, targetReward);
        	} else {
        		DebugLog("redeemReward: reward for id not found!");
        	}
        	
        }catch (Exception e) {
            Logs.e(TAG, "AR:Some Unexpected exception during AchievementReached()");
            e.printStackTrace();
        }
    }


    public void redeemReward(String emailStr, Reward reward) {
        if (!sdkInitialized) {
            DebugLog("redeemReward: sdk is disabled!");
            return;
        }

        DebugLog("redeemReward: email: %s id: %s name: %s", emailStr, reward.id, reward.name);

        redeemReward(emailStr, reward);
    }

    
    public void redeemReward(String emailStr, Reward reward, IRedeemReward callback) {
        if (!sdkInitialized) {
            DebugLog("redeemReward: sdk is disabled!");
            return;
        }

        DebugLog("redeemReward: email: %s id: %s name: %s", emailStr, reward.id, reward.name);

        try {
            if (lootsieEngineInstance.redeemCB == null) {
                lootsieEngineInstance.redeemCB = new InternalRedeemRewardCallback(callback);
            }
            
            // kick off the redeem reward sequence
            RedemptionSequence.start(emailStr, reward, lootsieEngineInstance.redeemCB);
        }catch (Exception e) {
            Logs.e(TAG, "AR:Some Unexpected exception during AchievementReached()");
            e.printStackTrace();
        }
    }

    static class InternalMergePointsCallback implements IMergePointsCallback {

		@Override
		public void onMergePointsSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result) {

            switch(result) {
                case MERGE_POINTS_SEQUENCE_SUCCESS:
                    lootsieMessageDialog(
                            "Welcome back!",
                            "You are logged in.");
                    break;
                default:
                    lootsieMessageDialog("Welcome back!", "You are logged in.");
                    break;
            }


		}

		@Override
		public void onMergePointsFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result) {

            switch(result) {
                case MERGE_POINTS_SEQUENCE_FAILURE_REGISTER_EMAIL:
                    lootsieMessageDialog(
                            "Uh oh!",
                            "Sorry, but points cannot be merged at this time.");
                    break;
                case MERGE_POINTS_SEQUENCE_FAILURE_ALREADY_REGISTERED:
                    lootsieMessageDialog(
                            "No need to merge!",
                            "You’re already logged in!");
                    break;
                case MERGE_POINTS_SEQUENCE_FAILURE_INVALID_EMAIL:
                    lootsieMessageDialog(
                            "Uh oh!",
                            "Invalid email address.");
                    break;
                case MERGE_POINTS_SEQUENCE_FAILURE_LP_MAXED:
                    lootsieMessageDialog(
                            "You’re too good!",
                            "Looks like you've hit the daily app max. Dont worry, you'll keep earning more tomorrow!");
                    break;
                default:
                    lootsieMessageDialog("Uh oh!", "Sorry, but points cannot be merged at this time.");
                    break;
            }


		}
    	
    	
    }
    
    /**
     * merge points by converting a guest account to a registed account with an email address
     * @param emailStr
     */
    public void mergePoints(String emailStr) {
    	InternalMergePointsCallback mergePointsCallback = new InternalMergePointsCallback();
    	
    	mergePoints(emailStr, mergePointsCallback);
    }
    
    /**
     * merge points by converting a guest account to a registed account with an email address
     * @param emailStr
     */
    public void mergePoints(String emailStr, IMergePointsCallback mergePointsCallback) {
    	
        if (!sdkInitialized) {
            DebugLog("mergePoints: sdk is disabled!");
            return;
        }

        DebugLog("mergePoints: %s", emailStr);

        try {
            // kick off the merge points sequence
            MergePointsSequence.start(emailStr, mergePointsCallback);
        }catch (Exception e) {
            Logs.e(TAG, "AR:Some Unexpected exception during MergePointsSequence()");
            e.printStackTrace();
        }

    }

    /**
     * tasks to perform when succesfully updating the user account
     */
    static class InternalUpdateUserAccountCallback implements IUpdateUserAccountCallback {
        @Override
        public void onUpdateUserAccountSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result) {

            switch(result) {
                case UPDATE_USER_ACCOUNT_SEQUENCE_SUCCESS:
                    lootsieMessageDialog("Success!", "Account Updated!");
                    break;
                default:
                    lootsieMessageDialog("Success!", "Account Updated!");
                    break;
            }

            LootsieEngine.getInstance().updateCurrentActivitiesAndFragments();
        }

        @Override
        public void onUpdateUserAccountFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result) {

            switch(result) {
                case UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_REGISTER_EMAIL:
                    lootsieMessageDialog("Uh oh!", "You’re already logged in!");
                    break;
                case UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE:
                    lootsieMessageDialog("Uh oh!", "Account cannot be updated at this time.");
                    break;
                case UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_INVALID_EMAIL:
                    lootsieMessageDialog("Uh oh!", "Invalid email address.");
                    break;
                default:
                    lootsieMessageDialog("Uh oh!", "Account cannot be updated at this time.");
                    break;
            }

        }
    }

    /**
     * update the user account on server with new settings
     * @param user
     */
    public void updateUserAccount(User user) {
        InternalUpdateUserAccountCallback callback = new InternalUpdateUserAccountCallback();
        updateUserAccount(user,callback);
    }

    /**
     * update the user account on server with new settings
     * @param user
     * @param updateUserAccountCallback
     */
    public void updateUserAccount(User user, IUpdateUserAccountCallback updateUserAccountCallback) {

        if (!sdkInitialized) {
            DebugLog("updateUserAccount: sdk is disabled!");
            return;
        }

        DebugLog("updateUserAccount: %s", user.email);

        try {

            // kick off the UpdateUserAccountSequence
            UpdateUserAccountSequence.start(user, updateUserAccountCallback);
        }catch (Exception e) {
            Logs.e(TAG, "AR:Some Unexpected exception during updateUserAccountSequence()");
            e.printStackTrace();
        }

    }



    public static void onConfigurationChanged(Activity a) {

        try {
//            LootsieManager.onConfigurationChanged(a);
        }catch (Exception e) {
            Logs.e(TAG, "AR:Some Unexpected exception during onConfigurationChanged()");
            e.printStackTrace();
        }

    }

    public Device getDevice() {
        return this.device;
    }

    /**
     * getLogBuffer returns a copy of the debugging buffer of Lootsie sdk to show in a text view
     * @return
     */
    public String getLogBuffer() {
        String str = Logs.getBufferDump();
        return str;
    }

    /**
     * this will register a user's email address and log them in
     */
    public static void registerUserEmail(String emailStr) {
        try {
        	LootsieEngine.getInstance().mergePoints(emailStr);
        }catch (Exception e) {
            Logs.e(TAG, "registerUserEmail: Some Unexpected exception");
            e.printStackTrace();
        }
    }


    /**
     * this will retrieve the session tokens through the api for developer apps which want to notify users later through a webb page
     * ASDK-81 Android SDK Call to give developer session tokens.
     * @param callback
     */
    public static void getSessionTokens(IGetSessionTokens callback) {
        try {
        	if (callback != null) {
        		
        		if ((DataModel.apiSessionToken != null) && (DataModel.userSessionToken != null)) {
	        		String jsonStr = "{\"apiSessionToken\" : \"" + DataModel.apiSessionToken + "\", " +
	        				"\"userSessionToken\" : \"" + DataModel.userSessionToken + "\"}";
	        		callback.onLootsieGetSessionTokensSucess(jsonStr);
        		} else {
        			callback.onLootsieGetSessionTokensFailed();
        		}
        		
        	}
        }catch (Exception e) {
            Logs.e(TAG, "getSessionTokens: Some Unexpected exception");
            e.printStackTrace();
        }
    }

    static class InternalUpdateAchievementsCallback implements IUpdateAchievementsCallback {

		@Override
		public void onUpdateAchievementsSuccess() {
			
//	        Intent intent = new Intent(Lootsie.getInstance().getActivityContext(), AchievementsActivity.class);
//	        Lootsie.getInstance().getActivityContext().startActivity(intent);


	        // ASDK-370 Android-SDK4 Page Border and Info Base Class
	        // ASDK-371 Android-SDK4 Page Border and Info - Native View
	        Intent intent = new Intent(LootsieEngine.getInstance().getActivityContext(), LootsiePageBorderActivity.class);
	        
	        LOOTSIE_NOTIFICATION_CONFIGURATION notifyConfig = LootsieEngine.getInstance().getNotificationConfiguration();
	        DebugLog("UpdateAchievementPageSequence: showAchievementsPage: LOOTSIE_NOTIFICATION_CONFIGURATION: %s", notifyConfig.toString());

	        intent.putExtra("LOOTSIE_NOTIFICATION_CONFIGURATION",notifyConfig.toString());        
	        
	        LootsieEngine.getInstance().getActivityContext().startActivity(intent);
			
		}

		@Override
		public void onUpdateAchievementsFailure() {
			Logs.e(TAG, "LootsieEngine: UpdateAchievementsFailure: Some Unexpected exception");			
		}
    	
    }
    
    
    /**
     * This will display Lootsie full screen window on top of the existing activity
     */
    public static void showRewardsPage(Activity a) {
        if (!sdkInitialized) {
            DebugLog("showRewardsPage: sdk is disabled!");
            return;
        }

        InternalUpdateAchievementsCallback updateAchievementsCallback = new InternalUpdateAchievementsCallback();
        
        if (!LootsieEngine.getInstance().sdkShowing) {
	        try {
	        	LootsieEngine.getInstance().sdkShowing = true;
	        	
	            if (lootsieEngineInstance.achievementCB == null) {
	                lootsieEngineInstance.achievementCB = new InternalAchievementReachedCallback(null);
	            }
	
	            lootsieActivityContext = a;

                //setNotificationConfiguration(LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_rewardsPage);

	            UpdateAchievementPageSequence.start(updateAchievementsCallback);	            
	        }catch (Exception e) {
	            Logs.e(TAG, "AR:Some Unexpected exception");
	            e.printStackTrace();
	            
	            LootsieEngine.getInstance().sdkShowing = false;
	        }
        } else {
        	DebugLog("showAchievementsPage: sdk is showing!");
        	return;
        }
    }

    static class InternalUpdateAchievementsCallbackToAchievementsPage implements IUpdateAchievementsCallback {

		@Override
		public void onUpdateAchievementsSuccess() {
	        // ASDK-370 Android-SDK4 Page Border and Info Base Class
	        // ASDK-371 Android-SDK4 Page Border and Info - Native View
	        Intent intent = new Intent(LootsieEngine.getInstance().getActivityContext(), LootsiePageBorderActivity.class);
	        
	        LOOTSIE_NOTIFICATION_CONFIGURATION notifyConfig = LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_achievementsPage;
	        intent.putExtra("LOOTSIE_NOTIFICATION_CONFIGURATION",notifyConfig.toString());        

	        LootsieEngine.getInstance().getActivityContext().startActivity(intent);
			
		}

		@Override
		public void onUpdateAchievementsFailure() {
			Logs.e(TAG, "LootsieEngine: UpdateAchievementsFailure: Some Unexpected exception");			
		}
    	
    }
    
    /**
     * This will display Lootsie full screen window on top of the existing activity
     */
    public static void showAchievementsPage(Activity a) {
        if (!sdkInitialized) {
            DebugLog("showAchievementsPage: sdk is disabled!");
            return;
        }

        InternalUpdateAchievementsCallbackToAchievementsPage callback = new InternalUpdateAchievementsCallbackToAchievementsPage();

        if (!LootsieEngine.getInstance().sdkShowing) {
	        try {
	        	LootsieEngine.getInstance().sdkShowing = true;
	        	
	            if (lootsieEngineInstance.achievementCB == null) {
	                lootsieEngineInstance.achievementCB = new InternalAchievementReachedCallback(null);
	            }
	
	            lootsieActivityContext = a;
	
                //setNotificationConfiguration(LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_achievementsPage);

	            // verify if user got updated achievement, get user achievements again
	            UpdateAchievementPageSequence.start(callback);
	
	        }catch (Exception e) {
	            Logs.e(TAG, "AR:Some Unexpected exception");
	            e.printStackTrace();
	            
	            LootsieEngine.getInstance().sdkShowing = false;
	        }
        } else {
        	DebugLog("showAchievementsPage: sdk is showing!");
        	return;
        }
    }


    public IAchievementReached getCallback() {
        if (lootsieEngineInstance.achievementCB == null) {
            lootsieEngineInstance.achievementCB = new InternalAchievementReachedCallback(null);
        }

        return lootsieEngineInstance.achievementCB;
    }


    /**
     * This will display Lootsie badge on top of the existing activity
     */
    public static void showLootsieButton(Activity a) {
        try {
//            LootsieManager.showLootsieButton(a);
        }catch (Exception e) {
            Logs.e(TAG, "AR:Some Unexpected exception");
            e.printStackTrace();
        }
    }

    /**
     * this will remove the lootsie badge from on top of the activity
     * @param a
     */
    public static void hideLootsieButton(Activity a) {
        try {
//            LootsieManager.hideLootsieButton(a);
        }catch (Exception e) {
            Logs.e(TAG, "AR:Some Unexpected exception");
            e.printStackTrace();
        }
    }


    // this method will release the lootsie resources gracefully
    /**
     * This method releases all lootsie resources gracefully. call it on
     * application ondestroy method
     *
     */
    public static void onDestroy() {
        try {

            DebugLog("onDestroy");

            // ASDK-356 Android-SDK4 sendUserActivity
            // metrics: start session time
            TrackingSessionAsyncTask task = new TrackingSessionAsyncTask();
            task.execute("onDestroy");


            // ASDK-366
            // stop the LootsieJSCommandQueue
            if (commandThread != null) {
                DebugLog("Stop command queue");

                // cleanup ending of thread
                runnableQueue.finishQueue();
                commandThread = null;
            }

            DataModel.saveNetworkStats();

        }catch (Exception e) {
            Logs.e(TAG, "ondestroy:Some Unexpected exception");
            e.printStackTrace();
        }
    }


    /**
     * This method sets the requested Rendering mode to display Lootsie
     * Rewards window. Use this method to lock down portrait or landscape only
     *
     * @param requestedState - state to lock the screen
     * 			 Lootsie.RENDER_PORTRAIT - To render Portrait locked screen
     * 			 Lootsie.RENDER_LANDSCAPE - To render Landscape locked screen
     * 			 Lootsie.RENDER_DEFAULT - To render with default automated screen
     */
    // ASDK-354 Android-SDK4 LEG says use portrait or landscape - setRenderingMode - optional portrait, landscape or automatic mode
    // ASDK-363 Android-SDK4 lock orientation of android activity view based on rendering mode
    public static void setRenderingMode(int requestedState)
    {
        try {
            if(screenOrientationMgr !=null)
            {
                screenOrientationMgr.setRequestedScreenOrientation(requestedState);
            }
        }catch (Exception e) {
            Logs.e(TAG, "rendering:Some Unexpected exception");
            e.printStackTrace();
        }
        return;
    }

    /**
     *  Get the saved Screen Orientation
     */
    public int getScreenOrientation()
    {
        if(screenOrientationMgr !=null)
        {
            return screenOrientationMgr.getOrientationState();
        }
        // default
        Logs.e(TAG, "Orientation class is not running. never happen ever !!");
        return ScreenOrientationManager.SCREEN_AUTO;
    }

    public ScreenOrientationManager getScreenOrientationManager() {
        return screenOrientationMgr;
    }


    // setConfiguration:
    // allows ANDSDK-42 [Discovered, Platform] Adobe AIR Support
    public static void setConfiguration(LOOTSIE_CONFIGURATION requestedConfiguration) {
        try {

            switch (requestedConfiguration) {
                // ASDK-400 ASDK-4 Remove GPS tracking from SDK layer, and put it in app layer as an option
                // lootsie engine should no longer care about this configuration!
//                case ENABLE_GPS:
//                    gpsEnabled = true;
//                    break;
//
//                case DISABLE_GPS:
//                    gpsEnabled = false;
//                    break;

                case ENABLE_CACHE:
                    DataModel.networkCacheEnabled = true;
                    break;

                case DISABLE_CACHE:
                    DataModel.networkCacheEnabled = false;
                    break;

                case ENABLE_IN_GAME_HOOK_WINDOW:
                    // TODO: enable IAN
                    break;

                case DISABLE_IN_GAME_HOOK_WINDOW:
                    // TODO: disable IAN
                    break;


            };


        }catch (Exception e) {
            Logs.e(TAG, "rendering:Some Unexpected exception");
            e.printStackTrace();
        }
        return;

    }

    public static void setNotificationConfiguration(LOOTSIE_NOTIFICATION_CONFIGURATION inputNotificationConfig) {
    	
    	DebugLog("LootsieEngine: setNotificationConfiguration:" + inputNotificationConfig.toString());
    	
    	LootsieEngine.getInstance().notificationConfiguration = inputNotificationConfig;
    	
    }
    
    public LOOTSIE_NOTIFICATION_CONFIGURATION getNotificationConfiguration() {
    	return this.notificationConfiguration;
    }
    
    
    
    public LootsieAccountManager getLootsieAccountManager() {
        return this.lootsieAccountManager;
    }

    public void setAppId(String inputAppId) {
        appId = inputAppId;
    }

    public String getAppId() {
        return this.appId;
    }

    private static void setApplicationKey(String appKey) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(LootsieEngine.lootsieAppcontext);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(LootsieEngine.APP_KEY,appKey);
        editor.commit();
    }

    public static LootsieEngine getInstance() {
        return lootsieEngineInstance;
    }

    public android.content.Context getApplicationContext() {
        return lootsieAppcontext;
    }

    public android.content.Context getActivityContext() {
        return lootsieActivityContext;
    }

    /**
     * needed for updating the current app with push notification messages
     * when app activity changes on rotation, etc
     * @param inputActivity
     */
    public void setActivityContext(Activity inputActivity) {
        DebugLog("setActivityContext");

        lootsieActivityContext = inputActivity;
    }

    public static void setCacheDurationInSeconds(int cacheDurationSec) {
        DebugLog("setCacheDurationInSeconds: %d", cacheDurationSec);
        LootsieEngine.getInstance().cacheDuration = cacheDurationSec;

    }

    public int getCacheDuration() {
        return cacheDuration;
    }


    public static void setLocation(float latitude, float longitude) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("latlng", String.format(Locale.getDefault(), "%f,%f",
                    latitude, longitude));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        LootsieEngine.getInstance().locationStr = String.format(Locale.getDefault(), "%f,%f",
                latitude, longitude);

        // save the location in the data model somewhere?
        LootsieEngine.getInstance().setLocation(obj);

    }

    public static String getLocationStr() {
        return locationStr;
    }


    // location update from LocationUpdates to the Lootsie API server
    private boolean setLocation(JSONObject json) {

        // {"location","xx,xx"}
        // { "latlng": "30.456456,-150.39482" }

        // make sure Headers.SESSION_TOKEN, Headers.USER_TOKEN, Headers.APP_SECRET are set properly in RestClient
        // sometimes the GPS is ready before the tokens are here, so add it to the init sequence
        if ((DataModel.userSessionToken == null) || (DataModel.apiSessionToken == null)) {
            Logs.e(TAG, "setLocation failure - user session token or api session token is invalid!");

            InitSequence.setLocation(json.toString());
            return false;
        }



        // have we sent a locaiton update within the LootsieGlobals.NETWORK_CACHE_TIME_SECONDS timeout?
        if (DataModel.updateNetwork(LootsieApi.USER_LOCATION_URL.toString())) {
            // TODO: create a post call which uses json object directly, rather than switching from json to string, then to json again!
            new RESTPostUserLocationTask().execute(json.toString());

            DataModel.saveNetworkStats();
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method returns the currently running Application context
     */
    public Application getApp() {
        return lootsieEngineInstance.lootsieAppcontext;
    }

    public User getUserAccount() {
        if (!sdkInitialized) {
            DebugLog("getUserAccount: sdk is disabled!");
            return null;
        }    	
    	
    	return DataModel.user;
    }
    
    // put updating activities into a generic interface which Lootsie has to implement
    // IUpdatableActivity
    // IUpdatableListActivity
    // IUpdatableListFragment
    // IUpdatableFragment

    // updating activities
    public synchronized void setUpdatable(UpdatableActivity updatable) {
        this.currentActivity = updatable;
    }

    public Activity getCurrentActivity() {
        return this.currentActivity;
    }

    // updating activities
    public synchronized void updateCurrentActivity() {
        if (currentActivity != null)
            currentActivity.updateActivityFromBgThread();
    }

    // updating activities
    public synchronized void setUpdatableListActivity(UpdatableListActivity updatable) {
        this.currentListActivity = updatable;
    }

    // updating activities
    public synchronized void updateCurrentListActivity() {
        if (currentListActivity != null)
            currentListActivity.updateActivityFromBgThread();
    }

    // updating activities
    public synchronized void setUpdatableListFragment(UpdatableListFragment updatable, String key) {
        DebugLog("setUpdatableListFragment: %s", key);
        //this.currentListFragment = updatable;
        if (!listFragmentMap.containsKey(key)) {
            listFragmentMap.put(key, updatable);
        }
    }

    public synchronized void unsetUpdatableListFragment(String key) {
        DebugLog("unsetUpdatableListFragment: %s", key);
        //this.currentListFragment = null;
        if (listFragmentMap.containsKey(key)) {
            listFragmentMap.remove(key);
        }
    }

    // updating activities
    public synchronized void updateCurrentListFragment() {
//        if (currentListFragment != null)
//            currentListFragment.updateFragmentFromBgThread();

        for (String key : listFragmentMap.keySet()) {
            DebugLog("updateCurrentListFragment: %s", key);
            listFragmentMap.get(key).updateFragmentFromBgThread();
        }

    }

    // updating activities
    public synchronized void setUpdatableFragment(UpdatableFragment updatable) {
        this.currentFragment = updatable;
    }

    // updating activities
    public synchronized void updateCurrentFragment() {
        if (currentFragment != null)
            currentFragment.updateFragmentFromBgThread();
    }

    // updating activities
    public synchronized void updateCurrentActivitiesAndFragments() {
        DebugLog("updateCurrentActivitiesAndFragments");

        updateCurrentActivity();
        updateCurrentListActivity();
        updateCurrentListFragment();
        updateCurrentFragment();
    }

    // ASDK-356 Android-SDK4 sendUserActivity
    public void onPause() {
        DebugLog("onPause");

        // metrics session end time
        TrackingSessionAsyncTask task = new TrackingSessionAsyncTask();
        task.execute("onPause");


    }

    // ASDK-356 Android-SDK4 sendUserActivity
    public void onResume() {
        DebugLog("onResume");

        // metrics: start session time
        TrackingSessionAsyncTask task = new TrackingSessionAsyncTask();
        task.execute("onResume");

    }

    public void setSDKShowing(boolean inputValue) {
    	DebugLog("setSDKShowing: " + inputValue);
    	
    	LootsieEngine.getInstance().sdkShowing = inputValue;
    }

    public static void lootsieMessageDialog(String titleStr, String messageStr) {

        if (LootsieEngine.getInstance().getActivityContext() != null) {
            MessageView messageView = new MessageView(LootsieEngine.getInstance().getActivityContext(),
                    LootsieEngine.getInstance().getCurrentActivity().getCurrentFocus(),
                    titleStr, messageStr);
        } else {
            Logs.v(TAG, "message: " + titleStr + " " + messageStr);
        }
    }

    /**
     * safely call toast methods to avoid errors with null references to activity context
     * @param text
     * @param duration
     */
//    private static void SafeToastMakeText(CharSequence text, int duration) {
//    	//public static Toast makeText(Context context, CharSequence text, int duration)
//        if (LootsieEngine.getInstance().getActivityContext() != null) {
//        	Toast.makeText(LootsieEngine.getInstance().getActivityContext(), text, duration).show();
//        }
//    }
    
    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (com.lootsie.sdk.utils.LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}
