package com.lootsie.sdk.model;

import com.lootsie.sdk.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by jerrylootsie on 2/11/15.
 */
/*
    {
       "achievements": [
           {
               "id": "l5_5stars",
               "date": "2012-10-12T13:29:25",
               "lp": 50
           }
       ],
       "achieved_lp": 50,
       "total_lp": 150
    }
 */
public class UserAchievements {

    private static String TAG = "Lootsie UserAchievements";

    public ArrayList<Achievement> achievements = new ArrayList<Achievement>();
    public int achieved_lp = 0;
    public int total_lp = 0;

    public void parseFromJSON(JSONObject json) throws JSONException {

        achieved_lp = json.getInt("achieved_lp");
        DebugLog("achieved_lp %d", achieved_lp);

        total_lp = json.getInt("total_lp");
        DebugLog("total_lp %d", total_lp);

        JSONArray achievementsArr = json.getJSONArray("achievements");
        achievements = new ArrayList<Achievement>();

        int length = achievementsArr.length();
        for (int i = 0; i < length; i++) {
            try {
                JSONObject obj = achievementsArr.getJSONObject(i);

                Achievement a = new Achievement();
                a.parseFromJSON(obj);

                achievements.add(a);

                DebugLog("achievement[%d] %s", i, a.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



    }


    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (com.lootsie.sdk.utils.LootsieGlobals.debugLevel > 0) {
           Logs.v(TAG, String.format(msg, args));
        }
    }
}
