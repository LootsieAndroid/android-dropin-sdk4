package com.lootsie.sdk.callbacks;

/**
 * Created by jerrylootsie on 2/9/15.
 */
public interface IAchievementReached {


    /**
     *  Lootsie will call back this developer implemented method after user navigates
     *  out of the Lootsie Window
     */
    public void onLootsieBarClosed();

    /**
     *  Lootsie will call back this developer implemented method after user clicks
     *  on Lootsie in-hook Window to navigate Lootsie system offers
     */
    public void onLootsieBarExpanded();

    /**
     *  Lootsie will call back this developer implemented method when Lootsie
     *  failed to display its in-hook window
     */
    public void onLootsieFailed(String mesg);


    /**
     *  Lootsie will call back this developer implemented method when Lootsie
     *  successfully received confirmation back before displaying the in-game
     *  hook
     */
    public void onLootsieSuccess();

    /**
     *  Lootsie will call back this developer implemented method when Lootsie
     *  successfully received data back from the in-game-hook / In-App-Notification
     *  Only used for when configuration disable IAN:
     *  Lootsie.setConfiguration(LOOTSIE_CONFIGURATION.DISABLE_IN_GAME_HOOK_WINDOW);
     */
    // ASDK-336 create a path to retrieve IAN data when IAN gui is disabled for whitelabel solutions
    public void onNotificationData(String json);
}