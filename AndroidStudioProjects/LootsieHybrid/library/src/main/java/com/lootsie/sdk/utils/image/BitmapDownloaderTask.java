package com.lootsie.sdk.utils.image;

/**
 * Created by jerrylootsie on 7/27/15.
 */

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.lootsie.sdk.utils.LootsieGlobals;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

/**
 * The actual AsyncTask that will asynchronously download the image.
 */
class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {

    public String url;
    private final WeakReference<ImageView> imageViewReference;

    private static String TAG = "BitmapDownloaderTask";

    // 4 * 1024 * 1024 = 4194304 ~ 4mb
    //private final BitmapLruCache cache = new BitmapLruCache(4 * 1024 * 1024);
    //private static BitmapLruCache cache = null;

    private AsyncImageDownloader.Mode mode = AsyncImageDownloader.Mode.NO_ASYNC_TASK;
    private AsyncImageDownloader asyncImageDownloader = null;

    public BitmapDownloaderTask(ImageView imageView) {
        imageViewReference = new WeakReference<ImageView>(imageView);
    }

    @SuppressLint("NewApi")
    Bitmap downloadBitmap(String url, AsyncImageDownloader.Mode inputMode) {
        final int IO_BUFFER_SIZE = 4 * 1024;

        mode = inputMode;

        // AndroidHttpClient is not allowed to be used from the main thread
        final HttpClient client = (mode == AsyncImageDownloader.Mode.NO_ASYNC_TASK) ? new DefaultHttpClient() :
                AndroidHttpClient.newInstance("Android");
        final HttpGet getRequest = new HttpGet(url);

        if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "AsyncImageDownloader: downloadBitmap: url: \"" + url + "\"");

        try {
            HttpResponse response = client.execute(getRequest);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                Log.w("ImageDownloader", "Error " + statusCode +
                        " while retrieving bitmap from " + url);
                return null;
            }

            final HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = null;
                try {
                    inputStream = entity.getContent();
                    // return BitmapFactory.decodeStream(inputStream);
                    // Bug on slow connections, fixed in future release.
                    return BitmapFactory.decodeStream(new FlushedInputStream(inputStream));
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    entity.consumeContent();
                }
            }
        } catch (IOException e) {
            getRequest.abort();
            Log.w(TAG, "I/O error while retrieving bitmap from " + url, e);
        } catch (IllegalStateException e) {
            getRequest.abort();
            Log.w(TAG, "Incorrect URL: " + url);
        } catch (Exception e) {
            getRequest.abort();
            Log.w(TAG, "Error while retrieving bitmap from " + url, e);
        } finally {
            if ((client instanceof AndroidHttpClient)) {
                ((AndroidHttpClient) client).close();
            }
        }
        return null;
    }

    // this would be useful if we want to scale down loaded images by at least a power of 2
    static Bitmap decodeStreamToMaxSize(InputStream inputStream,int WIDTH,int HIGHT){

        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(inputStream,null,o);

        //The new size we want to scale to
        final int REQUIRED_WIDTH=WIDTH;
        final int REQUIRED_HIGHT=HIGHT;
        //Find the correct scale value. It should be the power of 2.
        int scale=1;
        while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HIGHT)
            scale*=2;

        if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "AsyncImageDownloader: decodeStreamToMaxSize: scale: " + scale);

        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize=scale;
        return BitmapFactory.decodeStream(inputStream, null, o2);

    }


    public void setAsyncImageDownloader(AsyncImageDownloader parentDownloader) {
        asyncImageDownloader = parentDownloader;
    }


    /**
     * Actual download method.
     */
    @Override
    protected Bitmap doInBackground(String... params) {
        url = params[0];
        return downloadBitmap(url, mode);
    }

    /**
     * Once the image is downloaded, associates it to the imageView
     */
    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            bitmap = null;
        }


        if ((LootsieGlobals.debugLevel > 0) && (bitmap != null)){
            Log.w(TAG, "AsyncImageDownloader: BitmapDownloaderTask: onPostExecute: bitmap: " +
                    bitmap.getWidth() + "x" + bitmap.getHeight());
        }


        asyncImageDownloader.addBitmapToCache(url, bitmap);

        if (imageViewReference != null) {
            ImageView imageView = imageViewReference.get();

            // Why do this again????
            //BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);

            // Change bitmap only if this process is still associated with it
            // Or if we don't use any bitmap to task association (NO_DOWNLOADED_DRAWABLE mode)
//            if ((bitmap != null) && (imageView != null) &&
//                    ((this == bitmapDownloaderTask) || (mode != AsyncImageDownloader.Mode.CORRECT))) {

            if ((bitmap != null) && (imageView != null)) {
                imageView.clearAnimation();
                imageView.setImageBitmap(bitmap);
            }
        } else {
            Log.e(TAG, "AsyncImageDownloader: BitmapDownloaderTask: onPostExecute: imageViewReference is invalid!");
        }
    }


    /*
        * An InputStream that skips the exact number of bytes provided, unless it reaches EOF.
        */
    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int b = read();
                    if (b < 0) {
                        break;  // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }
}
