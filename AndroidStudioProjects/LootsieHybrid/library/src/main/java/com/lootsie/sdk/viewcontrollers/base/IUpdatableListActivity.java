package com.lootsie.sdk.viewcontrollers.base;

/**
 * Created by jerrylootsie on 3/2/15.
 */
public interface IUpdatableListActivity {
    public void setUpdatableListActivity(UpdatableListActivity updatable);
    public void updateCurrentListActivity();
}
