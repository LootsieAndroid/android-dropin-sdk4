package com.lootsie.sdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.lootsie.sdk.utils.Logs;

import org.json.JSONObject;
import org.json.JSONException;

/**
 * Icon
 *
 * @author Aliaksandr Salnikau
 */
public class Icon implements Parcelable{
    public String S;
    public String M;
    public String L;
    public Icon() {
    }

    private static String TAG = "Lootsie Icon";

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(S);
        out.writeString(M);
        out.writeString(L);
    }

    public static final Parcelable.Creator<Icon> CREATOR
            = new Parcelable.Creator<Icon>() {
        public Icon createFromParcel(Parcel in) {
            return new Icon(in);
        }

        public Icon[] newArray(int size) {
            return new Icon[size];
        }
    };

    private Icon(Parcel in) {
        S = in.readString();
        M = in.readString();
        L = in.readString();
    }

    public void parseFromJSON(JSONObject images) throws JSONException {

        try {
            this.S = images.getString("S");
            this.M = images.getString("M");
            this.L = images.getString("L");
        } catch (JSONException ex) {
            Logs.e(TAG, "Icon: parseFromJSON exception: " + ex.getMessage());
            throw ex;
        }

    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("S", S);
            jsonObj.put("M", M);
            jsonObj.put("L", L);
        } catch (JSONException ex) {
            Logs.e(TAG, "Icon: toJSON exception: " + ex.getMessage());
            throw ex;
        }

        return jsonObj;
    }

}
