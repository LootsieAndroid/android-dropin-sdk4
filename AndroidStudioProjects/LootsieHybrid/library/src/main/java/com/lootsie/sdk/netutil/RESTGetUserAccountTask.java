package com.lootsie.sdk.netutil;

import android.os.AsyncTask;
//import android.util.Log;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;


/**
 * Created by jerrylootsie on 2/10/15.
 */

/*
    GET https://api-v2.lootsie.com/v2/user/account
    X-Lootsie-App-Secret
    X-Lootsie-User-Session-Token

 */
//public class RESTGetUserAccountTask extends AsyncTask< String, Void, RestResult> {
public class RESTGetUserAccountTask extends AsyncTask< String, Void, RestResult> implements IGenericAsyncTask<String> {

    private final static String TAG = "Lootsie.RESTGetUserAccountTask";

    IRESTCallback callback = null;

    public RESTGetUserAccountTask() {}

    public RESTGetUserAccountTask(IRESTCallback restCallback) {
        callback = restCallback;
    }

    private RestResult processError(Exception e) {
        RestResult response = new RestResult();
        String errorResponse = e.getMessage();
        if (errorResponse == null) {
            e.printStackTrace();
        } else {
//            response.status = e.code;
            Logs.e(TAG, "RESTGetUserAccountTask: processError: getMessage: " + e.getMessage());
        }
        return response;
    }

    @Override
    protected RestResult doInBackground( String ... params) {

//        if (params == null || params.length == 0) {
//            return null;
//        }

        RestResult response = new RestResult();
        try {

            response.content = (String) RestClient.doGet(LootsieApi.USER_ACCOUNT_URL.getUri(), null);
            response.status = 200;

            if (LootsieGlobals.debugLevel > 0) Logs.v(TAG, "RESTGetUserAccountTask:" + response.content);

        } catch (Exception e) {
            Logs.e(TAG, "RESTGetUserAccountTask: processError: getMessage: " + e.getMessage());
        }
        return response;
    }



    @Override
    protected void onPostExecute(final RestResult restResult) {
        if (callback != null) {
            callback.restResult(restResult);
        }
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        execute(params);
    }
}