package com.lootsie.sdk.model;

/**
 * Created by jerrylootsie on 2/11/15.
 */

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Achievement
 *
 * @author Aliaksandr Salnikau
 */
public class Achievement implements Parcelable{
    public String id;
    public String name;
    public String description;
    public String date;
    public int lp;
    public boolean repeatable;

    public static final Parcelable.Creator<Achievement> CREATOR
            = new Parcelable.Creator<Achievement>() {
        public Achievement createFromParcel(Parcel in) {
            return new Achievement(in);
        }

        public Achievement[] newArray(int size) {
            return new Achievement[size];
        }
    };

    public Achievement() {
    }

    // userachievemnts versus application achievements parsing?
    // GET /user/achievements only gives date, id, lp (not name!)
    // GET /app gives id, name, lp (not date!)
    public void parseFromJSON(JSONObject json) throws JSONException {
        if (json.has("id")) this.id = json.getString("id");
        if (json.has("name")) this.name = json.getString("name");
        if (json.has("date")) this.date = json.optString("date");
        if (json.has("lp")) this.lp = json.getInt("lp");
        if (json.has("description")) this.description = json.getString("description");
        if (json.has("repeatable")) this.repeatable = json.getBoolean("repeatable");
    }

    private Achievement(Parcel in) {
        id = in.readString();
        name = in.readString();
        date = in.readString();
        lp = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(date);
        dest.writeInt(lp);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Achievement that = (Achievement) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public JSONObject toJson() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("id", id);
        obj.put("name", name);
        obj.put("date", date);
        obj.put("lp", lp);
        obj.put("description", description);
        obj.put("repeatable", repeatable);
        return obj;
    }

    public String toString() {
        String str = "";

        str += "id: " + id;
        str += " name: " + name;
        str += " date: " + date;
        str += " lp: " + lp;
        str += " repeatable: " + repeatable;

        return str;
    }

}