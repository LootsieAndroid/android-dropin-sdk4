package com.lootsie.sdk.callbacks;

/**
 * Created by jerrylootsie on 2/9/15.
 */
public interface IInitializationCallback {
    public void onInitSuccess();
    public void onInitFailure();
}
