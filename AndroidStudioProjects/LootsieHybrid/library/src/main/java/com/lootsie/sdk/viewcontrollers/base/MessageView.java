package com.lootsie.sdk.viewcontrollers.base;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.lootsie.sdk.model.RewardListEntryModel;
import com.lootsie.sdk.model.RewardViewHolder;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.RUtil;

/**
 * Created by jerrylootsie on 8/7/15.
 */
public class MessageView {

//    public MessageView(Context context) {
//        super(context);
//    }

    private static String TAG = "MessageView";

    private final Context context;

    private PopupWindow messagePopup = null;
    private PopupWindow dimPopup = null;

    private static int mWidth = 0;
    private static int mHeight = 0;

    public MessageView(Context context, View parentView, String titleStr, String messageStr) {
        this.context = context;

        DebugLog("MessageView");

        dimPopup = dimBackground();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View pview;
        //pview = inflater.inflate(R.layout.com_lootsie_reward_redeem, null);
        pview = inflater.inflate(RUtil.getLayoutId(context, "com_lootsie_message"), null);

        messagePopup = new PopupWindow(pview, ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, true);


        messagePopup.showAtLocation(parentView, Gravity.CENTER, 0, 0);
        messagePopup.setOutsideTouchable(false); // don't allow touches outside to dismiss it
        messagePopup.setFocusable(true);
        //cp.update(0, 0, 500, 350);

        TextView titleText = (TextView) pview.findViewById(RUtil.getResourceId(context, "messageTitle", "id", context.getPackageName()));
        if (titleText != null) {
            titleText.setText(titleStr);
        }

        TextView contentText = (TextView) pview.findViewById(RUtil.getResourceId(context, "messageContent", "id", context.getPackageName()));
        if (contentText != null) {
            contentText.setText(messageStr);
        }

        Button cancelButton = (Button) pview.findViewById(RUtil.getResourceId(context, "okButton", "id", context.getPackageName()));
        if (cancelButton != null) {
            cancelButton.setOnClickListener(new CancelButtonListener());

        }
    }

    class CancelButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View arg0) {

            DebugLog("onClickCancel:");

            if (messagePopup != null) messagePopup.dismiss();
            if (dimPopup != null) dimPopup.dismiss();
        }
    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static void getDisplayDimensions_HoneyComb(Display display) {
        Rect displaySizeRect = new Rect();
        display.getRectSize(displaySizeRect);

        mWidth = displaySizeRect.width();
        mHeight = displaySizeRect.height();
    }

    private PopupWindow dimBackground() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View pview = inflater.inflate(RUtil.getLayoutId(context, "com_lootsie_fadepopup"), null);

        // fadePopup is the id of LinearLayout inside layout/com_lootsie_fadepopup
        View layout = pview.findViewById(RUtil.getResourceId(context, "fadePopup", "id", context.getPackageName()));


        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getDisplayDimensions_HoneyComb(display);
        } else {
            mWidth = display.getWidth();
            mHeight = display.getHeight();
        }


        PopupWindow fadePopup = new PopupWindow(layout, mWidth, mHeight, false);
        if (fadePopup != null) {
            fadePopup.showAtLocation(layout, Gravity.NO_GRAVITY, 0, 0);
        } else {
            Logs.e(TAG, "dimBackground: fadePopup is null!");
        }
        return fadePopup;
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}
