package com.lootsie.sdk.model;

/**
 * Created by jerrylootsie on 2/11/15.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.lootsie.sdk.utils.Logs;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

/**
 * Reward
 *
 * @author Aliaksandr Salnikau
 */
public class Reward implements Parcelable {
    public String id;
    public String name;
    public String textToShare;
    public String tos_text;
    public String description;
    public int lp;
    public Icon icon = new Icon();
    public ArrayList<Engagement> engagements = new ArrayList<Engagement>(2);

    private static String TAG = "Lootsie Reward";

    public Reward() {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(name);
        out.writeString(description);
        out.writeString(textToShare);
        out.writeString(tos_text);
        out.writeInt(lp);
        out.writeParcelable(icon, flags);
        out.writeTypedList(engagements);
    }

    public static final Parcelable.Creator<Reward> CREATOR
            = new Parcelable.Creator<Reward>() {
        public Reward createFromParcel(Parcel in) {
            return new Reward(in);
        }

        public Reward[] newArray(int size) {
            return new Reward[size];
        }
    };

    private Reward(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        textToShare = in.readString();
        tos_text = in.readString();
        lp = in.readInt();
        icon = in.readParcelable(Icon.class.getClassLoader());
        in.readTypedList(engagements, Engagement.CREATOR);
    }

    // moved parsing from RewardManager
    /*
    "id": "4",
    "name": "Free Venti Coffee!",
    "description": "",
    "lp": 2000,
    "tos_url": "https://www-stage1.lootsie.com/mp/rewards/4/terms-of-service",
    "tos_text": "Lorem ipsum...",
    "text_to_share": "I just got Free Venti Coffee! at Starbucks from @Lootsie by playing my favorite games!",
    "image_urls": {
        "S": "https://lootsie.s3.amazonaws.com/stage1/ap-image-s/1351080030677..png",
        "M": null,
        "L": null
    },
    "engagements": [
        {
            "id": "12",
            "name": "Facebook Share",
            "lp": 5
        },
        {
            "id": "14",
            "name": "Twitter Share",
            "lp": 5
        }
    ]
    */
    public void parseFromJSON(JSONObject o) throws JSONException {

        try {
            this.id = o.getString("id");
            this.name = o.getString("name");
            this.description = o.getString("description");
            this.lp = o.getInt("lp");
            this.textToShare = o.getString("text_to_share");
            this.tos_text = o.getString("tos_text");

            JSONObject images = o.getJSONObject("image_urls");
            if (images != null) {
                this.icon.parseFromJSON(images);
            }

            JSONArray engagementsArr = o.getJSONArray("engagements");
            if (engagementsArr != null && engagementsArr.length() > 0) {
                for (int j = 0; j < engagementsArr.length(); j++) {
                    JSONObject oo = engagementsArr.getJSONObject(j);
                    Engagement engagement = new Engagement();
                    engagement.parseFromJSON(oo);
                    this.engagements.add(engagement);
                }
            }

        } catch (JSONException ex) {
            Logs.e(TAG, "Reward: parseFromJSON exception: " + ex.getMessage());
            throw ex;
        }
    }
    
    // ASDK-441 test empty rewards list
    public void generateTestReward() {
    	
        this.id = "-1";
        this.name = "Generic Test Reward";
        this.description = "Your Reward List is empty.  Lootsie Rewards may be unavailable in your area.";
        this.lp = 0;
        this.textToShare = "Share Lootsie Rewards!";
        this.tos_text = "Terms of Service are not applicable for test rewards.";
        
    	// icon should contain empty images
    }

    public JSONObject toJSON() throws JSONException {

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("id",id);
            jsonObj.put("name",name);
            jsonObj.put("description", description);
            jsonObj.put("lp", lp);
            jsonObj.put("text_to_share", textToShare);
            jsonObj.put("tos_text", tos_text);

            JSONObject images = this.icon.toJSON();
            jsonObj.put("image_urls", images);

            JSONArray engagementsArr = new JSONArray();
            if (engagements != null && engagements.size() > 0) {
                for (int j = 0; j < engagements.size(); j++) {
                    Engagement engagement = engagements.get(j);
                    JSONObject engagmentObj = engagement.toJSON();

                    engagementsArr.put(j, engagmentObj);
                }

            }
            jsonObj.put("engagements", engagementsArr);
        } catch (JSONException ex) {
            Logs.e(TAG, "Reward: toJSON exception: " + ex.getMessage());
            throw ex;
        }

        return jsonObj;
    }


    @Override
    public String toString() {
        String str = "";

        str += "id: " + id;
        str += " name: " + name;
        str += " lp: " + lp;

        return str;
    }
        
        
}
