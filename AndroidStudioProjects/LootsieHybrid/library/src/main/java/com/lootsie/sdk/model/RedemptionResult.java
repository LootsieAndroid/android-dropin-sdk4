package com.lootsie.sdk.model;

import com.lootsie.sdk.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by jerrylootsie on 2/23/15.
 */

/*
    {"total_lp": 659, "reward_redemption_id": "5032", "lp": -5}
 */
public class RedemptionResult {

    public int totalLp = 0;
    public int rewardRedemptionId = 0;
    public int lp = 0;

    private static String TAG = "Lootsie RedemptionResult";

    public void print() {

        DebugLog("RedemptionResult: totalLp: %d", totalLp);
        DebugLog("RedemptionResult: rewardRedemptionId: %d", rewardRedemptionId);
        DebugLog("RedemptionResult: lp: %d", lp);

    }


    public void parseFromJSON(JSONObject json) throws JSONException {

        this.totalLp = json.getInt("total_lp");
        this.rewardRedemptionId = json.getInt("reward_redemption_id");
        this.lp = json.getInt("lp");

    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (com.lootsie.sdk.utils.LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }



}
