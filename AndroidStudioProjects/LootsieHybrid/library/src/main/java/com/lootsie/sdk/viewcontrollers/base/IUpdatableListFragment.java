package com.lootsie.sdk.viewcontrollers.base;

/**
 * Created by jerrylootsie on 3/2/15.
 */
public interface IUpdatableListFragment {
    public void setUpdatableListFragment(UpdatableListFragment updatable, String key);
    public void unsetUpdatableListFragment(String key);
    public void updateCurrentListFragment();
}
