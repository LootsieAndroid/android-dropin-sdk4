package com.lootsie.sdk.utils;

import android.R;
import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.TextAppearanceSpan;

/**
 * CustomTypefaceSpan
 *
 * @author Alexander Salnikov
 */
public class CustomTypefaceSpan extends TextAppearanceSpan {

    private final Typeface newType;


    public CustomTypefaceSpan(String family, int style, int size,
                              ColorStateList color, ColorStateList linkColor, Typeface type) {
        super(family, style, size, color, linkColor);

        newType = type;
    }

    public CustomTypefaceSpan(Typeface type) {
        super("normal", R.style.TextAppearance_Medium, 20, null, null);
        newType = type;
    }

    public CustomTypefaceSpan(String family, Typeface type) {
        super(family, android.R.style.TextAppearance_Medium, 20, null, null);
        newType = type;

    }

    @Override
    public void updateDrawState(TextPaint ds) {
        applyCustomTypeFace(ds, newType);
    }

    @Override
    public void updateMeasureState(TextPaint paint) {
        applyCustomTypeFace(paint, newType);
    }

    private static void applyCustomTypeFace(Paint paint, Typeface tf) {
        int oldStyle;
        Typeface old = paint.getTypeface();
        if (old == null) {
            oldStyle = 0;
        } else {
            oldStyle = old.getStyle();
        }

        int fake = oldStyle & ~tf.getStyle();
        if ((fake & Typeface.BOLD) != 0) {
            paint.setFakeBoldText(true);
        }

        if ((fake & Typeface.ITALIC) != 0) {
            paint.setTextSkewX(-0.25f);
        }

        paint.setTypeface(tf);
    }
}