package com.lootsie.sdk.netutil;

import android.os.AsyncTask;
//import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.PreferencesHandler;

import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.utils.LootsieGlobals;

/**
 * Created by jerrylootsie on 3/4/15.
 */
//public class TrackingSessionAsyncTask extends AsyncTask<String, Void, Void> {
public class TrackingSessionAsyncTask extends AsyncTask<String, Void, Void> implements IGenericAsyncTask<String> {

private static String TAG = "Lootsie TrackingSessionAsyncTask";

    private void markSessionEnd() {
        long beginTime = PreferencesHandler.getLong(LootsieGlobals.KEY_LOOTSIE_SESSION_BEGIN_TIMESTAMP, LootsieEngine.getInstance().getApplicationContext());
        long currentTime = System.currentTimeMillis() / 1000L;
        RestResult result = null;

        try {
            Map<String, Object> params = new HashMap<String,Object>();
            params.put("metric","SessionEndTime");
            params.put("value","1");
            params.put("time",String.valueOf(currentTime));

            DataModel.updateNetworkForced(LootsieApi.USER_ACTIVITY_URL.toString()+":SessionEndTime");
            result = RestClient.doPost(LootsieApi.USER_ACTIVITY_URL.getUri(), params);
            if (LootsieGlobals.debugLevel > 1) Logs.v(TAG, "LootsieImpl: onPause: PostUserActivity: response: " + result.content);
        } catch (Exception e) {
            Logs.v(TAG,"LootsieImpl: onPause: PostUserActivity: ResponseException:" + e.getMessage());
        }
    }


    private void markSessionDuration() {
        long beginTime = PreferencesHandler.getLong(LootsieGlobals.KEY_LOOTSIE_SESSION_BEGIN_TIMESTAMP, LootsieEngine.getInstance().getApplicationContext());
        long currentTime = System.currentTimeMillis() / 1000L;
        RestResult result = null;

        if (beginTime != 0) {
            long durationSec = currentTime - beginTime;

            try {
                Map<String, Object> params = new HashMap<String,Object>();
                params.put("metric","SessionDuration");
                params.put("value","1");
                params.put("time",String.valueOf(durationSec));

                DataModel.updateNetworkForced(LootsieApi.USER_ACTIVITY_URL.toString()+":SessionDuration");
                result = RestClient.doPost(LootsieApi.USER_ACTIVITY_URL.getUri(), params);
                if (LootsieGlobals.debugLevel > 1) Logs.v(TAG,"LootsieImpl: onPause: PostUserActivity: response: " + result.content);
            } catch (Exception e) {
                Logs.v(TAG,"LootsieImpl: onPause: PostUserActivity: ResponseException:" + e.getMessage());
            }
        }

    }

    private void markSessionStart() {

        RestResult result = null;

        long unixTime = System.currentTimeMillis() / 1000L;
        PreferencesHandler.saveLong(LootsieGlobals.KEY_LOOTSIE_SESSION_BEGIN_TIMESTAMP, unixTime, LootsieEngine.getInstance().getApplicationContext());
        try {
            //[[Services sharedManager] sendUserActivity:@"SessionEndTime" value:@"1" time:endTimestampStr activityId:nil callback:nil];
            Map<String, Object> params = new HashMap<String,Object>();
            params.put("metric","SessionStartTime");
            params.put("value","1");
            params.put("time", String.valueOf(unixTime));

            DataModel.updateNetworkForced(LootsieApi.USER_ACTIVITY_URL.toString()+":SessionStartTime");
            result = RestClient.doPost(LootsieApi.USER_ACTIVITY_URL.getUri(), params);
            if (LootsieGlobals.debugLevel > 1) Logs.v(TAG,"LootsieImpl: onResume: PostUserActivity: response: " + result.content);
        } catch (Exception e) {
            Logs.v(TAG,"LootsieImpl: onResume: PostUserActivity: ResponseException:" + e.getMessage());
        }

    }

    @Override
    protected Void doInBackground(String... tokens) {
        if (LootsieGlobals.debugLevel > 1) Logs.v(TAG, "SDK_TrackingSessionAsyncTask: token: " + tokens[0]);

        String token = tokens[0];

        if (token.equalsIgnoreCase("onPause")) {
            // mark session end
            markSessionEnd();

            // mark session duration
            markSessionDuration();

        } else if (token.equalsIgnoreCase("onResume")) {

            // mark session start
            markSessionStart();

        } else if (token.equalsIgnoreCase("onDestroy")) {
            DataModel.saveNetworkStats();


            // mark session end
            markSessionEnd();


            // mark session duration
            markSessionDuration();

        } else if (token.equalsIgnoreCase("startSession")) {

            // mark session start
            markSessionStart();

        }



        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        if (LootsieGlobals.debugLevel > 1) Logs.v(TAG, "SDK_TrackingSessionAsyncTask: finish");
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        //callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        execute(params);
    }
}