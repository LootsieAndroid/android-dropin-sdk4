package com.lootsie.sdk.device;

import android.app.Activity;
import android.content.pm.ActivityInfo;

import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.Logs;

/**
 * This class stores the device orientation
 * @author raghunandank
 *
 */
public class ScreenOrientationManager
{
    private static final String TAG = "LootsieScreenOrientation";
    public static int SCREEN_PORTRAIT = 1;
    public static int SCREEN_LANDSCAPE = 2;
    public static int SCREEN_AUTO = 4;

    // Web app requested Rendering values
    private String  WEBAPP_SCREEN_PORTRAIT = "portrait";
    private String  WEBAPP_SCREEN_LANDSCAPE = "landscape";
    private String  WEBAPP_SCREEN_AUTO = "automatic";

    // Currently not used
//	private String  WEBAPP_SCREEN_PORTRAIT_AS_LANDSCAPE = "portrait_as_landscape";
//	private String  WEBAPP_SCREEN_LANDSCAPE_AS_PORTRAIT = "landscape_as_portrait";
//	private String  WEBAPP_SCREEN_CLIENT_SIDE_OVERRIDE = "client_side_override";
//
    private int reqScreenOrientation ;


    public ScreenOrientationManager()
    {
        reqScreenOrientation = SCREEN_AUTO;
    }

    public int getOrientationState() {
        return reqScreenOrientation;
    }

    /**
     * Developer requested screen Orientation
     * @param reqOrientationState
     */
    public void setRequestedScreenOrientation(int reqOrientationState) {

        DebugLog("LootsieScreenOrientation: Orientation requested set to %d", reqOrientationState);
        reqScreenOrientation = reqOrientationState;
    }

    /**
     * Developer requested screen Orientation from webapp
     * @param requestedState
     */
    public void setRequestedScreenOrientation(String requestedState) {

        DebugLog("LootsieScreenOrientation: Webapp requested Orientation : %s", requestedState);

        int reqOrientationState = ScreenOrientationManager.SCREEN_AUTO;

        if(requestedState.equalsIgnoreCase(WEBAPP_SCREEN_PORTRAIT))
        {
            reqOrientationState = ScreenOrientationManager.SCREEN_PORTRAIT;

        } else if(requestedState.equalsIgnoreCase(WEBAPP_SCREEN_LANDSCAPE))
        {
            reqOrientationState = ScreenOrientationManager.SCREEN_LANDSCAPE;
        }
        else if(requestedState.equalsIgnoreCase(WEBAPP_SCREEN_AUTO))
        {
            reqOrientationState = ScreenOrientationManager.SCREEN_AUTO;
        }
        else
            // Otherwise for all other webapp values it is defaulting to auto
            reqOrientationState = ScreenOrientationManager.SCREEN_AUTO ;

        // Save it for future display
        setRequestedScreenOrientation(reqOrientationState);

    }
    /**
     * This method displays webview based on the screen orientation set
     * by the developer
     * @param act
     */
    public void renderScreenWith(Activity act)
    {
        if(act == null )
        {
            Logs.e(TAG, "Activity context null , No further action is required for rendering");
            return;
        }

        if(reqScreenOrientation == ScreenOrientationManager.SCREEN_PORTRAIT)
            act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        else if (reqScreenOrientation == ScreenOrientationManager.SCREEN_LANDSCAPE)
            act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        else
            act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }


}
