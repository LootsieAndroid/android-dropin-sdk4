package com.lootsie.sdk.callbacks;

import com.lootsie.sdk.netutil.RestResult;

/**
 * Created by jerrylootsie on 2/24/15.
 */
public interface IRESTCallback {
    public void restResult(RestResult result);
}
