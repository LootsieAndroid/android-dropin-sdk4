package com.lootsie.sdk.utils.image;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.LruCache;
import com.lootsie.sdk.viewcontrollers.base.LootsieNetworkImageView;

//import com.lootsie.sdk.Globals;
//import com.lootsie.sdk.mangers.SubmitScoreManager;

import java.io.*;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;


/**
 * This helper class download images from the Internet and binds those with the provided ImageView.
 *
 * This is an image downloader which doesn't use Volley library.
 * 
 * @author Alexander Salnikov
 */
@SuppressLint("NewApi")
public class AsyncImageDownloader {
    private static final String LOG_TAG = "AsyncImageDownloader";
    private final static String TAG = AsyncImageDownloader.class.getName();
    
    public enum Mode {NO_ASYNC_TASK, NO_DOWNLOADED_DRAWABLE, CORRECT}

    private Mode mode = Mode.NO_ASYNC_TASK;

    private Context context = null;

    // 4mb for every image is reserved?
    private final BitmapLruCache cache = new BitmapLruCache(4 * 1024 * 1024);
    // note: sharing the cache across instances causes an exception ConcurrentModificationException in LRU?
    //private static BitmapLruCache cache = null;

    private static final int HARD_CACHE_CAPACITY = 10;

    private static final int DELAY_BEFORE_PURGE = 60 * 1000; // in milliseconds

    private static ArrayList<BitmapDownloaderTask> downloaderTasks = new ArrayList<BitmapDownloaderTask>();

    public AsyncImageDownloader(Context context) {
        this.context = context;

        cache.init(context);
    }

    /**
     * retrieve a previously cached image - or display default image if not present
     */
    public void getCachedImage(String url, ImageView imageView, int defaultResId) {
        // State sanity: url is guaranteed to never be null in DownloadedDrawable and cache keys.
    	if ((url == null) || (url.compareToIgnoreCase("null") == 0)) {
            if (imageView != null) {
            	if (defaultResId >= 0) {
            		imageView.setImageResource(defaultResId);
            	} else {
            		imageView.setImageDrawable(null);
            	}
            } else {
                Log.e(TAG, "AsyncImageDownloader: getCachedImage: imageView is null!");
            }
            return;
        }    
   	    	
        resetPurgeTimer();
        Bitmap bitmap = getBitmapFromCache(url);
        
        if (bitmap == null) {
        	if (LootsieGlobals.debugLevel > 0) Log.w(TAG, "AsyncImageDownloader: getCachedImage: can't find cached image: " + url);
        	
            if (imageView != null) {
            	if (defaultResId >= 0) {
            		imageView.setImageResource(defaultResId);
            	} else {
            		imageView.setImageDrawable(null);
            	}
            } else {
                Log.e(TAG, "AsyncImageDownloader: getCachedImage: imageView is null!");
            }
        } else {
            if (LootsieGlobals.debugLevel > 0) Log.w(TAG, "AsyncImageDownloader: getCachedImage:" + bitmap.getWidth() + "x" + bitmap.getWidth());
        }
        
    }
    
    
    /**
     * Download the specified image from the Internet and binds it to the provided ImageView. The
     * binding is immediate if the image is found in the cache and will be done asynchronously
     * otherwise. A null bitmap will be associated to the ImageView if an error occurs.
     *
     * @param url       The URL of the image to download.
     * @param imageView The ImageView to bind the downloaded image to.
     * @param defaultResId	The default image resource to load, if provided instead of url image.
     */
    public void download(String url, ImageView imageView, int defaultResId) {

        if (LootsieGlobals.debugLevel > 0) Log.w(TAG, "AsyncImageDownloader: download: url: " + url);

        // State sanity: url is guaranteed to never be null in DownloadedDrawable and cache keys.
    	if ((url == null) || (url.compareToIgnoreCase("null") == 0)) {
            if (imageView != null) {
            	if (defaultResId >= 0) {
            		imageView.setImageResource(defaultResId);
            	} else {
            		imageView.setImageDrawable(null);
            	}
            } else {
                Log.e(TAG, "AsyncImageDownloader: download: imageView is null!");
            }
            return;
        }    
   	
        resetPurgeTimer();
        Bitmap bitmap = getBitmapFromCache(url);

        if (bitmap == null) {
            forceDownload(url, imageView, defaultResId);
        } else {
            cancelPotentialDownload(url, imageView);

            if (LootsieGlobals.debugLevel > 0) Log.w(TAG, "AsyncImageDownloader: download: bitmap: " + bitmap.getWidth() + "x" + bitmap.getHeight());

            if (imageView != null) {
                imageView.clearAnimation();
                imageView.setImageBitmap(bitmap);
            } else {
                Log.e(TAG, "AsyncImageDownloader: download: imageView is null!");
            }

        }    	
    }
    
    public void download(String url, ImageView imageView) {
    	download(url, imageView, -1);
    }

    /*
     * Same as download but the image is always downloaded and the cache is not used.
     * Kept private at the moment as its interest is not clear.
       private void forceDownload(String url, ImageView view) {
          forceDownload(url, view, null);
       }
     */

    /**
     * Same as download but the image is always downloaded and the cache is not used.
     * Kept private at the moment as its interest is not clear.
     */
    //private void forceDownload(String url, ImageView imageView) {
    private void forceDownload(String url, ImageView imageView, int defaultResId) {
        // State sanity: url is guaranteed to never be null in DownloadedDrawable and cache keys.
        if ((url == null) || (url.compareToIgnoreCase("null") == 0)) {
            if (imageView != null) {
            	if (defaultResId >= 0) {
            		imageView.setImageResource(defaultResId);
            	} else {
            		imageView.setImageDrawable(null);
            	}
            } else {
                Log.e(TAG, "AsyncImageDownloader: forceDownload: imageView is null!");
            }

            return;
        }

        if (cancelPotentialDownload(url, imageView)) {
        	if (LootsieGlobals.debugLevel > 0) Log.v(TAG,"AsyncImageDownloader: forceDownload: tasks: " + downloaderTasks.size() + " mode: " + mode.toString() + " url: " + url);
        	
            switch (mode) {
                case NO_ASYNC_TASK:                	
//                    Bitmap bitmap = downloadBitmap(url);
//                    if (bitmap != null) {
//                        addBitmapToCache(url, bitmap);
//                        if (imageView != null) {
//                            imageView.clearAnimation();
//                            imageView.setImageBitmap(bitmap);
//                        } else {
//                            Log.e(TAG, "AsyncImageDownloader: forceDownload: imageView is null!");
//                        }
//                    }
                    Log.e(TAG, "AsyncImageDownloader: NO_ASYNC_TASK: unsupported!");
                    break;

                case NO_DOWNLOADED_DRAWABLE:
                    if (imageView != null) {
                        imageView.setMinimumHeight(32);
                    } else {
                        Log.e(TAG, "AsyncImageDownloader: forceDownload: imageView is null!");
                    }
                    BitmapDownloaderTask task = new BitmapDownloaderTask(imageView);
                    //task.setCache(cache);
                    task.setAsyncImageDownloader(this);
                    downloaderTasks.add(task);
                    task.execute(url);
                    break;

                case CORRECT:
                    task = new BitmapDownloaderTask(imageView);
                    DownloadedDrawable downloadedDrawable = new DownloadedDrawable(task);
                    if (imageView != null) {
                        imageView.setImageDrawable(downloadedDrawable);
                        imageView.setMinimumHeight(32);
                    } else {
                        Log.e(TAG, "AsyncImageDownloader: forceDownload: imageView is null!");
                    }
                    downloaderTasks.add(task);
                    task.execute(url);
                    break;
            }
        }
    }

    /**
     * Returns true if the current download has been canceled or if there was no download in
     * progress on this image view.
     * Returns false if the download in progress deals with the same url. The download is not
     * stopped in that case.
     */
    private static boolean cancelPotentialDownload(String url, ImageView imageView) {
        BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);

        if (LootsieGlobals.debugLevel > 0) Log.w(TAG, "AsyncImageDownloader: cancelPotentialDownload: url: " + url);

        if (bitmapDownloaderTask != null) {
            String bitmapUrl = bitmapDownloaderTask.url;
            if ((bitmapUrl == null) || (!bitmapUrl.equals(url))) {
                bitmapDownloaderTask.cancel(true);
            } else {
                // The same URL is already being downloaded.
                return false;
            }
        }
        return true;
    }

    /**
     * @param imageView Any imageView
     * @return Retrieve the currently active download task (if any) associated with this imageView.
     *         null if there is no such task.
     */
    private static BitmapDownloaderTask getBitmapDownloaderTask(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof DownloadedDrawable) {
                DownloadedDrawable downloadedDrawable = (DownloadedDrawable) drawable;
                return downloadedDrawable.getBitmapDownloaderTask();
            }
        } else {
            Log.e(TAG, "AsyncImageDownloader: BitmapDownloaderTask: imageView is null!");
        }
        return null;
    }







    /**
     * A fake Drawable that will be attached to the imageView while the download is in progress.
     * <p/>
     * <p>Contains a reference to the actual download task, so that a download task can be stopped
     * if a new binding is required, and makes sure that only the last started download process can
     * bind its result, independently of the download finish order.</p>
     */
    static class DownloadedDrawable extends ColorDrawable {
        private final WeakReference<BitmapDownloaderTask> bitmapDownloaderTaskReference;

        public DownloadedDrawable(BitmapDownloaderTask bitmapDownloaderTask) {
            super(Color.TRANSPARENT);
            bitmapDownloaderTaskReference =
                    new WeakReference<BitmapDownloaderTask>(bitmapDownloaderTask);
        }

        public BitmapDownloaderTask getBitmapDownloaderTask() {
            return bitmapDownloaderTaskReference.get();
        }
    }

    public void setMode(Mode mode) {
        this.mode = mode;
//        clearCache();
    }


    // Soft cache for bitmaps kicked out of hard cache
    private final static ConcurrentHashMap<String, SoftReference<Bitmap>> sSoftBitmapCache =
            new ConcurrentHashMap<String, SoftReference<Bitmap>>(HARD_CACHE_CAPACITY / 2);

    private final Handler purgeHandler = new Handler();

    private final Runnable purger = new Runnable() {
        public void run() {
            clearCache();
        }
    };

    /**
     * Adds this bitmap to the cache.
     *
     * @param bitmap The newly downloaded bitmap.
     */
    public void addBitmapToCache(String url, Bitmap bitmap) {
        if (bitmap != null) {
            synchronized (cache) {
                cache.putBitmap(url, bitmap);
            }
        }
    }


    /**
     * @param url The URL of the image that will be retrieved from the cache.
     * @return The cached bitmap or null if it was not found.
     */
    private Bitmap getBitmapFromCache(String url) {

        if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "AsyncImageDownloader: getBitmapFromCache");

        // First try the hard reference cache
        synchronized (cache) {
            final Bitmap bitmap = cache.getBitmap(url);
            if (bitmap != null) {
                // Bitmap found in hard cache
                // Move element to first position, so that it is removed last
                cache.moveFirst(cache.getKeyForUrl(url));
                return bitmap;
            }
        }

        // Then try the soft reference cache
        SoftReference<Bitmap> bitmapReference = sSoftBitmapCache.get(url);
        if (bitmapReference != null) {
            final Bitmap bitmap = bitmapReference.get();
            if (bitmap != null) {
                // Bitmap found in soft cache
                return bitmap;
            } else {
                // Soft reference has been Garbage Collected
                sSoftBitmapCache.remove(url);
            }
        }

        return null;
    }

    /**
     * Clears the image cache used internally to improve performance. Note that for memory
     * efficiency reasons, the cache will automatically be cleared after a certain inactivity delay.
     */
    public void clearCache() {
    	if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "AsyncImageDownloader: clearCache");

        // this is a synchronized function
    	cache.clear();
    	
        sSoftBitmapCache.clear();
    }

    /**
     * Allow a new delay before the automatic cache clear is done.
     */
    private void resetPurgeTimer() {
        purgeHandler.removeCallbacks(purger);
        purgeHandler.postDelayed(purger, DELAY_BEFORE_PURGE);
    }
}
