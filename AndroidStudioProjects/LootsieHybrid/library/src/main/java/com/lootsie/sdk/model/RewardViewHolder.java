package com.lootsie.sdk.model;

import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Button;

/**
 * Created by jerrylootsie on 3/9/15.
 */
public class RewardViewHolder {
    public TextView t;
    public ImageView i;
    public int position;
    public FrameLayout descriptionLayout;
    public LinearLayout redeemLayout;
    public EditText editTextEmail;
    //public TextView termsAndConditionsTextView;
    public Button termsAndConditionsButton;

    // moved to the base RewardListEntryModel class?
//        public boolean descriptionVisible = false;
//        public boolean redeemVisible = false;
    public boolean startedImageLoad = false;

}