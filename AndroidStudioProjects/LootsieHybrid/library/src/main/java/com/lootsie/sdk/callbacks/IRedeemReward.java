package com.lootsie.sdk.callbacks;

import com.lootsie.sdk.utils.LootsieGlobals;

public interface IRedeemReward {
	
	public void onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT mesg, String json);
	public void onRedeemSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT mesg, String json);

}
