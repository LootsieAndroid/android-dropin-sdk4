package com.lootsie.sdk.viewcontrollers.base;

import android.app.Activity;

import com.lootsie.sdk.lootsiehybrid.LootsieEngine;

/**
 * Created by jerrylootsie on 2/23/15.
 */
// updating android activity from a background thread
// http://cogitolearning.co.uk/?p=793
public abstract class UpdatableActivity extends Activity {

    public final void updateActivityFromBgThread() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateActivity();
            }});
    }

    public abstract void updateActivity();

    @Override
    protected void onStart() {
        super.onStart();

        LootsieEngine.getInstance().setUpdatable(this);
    }

    @Override
    protected void onStop() {
        LootsieEngine.getInstance().setUpdatable(null);
        super.onStop();
    }
}