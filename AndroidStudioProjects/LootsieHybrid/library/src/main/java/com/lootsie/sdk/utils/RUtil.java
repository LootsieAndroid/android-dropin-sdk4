package com.lootsie.sdk.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * RUtil
 *
 * @author Alexander Salnikov
 */
public final class RUtil {

    private static String TAG = "Lootsie RUtil";

    public static int getResourceIdByNameUsingContext(Context context, String className, String name) {

//        Resources r = context.getResources();
//
//        int getRes = r.getIdentifier(name, className, context.getPackageName());
//        String setRes = String.valueOf(getRes);
//
//        int getRes2 = r.getIdentifier(setRes, "array", context.getPackageName());
//        String[] resourceList = r.getStringArray(getRes2);
//
//        for (int i = 0; i < resourceList.length; i++) {
//            DebugLog("getResourceIdByNameUsingContext resource[%d]: %s", i, resourceList[i]);
//        }
//        AssetManager mgr = context.getAssets();
//        displayFiles(mgr, "/", 0);

//        displayFiles(mgr, "/assets");
//        displayFiles(mgr, "./assets");
//        displayFiles(mgr, "./");
//        displayFiles(mgr, "//res");

        DebugLog("getResourceIdByNameUsingContext: name:%s className:%s packagePath:%s", name, className, context.getPackageResourcePath());
        DebugLog("getResourceIdByNameUsingContext: name:%s className:%s packageName:%s", name, className, context.getApplicationInfo().toString());


        int id = context.getResources().getIdentifier(name, className, context.getPackageName());

        return id;
    }

    static void displayFiles (AssetManager mgr, String path, int level) {

        Logs.v(TAG,"enter displayFiles("+path+")");
        try {
            String list[] = mgr.list(path);

            if (list.length > 0) {
                Logs.v(TAG, "L" + level + ": list:" + Arrays.asList(list));
            }

            if (list != null)
                for (int i=0; i<list.length; ++i)
                {
                    if(level>=1){
                        displayFiles(mgr, path + "/" + list[i], level+1);
                    }else{
                        displayFiles(mgr, list[i], level+1);
                    }
                }
        } catch (IOException e) {
            Logs.v(TAG,"List error: can't list" + path);
        }

    }

    static void displayFiles (AssetManager mgr, String path) {
        try {
            String list[] = mgr.list(path);
            if (list != null)
                for (int i=0; i<list.length; ++i)
                {
                    Logs.v("Assets:", path +"/"+ list[i]);
                    displayFiles(mgr, path + list[i]);
                }
        } catch (IOException e) {
            Logs.v("List error:", "can't list" + path);
        }

    }

    public static int getResourceIdByName(String packageName, String className, String name) {
        Class r = null;
        int id = 0;
        try {
            r = Class.forName(packageName + ".R");

//            Class[] declaredClasses = r.getDeclaredClasses();
            Class[] classes = r.getClasses();
            Class desireClass = null;

//            for (int i = 0; i < classes.length; i++) {
//                DebugLog("getResourceIdByName declaredClass[%d]: %s", i, declaredClasses[i].getName());
//            }

            for (int i = 0; i < classes.length; i++) {
                DebugLog("getResourceIdByName class[%d]: %s", i, classes[i].getName());

                if(classes[i].getName().split("\\$")[1].equals(className)) {
                    desireClass = classes[i];

                    break;
                }
            }

            if(desireClass != null)
                id = desireClass.getField(name).getInt(desireClass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        return id;

    }

    public static int getResourceId(Context context, String name, String defType, String defPackage) {
        return context.getResources().getIdentifier(name, defType, defPackage);

    }

    public static int getDrawableId(Context context, String name) {
        return getResourceId(context, name, "drawable", context.getPackageName());
    }

    public static int getStringId(Context context, String name) {
        return getResourceId(context, name, "string", context.getPackageName());
    }

    public static int getLayoutId(Context context, String name) {
        return getResourceId(context, name, "layout", context.getPackageName());
    }

    public static int getMenuId(Context context, String name) {
        return getResourceId(context, name, "menu", context.getPackageName());
    }

    public static int getArrayId(Context context, String name) {
        return getResourceId(context, name, "array", context.getPackageName());
    }

    public static int getId(Context context, String name) {
        return getResourceId(context, name, "id", context.getPackageName());
    }

    public static int getDimenId(Context context, String name) {
        return getResourceId(context, name, "dimen", context.getPackageName());
    }

    public static int getStyleId(Context context, String name) {
        return getResourceId(context, name, "style", context.getPackageName());
    }

    public static int getAnim(Context context, String name) {
        return getResourceId(context, name, "anim", context.getPackageName());
    }


    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (com.lootsie.sdk.utils.LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}
