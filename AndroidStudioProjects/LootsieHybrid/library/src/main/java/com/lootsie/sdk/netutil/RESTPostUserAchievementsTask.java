package com.lootsie.sdk.netutil;

import java.util.Map;

import org.json.JSONObject;

import android.os.AsyncTask;
//import android.util.Log;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

/**
 * Created by jerrylootsie on 2/12/15.
 */
/*
    POST https://api-v2.lootsie.com/v2/user/achievements
    {“achievement_ids”,[“#”]}
    X-Lootsie-App-Secret
    X-Lootsie-User-Session-Token
 */
//public class RESTPostUserAchievementsTask extends AsyncTask<JSONObject , Void, RestResult> {
public class RESTPostUserAchievementsTask extends AsyncTask<JSONObject , Void, RestResult> implements IGenericAsyncTask<JSONObject> {

//public class RESTPostUserAchievementsTask extends AsyncTask< Map<String, Object>, Void, RestResult> {
//public class RESTPostUserAchievementsTask extends AsyncTask<String, Void, RestResult> {

    private final static String TAG = "Lootsie.RESTPostUserAchievementsTask";

    IRESTCallback callback = null;

    public RESTPostUserAchievementsTask() {}

    public RESTPostUserAchievementsTask(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    protected RestResult doInBackground( JSONObject ... params) {
//    protected RestResult doInBackground( String ... params) {
//    protected RestResult doInBackground( Map<String, Object> ... params) {
        if (params == null || params.length == 0) {
            return null;
        }

        RestResult response = new RestResult();
        try {
            // pull stuff from Device.java
            //Map<String, Object> map = params[0];

            response = RestClient.doPost(LootsieApi.USER_ACHIEVEMENTS_URL.getUri(), params[0]);

            if (LootsieGlobals.debugLevel > 0) Logs.v(TAG, "RESTPostUserAchievementsTask:" + response.content);

        } catch (Exception e) {
            Logs.e(TAG, "RESTGetSessionTask: processError: getMessage: " + e.getMessage());
        }
        return response;
    }



    @Override
    protected void onPostExecute(final RestResult restResult) {
        if (callback != null) {
            callback.restResult(restResult);
        }
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(JSONObject... params) {
        execute(params);
    }
}
