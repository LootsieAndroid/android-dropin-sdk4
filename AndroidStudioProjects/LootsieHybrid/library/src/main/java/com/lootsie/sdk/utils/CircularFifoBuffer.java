package com.lootsie.sdk.utils;

import android.util.Log;

/**
 * Created by jerrylootsie on 2/13/15.
 */
public class CircularFifoBuffer implements Queue {

    private static String TAG = "Lootsie CircularFIFOBuffer";

    private static final int capacity = 5;
    private Object[] Q;
    private final int N; // capacity
    private int f = 0;
    private int r = 0;






    public CircularFifoBuffer() {
        this(capacity);
    }

    public CircularFifoBuffer(int capacity) {
        N = capacity;
        Q = new Object[N];
    }

    public int size() {
        if (r > f)
            return r - f;
        return N - f + r;
    }

    public boolean isEmpty() {
        return (r == f) ? true : false;
    }

    public boolean isFull() {
        int diff = r - f;
        if (diff == -1 || diff == (N - 1))
            return true;
        return false;
    }

    public void enqueue(Object obj) throws QueueFullException {
        if (isFull()) {
            //throw new QueueFullException("Queue is Full.");
            // instead of throwing exception, dump out from front (first in, first out)
            Object item = Q[f];
            Q[f] = null;
            f = (f + 1) % N;

            // add to rear
            Q[r] = obj;
            r = (r + 1) % N;

//            Log.v(TAG,"enqueue[f:" + f + " r:" + r + "]: front: " + item);
//            Log.v(TAG,"enqueue[f:" + f + " r:" + r + "]: rear: " + obj);
        } else {
            Q[r] = obj;
            r = (r + 1) % N;

//            Log.v(TAG,"enqueue[f:" + f + " r:" + r + "]: rear: " + obj);
        }
    }

    public Object dequeue() throws QueueEmptyException {
        Object item;
        if (isEmpty()) {
            throw new QueueEmptyException();
        } else {
            item = Q[f];
            Q[f] = null;
            f = (f + 1) % N;
        }
        return item;
    }

    public String getBufferDump() {
        StringBuilder sb = new StringBuilder("");
        String sep = "\n";

        int front = f;
        int rear = r;
        while (front != rear) {
            sb.append(sep).append(Q[front]);

            front = (front +1) % N;
        }

        return sb.toString();
    }


}