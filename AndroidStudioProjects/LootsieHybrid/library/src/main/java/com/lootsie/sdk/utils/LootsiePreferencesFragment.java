package com.lootsie.sdk.utils;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import com.lootsie.sdk.R;

/**
 * Created by jerrylootsie on 2/26/15.
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class LootsiePreferencesFragment extends PreferenceFragment {

    private static String TAG = "Lootsie LootsiePreferencesFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Logs.v(TAG, "onCreate");
//        addPreferencesFromResource(R.xml.account_preferences);
    }
}