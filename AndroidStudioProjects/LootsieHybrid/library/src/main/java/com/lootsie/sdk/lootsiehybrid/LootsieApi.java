package com.lootsie.sdk.lootsiehybrid;

/**
 * LootsieApi
 *
 * @author Alexander Salnikov
 */
public enum LootsieApi {
    INIT_APP_URL("/app"),
    SETTINGS_URL("/settings"),
    SIGN_IN_PAGE_URL("/user/session/email"),
    SIGN_IN_FACEBOOK_PAGE_URL("/user/session/facebook/server"),
    SIGN_IN_GUEST_URL("/user/session/guest"),
    SIGN_OUT_URL("/user/session"),
    USER_ACCOUNT_URL("/user/account"),
    USER_ACCOUNT_RESET_PASSWORD_EMAIL_URL("/user/account/reset-password-email"),
    USER_ACHIEVEMENTS_URL("/user/achievements"),
    USER_ACTIVITY_URL("/user/activity"),
    USER_SESSION_EMAIL_ONLY_URL("/user/session/emailonly"),
    USER_REWARD_EMAIL_REDEMPTION_URL("/user/reward/email_redemption"),
    USER_LOCATION_URL("/user/location"),
    REWARD_URL("/user/reward"),
    REWARD_REDEMPTIONS_URL("/user/reward/redemptions"),
    ENGAGEMENT_URL("/user/engagement"),
    INIT_API_SESSION_URL("/api/session");

    // Staging server URL
    //public static String BASE_API_URL = "https://stage-prime-api.lootsie.com/v1";

    // live
    //public static String BASE_API_URL = "https://api.lootsie.com/v1";
    public static String BASE_API_URL = "https://api-v2.lootsie.com/v2";
    // dev
    //public static String BASE_API_URL = "https://api-dev.lootsie.com/v1";
    // staging v2
    //public static String BASE_API_URL = "https://stage-prime-api.lootsie.com/v2";


    // Staging pages URLs - should be overridden at runtime by AppManager:getAppSettings
    public static String HOW_IT_WORKS = "https://stage-prime-api.lootsie.com/static/api/how_it_works.html";
    public static String REWARDS_PAGE = "https://stage-prime-www.lootsie.com/#/rewards";
    public static String PRIVACY_POLICY_PAGE = "https://stage-prime-www.lootsie.com/#/privacy-policy";
    public static String TERM_OF_SERVICE_PAGE = "https://stage-prime-www.lootsie.com/#/terms-of-service";

    // Live server URL
    // public static final String BASE_API_URL = "https://api.lootsie.com/v1";

    // Live pages URLs
    // public static final String HOW_IT_WORKS = "https://api.lootsie.com/static/api/how_it_works.html";
    // public static final String REWARDS_PAGE = "https://www.lootsie.com/#/rewards";
    // public static final String PRIVACY_POLICY_PAGE = "https://www.lootsie.com/#/privacy-policy";
    // public static final String TERM_OF_SERVICE_PAGE = "https://www.lootsie.com/#/terms-of-service";

    private final String url;


    private LootsieApi(String uri) {
        this.url = uri;
    }

    public String getUri() {
        return BASE_API_URL + url;
    }

    public static void setBaseApiUrl(String uri) {
        BASE_API_URL = uri;
    }

    public static void setHowitworksUrl(String uri) {
        HOW_IT_WORKS = uri;
    }

    public static void setPrivacyPolicyUrl(String uri) {
        PRIVACY_POLICY_PAGE = uri;
    }

    public static void setTermsUrl(String uri) {
        TERM_OF_SERVICE_PAGE = uri;
    }

    public static void setMarketplaceUrl(String uri) {
        REWARDS_PAGE = uri;
    }

}
