package com.lootsie.sdk.viewcontrollers.base;

import android.annotation.TargetApi;
import android.app.ListFragment;
import android.os.Build;

import com.lootsie.sdk.model.Achievement;
import com.lootsie.sdk.model.AchievementListEntryModel;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jerrylootsie on 3/2/15.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
//public class BaseAchievementsFragment extends UpdatableListFragment {
public class BaseAchievementsFragment extends UpdatableFragment {

    static private String TAG = "Lootsie BaseAchievementsFragment";

    protected List<AchievementListEntryModel> getModel(boolean getRepeatableAchievements) {
        List<AchievementListEntryModel> list = new ArrayList<AchievementListEntryModel>();


        if (DataModel.app.achievements == null) {
            Logs.e(TAG, "AchievementsActivity: app achievements is null!");
            return list;
        }

        if (DataModel.app.achievements != null) DebugLog("getModel achievments: " + DataModel.app.achievements.size());

        // create a hash lookup table based on achievement id for lookup later
        HashMap<String, AchievementListEntryModel> achievementHash = new HashMap<String, AchievementListEntryModel>();

        // add title entry first
        AchievementListEntryModel titleEntry = new AchievementListEntryModel();
        if (getRepeatableAchievements) {
            titleEntry.achievementEntryType = AchievementListEntryModel.AchievementEntryType.REPEATABLE_ACHIEVMENTS_TITLE;
        } else {
            titleEntry.achievementEntryType = AchievementListEntryModel.AchievementEntryType.ONE_TIME_ACHIEVEMENTS_TITLE;
        }
        list.add(titleEntry);


        if (DataModel.app.achievements != null) {
	        for (int i = 0; i< DataModel.app.achievements.size(); i++) {
	            Achievement achievement = DataModel.app.achievements.get(i);

                if ((achievement.repeatable && getRepeatableAchievements) ||
                        (!achievement.repeatable && !getRepeatableAchievements)) {

                    AchievementListEntryModel achievementListEntry = new AchievementListEntryModel(achievement);
                    list.add(achievementListEntry);

                    achievementHash.put(achievement.id, achievementListEntry);
                }
	        }
        }
        
        // dereference user achievements against app achievements to indicate successful achievements by the user
        if (DataModel.user.achievements != null) {
	        for (int userIndex = 0; userIndex < DataModel.user.achievements.size(); userIndex++) {
	            Achievement userAchievement = DataModel.user.achievements.get(userIndex);
	
	            if (achievementHash.containsKey(userAchievement.id)) {
	                AchievementListEntryModel achievementListEntry = achievementHash.get(userAchievement.id);
	                achievementListEntry.setAchievementReached(true);
	
	                DebugLog("Achievement reached: %s", userAchievement.id);
	            }
	        }
        }
        
        // Initially select one of the items
        //list.get(1).setSelected(true); // this is a bug, select item 0!
        if (list.size() > 0) {
        	list.get(0).setSelected(true);
        }
        
        return list;
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}
