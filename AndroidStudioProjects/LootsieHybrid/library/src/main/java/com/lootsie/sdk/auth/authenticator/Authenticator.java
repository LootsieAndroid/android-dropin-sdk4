package com.lootsie.sdk.auth.authenticator;


import android.accounts.*;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.utils.LootsieGlobals;


/**
 * This class is an implementation of AbstractAccountAuthenticator for
 * authenticating accounts in the com.lootsie.android.auth domain. The
 * interesting thing that this class demonstrates is the use of authTokens as
 * part of the authentication process. In the account setup UI, the user enters
 * their username and password. But for our subsequent calls off to the service
 * for syncing, we want to use an authtoken instead - so we're not continually
 * sending the password over the wire. getAuthToken() will be called when
 * SyncAdapter calls AccountManager.blockingGetAuthToken(). When we get called,
 * we need to return the appropriate authToken for the specified account. If we
 * already have an authToken stored in the account, we return that authToken. If
 * we don't, but we do have a username and password, then we'll attempt to talk
 * to the sample service to fetch an authToken. If that fails (or we didn't have
 * a username/password), then we need to prompt the user - so we create an
 * AuthenticatorActivity intent and return that. That will display the dialog
 * that prompts the user for their login information.
 */
class Authenticator extends AbstractAccountAuthenticator {

    /**
     * The tag used to log to adb console. *
     */
    private static final String TAG = "Authenticator";

    // Authentication Service context
    private final Context mContext;

    public Authenticator(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType,
                             String authTokenType, String[] requiredFeatures, Bundle options) {
        return new Bundle();
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) {
        return null;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account,
                               String authTokenType, Bundle loginOptions) throws NetworkErrorException {
        Log.v(TAG, "getAuthToken()");

        // If the caller requested an authToken type we don't support, then
        // return an error
        if (!authTokenType.equals(LootsieGlobals.AUTHTOKEN_TYPE)) {
            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ERROR_MESSAGE, "invalid authTokenType");
            return result;
        }

        // Extract the username and password from the Account Manager, and ask
        // the server for an appropriate AuthToken.
        final AccountManager am = AccountManager.get(mContext);
//        final String password = am.getPassword(account);
//        if (password != null) {
//            final String authToken = NetworkUtilities.authenticate(account.name, password);

        // we need to make the token unique to each application since the accountmanager is shared between several games on the device
        String appId = LootsieEngine.getInstance().getAppId();
        String uniqueKeyAuthToken = LootsieGlobals.KEY_AUTHTOKEN + LootsieGlobals.APP_ID_SEPARATOR + appId;
        String authToken = am.getUserData(account, uniqueKeyAuthToken);
        if (LootsieGlobals.debugLevel > 0) Log.v(TAG,"get uniqueKeyAuthToken: " + uniqueKeyAuthToken);

        if (!TextUtils.isEmpty(authToken)) {
            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, LootsieGlobals.ACCOUNT_TYPE);
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
            return result;
        }
//        }

        // If we get here, then we couldn't access the user's password - so we
        // need to re-prompt them for their credentials. We do that by creating
        // an intent to display our AuthenticatorActivity panel.
//        final Intent intent = new Intent(mContext, AuthenticatorActivity.class);
//        intent.putExtra(AuthenticatorActivity.PARAM_USERNAME, account.name);
//        intent.putExtra(AuthenticatorActivity.PARAM_AUTHTOKEN_TYPE, authTokenType);
//        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);

        final Bundle bundle = new Bundle();
//        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        bundle.putString(AccountManager.KEY_ERROR_MESSAGE, "invalid authTokenType");
        return bundle;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType) {
        // null means we don't support multiple authToken types
        Log.v(TAG, "getAuthTokenLabel()");
        return null;
    }

    @Override
    public Bundle hasFeatures(
            AccountAuthenticatorResponse response, Account account, String[] features) {
        // This call is used to query whether the Authenticator supports
        // specific features. We don't expect to get called, so we always
        // return false (no) for any queries.
        Log.v(TAG, "hasFeatures()");
        final Bundle result = new Bundle();
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
        return result;
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account,
                                    String authTokenType, Bundle loginOptions) {
        Log.v(TAG, "updateCredentials()");

//        final Intent intent = new Intent(mContext, AuthenticatorActivity.class);
//        intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, "new name");
//
//        final Bundle result = new Bundle();
//        result.putParcelable(AccountManager.KEY_INTENT, intent);
//        return result;
//        final Intent intent = new Intent(mContext, AuthenticatorActivity.class);
//        intent.putExtra(AuthenticatorActivity.PARAM_USERNAME, "qwerty");
//        intent.putExtra(AuthenticatorActivity.PARAM_AUTHTOKEN_TYPE,
//                authTokenType);

        final Bundle bundle = new Bundle();
//        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        bundle.putString(AccountManager.KEY_ERROR_MESSAGE, "invalid authTokenType");
        return bundle;
    }
}