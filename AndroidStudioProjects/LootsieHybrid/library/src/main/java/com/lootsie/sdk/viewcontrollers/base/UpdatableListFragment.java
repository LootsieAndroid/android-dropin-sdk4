package com.lootsie.sdk.viewcontrollers.base;

import android.annotation.TargetApi;
import android.app.ListFragment;
import android.os.Build;

import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

/**
 * Created by jerrylootsie on 3/2/15.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class UpdatableListFragment extends ListFragment {

    private static String TAG = "Lootsie BaseRewardsActivity";

    public final void updateFragmentFromBgThread() {
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateFragment();
            }
        });
    }

    public void updateFragment() {
        DebugLog("updateFragment");
    }

    @Override
    public void onStart() {
        super.onStart();

        // register activity callbacks with Lootsie instance? for updates and such.
        // we could use broadcast receiver for this?
        LootsieEngine.getInstance().setUpdatableListFragment(this, this.toString());
    }

    @Override
    public void onStop() {

        // unregister activity reference with Lootsie instance
        LootsieEngine.getInstance().unsetUpdatableListFragment(this.toString());
        super.onStop();
    }
    
    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }      
}
