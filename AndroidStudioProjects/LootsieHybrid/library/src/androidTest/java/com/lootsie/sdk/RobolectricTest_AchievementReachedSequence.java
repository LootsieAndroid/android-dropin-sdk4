package com.lootsie.sdk;

import com.lootsie.sdk.callbacks.IAchievementReached;
import com.lootsie.sdk.lootsiehybrid.LOOTSIE_NOTIFICATION_CONFIGURATION;
import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.sequences.AchievementReachedSequence;
import com.lootsie.sdk.model.App;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.net.Headers;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.netutil.MockedRestPostUserAchievementsTask;
import com.lootsie.sdk.netutil.MockedRestGenericJSONObjectResponseTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Created by jerrylootsie on 7/14/15.
 */
@Config(constants = BuildConfig.class, sdk = 21)
@RunWith(CustomRobolectricRunner.class)
public class RobolectricTest_AchievementReachedSequence {


    static enum TestResultStatus {
        SUCCESS,
        PENDING,
        FAILURE;

        private TestResultStatus() {
        }
    }

    class AchievementReachedCallback implements IAchievementReached {

        public TestResultStatus resultStatus = TestResultStatus.PENDING;

        @Override
        public void onLootsieBarClosed() {
            System.out.println("AchievementReachedCallback: onLootsieBarClosed");
        }

        @Override
        public void onLootsieBarExpanded() {
            System.out.println("AchievementReachedCallback: onLootsieBarExpanded");
        }

        @Override
        public void onLootsieFailed(String mesg) {
            System.out.println("AchievementReachedCallback: onLootsieFailed: " + mesg);
            resultStatus = TestResultStatus.FAILURE;
        }

        @Override
        public void onLootsieSuccess() {
            System.out.println("AchievementReachedCallback: onLootsieSuccess");
            resultStatus = TestResultStatus.SUCCESS;
        }

        @Override
        public void onNotificationData(String json) {
            System.out.println("AchievementReachedCallback: onNotificationData: " + json);
        }
    }


    @Test
    public void testSequence_AchievementReachedSequence() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_AchievementReachedSequence");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();
            DataModel.app = new App();

            String getAppResultJsonStr = "{\"achievements\": [" +
                    "{\"repeatable\": true, \"description\": \"Welcome back to Lootsie!  Keep on Lootin!\", \"lp\": 20, \"is_achieved\": false, \"id\": \"back1\", \"name\": \"Welcome Back!\"}," +
                    " {\"repeatable\": true, \"description\": \"Want even more LP?  Play our games!\", \"lp\": 20, \"is_achieved\": false, \"id\": \"back2\", \"name\": \"Wow!  You really like us!\"}," +
                    " {\"repeatable\": true, \"description\": \"Congratulations for installing Lootsie!!!  Here's some LP!  Happy Looting!!!\", \"lp\": 20, \"is_achieved\": false, \"id\": \"installed\", \"name\": \"Installed Lootsie!\"}" +
                    "]," +
                    " \"orientation\": \"automatic\", " +
                    " \"pos_x\": \"\", " +
                    " \"image_urls\": {" +
                    " \"S\": \"https://d1no9q2xfha3ui.cloudfront.net/live/dp-image-s/b730776f558955414898b80b7c09778038466500b8e5760ed4371ed9419a2796\"," +
                    " \"M\": null, " +
                    " \"L\": null," +
                    " \"XL\": null," +
                    " \"DETAIL\": null}," +
                    " \"ian_css\": \"https://lootsie.s3.amazonaws.com/live/ian-css/ian.css\"," +
                    " \"hook_type\": \"banner\"," +
                    " \"more_apps\": 1," +
                    " \"achievable_lp\": \"11\"," +
                    " \"duration\": 0.0," +
                    " \"total_lp_earned\": 0," +
                    " \"text_color\": {\"B\": 0, \"R\": 0, \"G\": 0}," +
                    " \"background_color\": {\"B\": 0, \"R\": 255, \"G\": 240}," +
                    " \"id\": \"173\"," +
                    " \"genres\": [{\"id\": 1, \"name\": \"Puzzle\"}]," +
                    " \"pos_y\": \"\"," +
                    " \"name\": \"AAAAAAAAAA Lootsie Hybrid\"," +
                    " \"notification\": \"top_left\"," +
                    " \"engagements\": [{\"id\": \"12\", \"lp\": 5, \"name\": \"Facebook Share\"}," +
                    " {\"id\": \"14\", \"lp\": 5, \"name\": \"Twitter Share\"}]}";
            JSONObject json = new JSONObject(getAppResultJsonStr);
            DataModel.app.parseFromJSON(json);


            RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            RestClient.addHeader(Headers.SESSION_TOKEN, "d6d14d08-1152-4d42-b8c3-4d29aec4213a");
            RestClient.addHeader(Headers.USER_TOKEN, "656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214");

            LootsieEngine.setNotificationConfiguration(LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_disabled);

            AchievementReachedCallback achievementReachedCallback = new AchievementReachedCallback();

            // {"total_lp": 40, "lp": 20}
            MockedRestPostUserAchievementsTask restPostUserAchievementsTask = new MockedRestPostUserAchievementsTask();

            AchievementReachedSequence.restPostUserAchievementsTask = restPostUserAchievementsTask;
            AchievementReachedSequence.start("back1", achievementReachedCallback);

            org.junit.Assert.assertTrue("Total LP is invalid for user", DataModel.user.totalLp == 40);
            org.junit.Assert.assertTrue("Achieveed LP is invalid for user", DataModel.user.achievedLp == 20);

            org.junit.Assert.assertTrue("AchievementReachedCallback should be success", achievementReachedCallback.resultStatus == TestResultStatus.SUCCESS);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_AchievementReachedSequence Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_AchievementReachedSequence finished");
    }


    @Test
    public void testSequence_AchievementReachedSequence_already_registered() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_AchievementReachedSequence_already_registered");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();
            DataModel.app = new App();

            String getAppResultJsonStr = "{\"achievements\": [" +
                    "{\"repeatable\": true, \"description\": \"Welcome back to Lootsie!  Keep on Lootin!\", \"lp\": 20, \"is_achieved\": false, \"id\": \"back1\", \"name\": \"Welcome Back!\"}," +
                    " {\"repeatable\": true, \"description\": \"Want even more LP?  Play our games!\", \"lp\": 20, \"is_achieved\": false, \"id\": \"back2\", \"name\": \"Wow!  You really like us!\"}," +
                    " {\"repeatable\": true, \"description\": \"Congratulations for installing Lootsie!!!  Here's some LP!  Happy Looting!!!\", \"lp\": 20, \"is_achieved\": false, \"id\": \"installed\", \"name\": \"Installed Lootsie!\"}" +
                    "]," +
                    " \"orientation\": \"automatic\", " +
                    " \"pos_x\": \"\", " +
                    " \"image_urls\": {" +
                    " \"S\": \"https://d1no9q2xfha3ui.cloudfront.net/live/dp-image-s/b730776f558955414898b80b7c09778038466500b8e5760ed4371ed9419a2796\"," +
                    " \"M\": null, " +
                    " \"L\": null," +
                    " \"XL\": null," +
                    " \"DETAIL\": null}," +
                    " \"ian_css\": \"https://lootsie.s3.amazonaws.com/live/ian-css/ian.css\"," +
                    " \"hook_type\": \"banner\"," +
                    " \"more_apps\": 1," +
                    " \"achievable_lp\": \"11\"," +
                    " \"duration\": 0.0," +
                    " \"total_lp_earned\": 0," +
                    " \"text_color\": {\"B\": 0, \"R\": 0, \"G\": 0}," +
                    " \"background_color\": {\"B\": 0, \"R\": 255, \"G\": 240}," +
                    " \"id\": \"173\"," +
                    " \"genres\": [{\"id\": 1, \"name\": \"Puzzle\"}]," +
                    " \"pos_y\": \"\"," +
                    " \"name\": \"AAAAAAAAAA Lootsie Hybrid\"," +
                    " \"notification\": \"top_left\"," +
                    " \"engagements\": [{\"id\": \"12\", \"lp\": 5, \"name\": \"Facebook Share\"}," +
                    " {\"id\": \"14\", \"lp\": 5, \"name\": \"Twitter Share\"}]}";
            JSONObject json = new JSONObject(getAppResultJsonStr);
            DataModel.app.parseFromJSON(json);


            RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            RestClient.addHeader(Headers.SESSION_TOKEN, "d6d14d08-1152-4d42-b8c3-4d29aec4213a");
            RestClient.addHeader(Headers.USER_TOKEN, "656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214");

            LootsieEngine.setNotificationConfiguration(LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_disabled);

            AchievementReachedCallback achievementReachedCallback = new AchievementReachedCallback();

            // {"total_lp": 40, "lp": 20}
            //MockedRestPostUserAchievementsTask restPostUserAchievementsTask = new MockedRestPostUserAchievementsTask();
            MockedRestGenericJSONObjectResponseTask restPostUserAchievementsTask = new MockedRestGenericJSONObjectResponseTask();
            RestResult response = new RestResult();
            // 409 Conflict {"errors": [{"field": "achievement_ids", "message": "already registered", "achievement_id": "l5_5stars"}]}
            response.status = 409;
            response.content = "{\"errors\": [{\"field\": \"achievement_ids\", \"message\": \"already registered\", \"achievement_id\": \"l5_5stars\"}]}";
            restPostUserAchievementsTask.setRestResult(response);

            AchievementReachedSequence.restPostUserAchievementsTask = restPostUserAchievementsTask;
            AchievementReachedSequence.start("back1", achievementReachedCallback);

            // no new LP for user!
            org.junit.Assert.assertTrue("Total LP is invalid for user", DataModel.user.totalLp == 0);
            org.junit.Assert.assertTrue("Achieveed LP is invalid for user", DataModel.user.achievedLp == 0);

            org.junit.Assert.assertTrue("AchievementReachedCallback should be failed", achievementReachedCallback.resultStatus == TestResultStatus.FAILURE);
            //org.junit.Assert.assertTrue("AchievementReachedCallback should be success", achievementReachedCallback.resultStatus == TestResultStatus.SUCCESS);



        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_AchievementReachedSequence_already_registered Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_AchievementReachedSequence_already_registered finished");
    }

    @Test
    public void testSequence_AchievementReachedSequence_invalid_achievement() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_AchievementReachedSequence_invalid_achievement");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();
            DataModel.app = new App();

            String getAppResultJsonStr = "{\"achievements\": [" +
                    "{\"repeatable\": true, \"description\": \"Welcome back to Lootsie!  Keep on Lootin!\", \"lp\": 20, \"is_achieved\": false, \"id\": \"back1\", \"name\": \"Welcome Back!\"}," +
                    " {\"repeatable\": true, \"description\": \"Want even more LP?  Play our games!\", \"lp\": 20, \"is_achieved\": false, \"id\": \"back2\", \"name\": \"Wow!  You really like us!\"}," +
                    " {\"repeatable\": true, \"description\": \"Congratulations for installing Lootsie!!!  Here's some LP!  Happy Looting!!!\", \"lp\": 20, \"is_achieved\": false, \"id\": \"installed\", \"name\": \"Installed Lootsie!\"}" +
                    "]," +
                    " \"orientation\": \"automatic\", " +
                    " \"pos_x\": \"\", " +
                    " \"image_urls\": {" +
                    " \"S\": \"https://d1no9q2xfha3ui.cloudfront.net/live/dp-image-s/b730776f558955414898b80b7c09778038466500b8e5760ed4371ed9419a2796\"," +
                    " \"M\": null, " +
                    " \"L\": null," +
                    " \"XL\": null," +
                    " \"DETAIL\": null}," +
                    " \"ian_css\": \"https://lootsie.s3.amazonaws.com/live/ian-css/ian.css\"," +
                    " \"hook_type\": \"banner\"," +
                    " \"more_apps\": 1," +
                    " \"achievable_lp\": \"11\"," +
                    " \"duration\": 0.0," +
                    " \"total_lp_earned\": 0," +
                    " \"text_color\": {\"B\": 0, \"R\": 0, \"G\": 0}," +
                    " \"background_color\": {\"B\": 0, \"R\": 255, \"G\": 240}," +
                    " \"id\": \"173\"," +
                    " \"genres\": [{\"id\": 1, \"name\": \"Puzzle\"}]," +
                    " \"pos_y\": \"\"," +
                    " \"name\": \"AAAAAAAAAA Lootsie Hybrid\"," +
                    " \"notification\": \"top_left\"," +
                    " \"engagements\": [{\"id\": \"12\", \"lp\": 5, \"name\": \"Facebook Share\"}," +
                    " {\"id\": \"14\", \"lp\": 5, \"name\": \"Twitter Share\"}]}";
            JSONObject json = new JSONObject(getAppResultJsonStr);
            DataModel.app.parseFromJSON(json);


            RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            RestClient.addHeader(Headers.SESSION_TOKEN, "d6d14d08-1152-4d42-b8c3-4d29aec4213a");
            RestClient.addHeader(Headers.USER_TOKEN, "656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214");

            LootsieEngine.setNotificationConfiguration(LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_disabled);

            AchievementReachedCallback achievementReachedCallback = new AchievementReachedCallback();

            // {"total_lp": 40, "lp": 20}
            //MockedRestPostUserAchievementsTask restPostUserAchievementsTask = new MockedRestPostUserAchievementsTask();
            MockedRestGenericJSONObjectResponseTask restPostUserAchievementsTask = new MockedRestGenericJSONObjectResponseTask();
            RestResult response = new RestResult();
            // 400 Bad Request {"errors": [{"field": "achievement_ids", "message": "invalid"}]}
            response.status = 400;
            response.content = "{\"errors\": [{\"field\": \"achievement_ids\", \"message\": \"invalid\"}]}";
            restPostUserAchievementsTask.setRestResult(response);

            AchievementReachedSequence.restPostUserAchievementsTask = restPostUserAchievementsTask;
            AchievementReachedSequence.start("back1", achievementReachedCallback);

            // no new LP for user!
            org.junit.Assert.assertTrue("Total LP is invalid for user", DataModel.user.totalLp == 0);
            org.junit.Assert.assertTrue("Achieveed LP is invalid for user", DataModel.user.achievedLp == 0);

            org.junit.Assert.assertTrue("AchievementReachedCallback should be failed", achievementReachedCallback.resultStatus == TestResultStatus.FAILURE);
            //org.junit.Assert.assertTrue("AchievementReachedCallback should be success", achievementReachedCallback.resultStatus == TestResultStatus.SUCCESS);



        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_AchievementReachedSequence_invalid_achievement Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_AchievementReachedSequence_invalid_achievement finished");
    }

}
