package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;

/**
 * Created by jerrylootsie on 7/14/15.
 */
public class MockedRestGenericStringResponseTask implements IGenericAsyncTask<String> {
    IRESTCallback callback = null;

    RestResult response = null;

    public MockedRestGenericStringResponseTask() {
        response = new RestResult();
        response.status = 200;
        response.content = "{}";
    }

    public void setRestResult(RestResult inputResponse) {
        response = inputResponse;
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        System.out.println("MockedRestGenericStringResponseTask: execute");

        if (callback != null) {
            callback.restResult(response);
        }
    }
}
