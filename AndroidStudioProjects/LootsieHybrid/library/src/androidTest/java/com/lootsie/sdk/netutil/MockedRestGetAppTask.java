package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.netutil.RestResult;

/**
 * Created by jerrylootsie on 7/10/15.
 */
public class MockedRestGetAppTask implements IGenericAsyncTask<String> {
    IRESTCallback callback = null;

    public MockedRestGetAppTask() {
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        System.out.println("MockedRestGetAppTask: execute");

        RestResult response = new RestResult();
        response.status = 200;

        // {"achievements": [{"repeatable": true, "description": "Welcome back to Lootsie!  Keep on Lootin!", "lp": 20, "is_achieved": false, "id": "back1", "name": "Welcome Back!"}, {"repeatable": true, "description": "Want even more LP?  Play our games!", "lp": 20, "is_achieved": false, "id": "back2", "name": "Wow!  You really like us!"}, {"repeatable": true, "description": "Congratulations for installing Lootsie!!!  Here's some LP!  Happy Looting!!!", "lp": 20, "is_achieved": false, "id": "installed", "name": "Installed Lootsie!"}], "orientation": "automatic", "pos_x": "", "image_urls": {"S": "https://d1no9q2xfha3ui.cloudfront.net/live/dp-image-s/b730776f558955414898b80b7c09778038466500b8e5760ed4371ed9419a2796", "M": null, "L": null, "XL": null, "DETAIL": null}, "ian_css": "https://lootsie.s3.amazonaws.com/live/ian-css/ian.css", "hook_type": "banner", "more_apps": 1, "achievable_lp": "11", "duration": 0.0, "total_lp_earned": 0, "text_color": {"B": 0, "R": 0, "G": 0}, "background_color": {"B": 0, "R": 255, "G": 240}, "id": "173", "genres": [{"id": 1, "name": "Puzzle"}], "pos_y": "", "name": "AAAAAAAAAA Lootsie Hybrid", "notification": "top_left", "engagements": [{"id": "12", "lp": 5, "name": "Facebook Share"}, {"id": "14", "lp": 5, "name": "Twitter Share"}]}
        response.content = "{\"achievements\": [" +
                "{\"repeatable\": true, \"description\": \"Welcome back to Lootsie!  Keep on Lootin!\", \"lp\": 20, \"is_achieved\": false, \"id\": \"back1\", \"name\": \"Welcome Back!\"}," +
                " {\"repeatable\": true, \"description\": \"Want even more LP?  Play our games!\", \"lp\": 20, \"is_achieved\": false, \"id\": \"back2\", \"name\": \"Wow!  You really like us!\"}," +
                " {\"repeatable\": true, \"description\": \"Congratulations for installing Lootsie!!!  Here's some LP!  Happy Looting!!!\", \"lp\": 20, \"is_achieved\": false, \"id\": \"installed\", \"name\": \"Installed Lootsie!\"}" +
                "]," +
                " \"orientation\": \"automatic\", " +
                " \"pos_x\": \"\", " +
                " \"image_urls\": {" +
                " \"S\": \"https://d1no9q2xfha3ui.cloudfront.net/live/dp-image-s/b730776f558955414898b80b7c09778038466500b8e5760ed4371ed9419a2796\"," +
                " \"M\": null, " +
                " \"L\": null," +
                " \"XL\": null," +
                " \"DETAIL\": null}," +
                " \"ian_css\": \"https://lootsie.s3.amazonaws.com/live/ian-css/ian.css\"," +
                " \"hook_type\": \"banner\"," +
                " \"more_apps\": 1," +
                " \"achievable_lp\": \"11\"," +
                " \"duration\": 0.0," +
                " \"total_lp_earned\": 0," +
                " \"text_color\": {\"B\": 0, \"R\": 0, \"G\": 0}," +
                " \"background_color\": {\"B\": 0, \"R\": 255, \"G\": 240}," +
                " \"id\": \"173\"," +
                " \"genres\": [{\"id\": 1, \"name\": \"Puzzle\"}]," +
                " \"pos_y\": \"\"," +
                " \"name\": \"AAAAAAAAAA Lootsie Hybrid\"," +
                " \"notification\": \"top_left\"," +
                " \"engagements\": [{\"id\": \"12\", \"lp\": 5, \"name\": \"Facebook Share\"}," +
                " {\"id\": \"14\", \"lp\": 5, \"name\": \"Twitter Share\"}]}";


        if (callback != null) {
            callback.restResult(response);
        }
    }
}
