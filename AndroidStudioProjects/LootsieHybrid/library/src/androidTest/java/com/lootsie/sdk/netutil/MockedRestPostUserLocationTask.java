package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.netutil.RestResult;

/**
 * Created by jerrylootsie on 7/10/15.
 */
public class MockedRestPostUserLocationTask implements IGenericAsyncTask<String> {
    IRESTCallback callback = null;

    public MockedRestPostUserLocationTask() {
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        System.out.println("MockedRestPostUserLocationTask: execute");

        RestResult response = new RestResult();
        response.status = 200;
        response.content = "{}";

        if (callback != null) {
            callback.restResult(response);
        }
    }
}