package com.lootsie.sdk;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import com.lootsie.sdk.callbacks.IInitializationCallback;
import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.model.Achievement;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.utils.LootsieGlobals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {

    private static String TAG = "ApplicationTest";

    public ApplicationTest() {
        super(Application.class);

        Log.d(TAG, "ApplicationTest");

    }

    @Before
    public void setUp() throws Exception {
        Log.d(TAG, "setUp");
        // The application has just launched, create Lootsie instance.
//		Context mContext = this.getInstrumentation().getTargetContext().getApplicationContext();

        //Lootsie.init((Application) this.getContext(), APP_KEY, APP_SECRET, null);
        this.createApplication();
        Application myApplication = getApplication();


        // create  a signal to let us know when our task is done.
        final CountDownLatch signal = new CountDownLatch(1);


        class InitCallback implements IInitializationCallback {


            @Override
            public void onInitSuccess() {
                Log.v(TAG, "setUp: onInitSuccess");

                signal.countDown();
            }

            @Override
            public void onInitFailure() {
                Log.v(TAG, "setUp: onInitFailure");

                signal.countDown();
            }
        }

        InitCallback initCallback = new InitCallback();


        Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
        Lootsie.init(this.getApplication(), LootsieGlobals.APPKEY, initCallback);

        /* The testing thread will wait here until the UI thread releases it
         * above with the countDown() or 30 seconds passes and it times out.
         */
        signal.await(30, TimeUnit.SECONDS);

    }

    @After
    public void tearDown() throws Exception {
        Log.d(TAG, "tearDown");
    }

    @Test
    public void testGetAppAchievements() {
        Log.v(TAG, "testGetAppAchievements");

        ArrayList<Achievement> appAchievements = null;
        appAchievements = Lootsie.getAppAchievements();

        for (int i = 0; i < appAchievements.size(); i++) {
            Achievement achievement = appAchievements.get(i);

            Log.v(TAG, "testGetUserAchievements: achievement["+i+"]: " + achievement.toString());
        }

        assertTrue(appAchievements != null);
        if (appAchievements != null) {
            assertTrue("achievements is empty!", appAchievements.size() > 0);
        }
    }


    @Test
    public void testGetUserRewards() {

        Log.v(TAG, "testGetUserRewards");


        ArrayList<Reward> userRewards = null;
        userRewards = Lootsie.getUserRewards();

        if ((userRewards != null) && (userRewards.size() > 0)) {
            for (int rewardIndex = 0; rewardIndex < userRewards.size(); rewardIndex++) {
                Reward reward = userRewards.get(rewardIndex);

                Log.v(TAG, "testGetUserRewards: reward["+rewardIndex+"]: " + reward.toString());

            }
        }

        assertTrue(userRewards != null);
        if (userRewards != null) {
            assertTrue("user rewards is empty!", userRewards.size() > 0);
        }


    }
}