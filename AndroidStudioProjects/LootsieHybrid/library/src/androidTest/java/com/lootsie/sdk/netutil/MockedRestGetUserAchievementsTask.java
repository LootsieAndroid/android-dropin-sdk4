package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.netutil.RestResult;

import java.util.Map;

/**
 * Created by jerrylootsie on 7/10/15.
 */
public class MockedRestGetUserAchievementsTask implements IGenericAsyncTask<String> {

    IRESTCallback callback = null;

    public MockedRestGetUserAchievementsTask() {
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        System.out.println("MockedRestGetUserAchievementsTask: execute");

        if (params == null || params.length == 0) {
            return;
        }

        RestResult response = new RestResult();
        response.status = 200;
        response.content = "{\"achievements\": [{\"date\": \"2015-07-10T18:26:08\", \"id\": \"back1\", \"lp\": 20}], \"total_lp\": 20, \"achieved_lp\": 20}";

        if (callback != null) {
            callback.restResult(response);
        }
    }
}