package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.netutil.RestResult;

/**
 * Created by jerrylootsie on 7/10/15.
 */
public class MockedRestGetUserRewardTask implements IGenericAsyncTask<String> {

    IRESTCallback callback = null;

    public MockedRestGetUserRewardTask() {
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        System.out.println("MockedRestGetUserRewardTask: execute");

        if (params == null || params.length == 0) {
            return;
        }

        RestResult response = new RestResult();
        response.status = 200;
        response.content = "{\"rewards\": [" +
                "{\"engagements\": [" +
                "{\"id\": \"12\", \"lp\": 5, \"name\": \"Facebook Share\"}, " +
                "{\"id\": \"14\", \"lp\": 5, \"name\": \"Twitter Share\"}], " +
                "\"name\": \"40% off\", \"redemptions_remaining\": 1, \"tags\": [], \"brand_name\": \"Shari's Berries\", \"image_urls\": {\"S\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/508/thumb/open-uri20150401-29036-74uhy8?1427899832\", \"M\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/508/large/open-uri20150401-29036-74uhy8?1427899832\", \"L\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/508/large/open-uri20150401-29036-74uhy8?1427899832\", \"XL\": null, \"DETAIL\": null}, \"is_new\": 0, \"is_limited_time\": 0, \"tos_text\": \"\\nElectronic coupons are valid on online orders only; not\\nvalid in retail stores. Does not apply to previously placed\\norders. Does not apply to gift wrap or gift certificates.\\nEntire coupon value must be used in one transaction. May\\nnot be resold or combined with other offers; other\\nrestrictions may apply. Coupons may not be returned or\\ncancelled after purchase. Coupons will not be replaced\\nif lost or stolen. We reserve the right to refuse, cancel\\nor hold for review coupons and orders for suspected fraud,\\nfor coupons mistakenly issued in an incorrect denomination,\\nor for other violations of coupon policies.\\n                \", \"lp\": 50, \"text_to_share\": \"I just got a 40% off from @lootsie by playing my favorite games! http://lootsie.com\", \"tos_url\": \"https://www.lootsie.com/#/rewards/22-e/terms-of-service\", \"rewards_remaining\": 1, \"id\": \"22-e\", \"description\": \" Save 40% on Half Dozen Gourmet Dipped Fancy Strawberries\"}, " +
                "{\"engagements\": [" +
                "{\"id\": \"12\", \"lp\": 5, \"name\": \"Facebook Share\"}, " +
                "{\"id\": \"14\", \"lp\": 5, \"name\": \"Twitter Share\"}]," +
                "\"name\": \"$5 off $50 or more\", \"redemptions_remaining\": 1, \"tags\": [], \"brand_name\": \"Chefs Catalog\", \"image_urls\": {\"S\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/471/thumb/chefs-catalog.jpg?1431549954\", \"M\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/471/large/chefs-catalog.jpg?1431549954\", \"L\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/471/large/chefs-catalog.jpg?1431549954\", \"XL\": null, \"DETAIL\": null}, \"is_new\": 0, \"is_limited_time\": 0, \"tos_text\": \"\\nElectronic coupons are valid on online orders only; not\\nvalid in retail stores. Does not apply to previously placed\\norders. Does not apply to gift wrap or gift certificates.\\nEntire coupon value must be used in one transaction. May\\nnot be resold or combined with other offers; other\\nrestrictions may apply. Coupons may not be returned or\\ncancelled after purchase. Coupons will not be replaced\\nif lost or stolen. We reserve the right to refuse, cancel\\nor hold for review coupons and orders for suspected fraud,\\nfor coupons mistakenly issued in an incorrect denomination,\\nor for other violations of coupon policies.\\n                \", \"lp\": 50, \"text_to_share\": \"I just got a $5 off $50 or more from @lootsie by playing my favorite games! http://lootsie.com\", \"tos_url\": \"https://www.lootsie.com/#/rewards/42-e/terms-of-service\", \"rewards_remaining\": 1, \"id\": \"42-e\", \"description\": \"$5 Off $50 Purchase at CHEFSCatalog.com!\"}, " +
                "{\"engagements\": [" +
                "{\"id\": \"12\", \"lp\": 5, \"name\": \"Facebook Share\"}, " +
                "{\"id\": \"14\", \"lp\": 5, \"name\": \"Twitter Share\"}], " +
                "\"name\": \"15% off \", \"redemptions_remaining\": 1, \"tags\": [], \"brand_name\": \"Pro Flowers\", \"image_urls\": {\"S\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/637/thumb/proflowers-logo.jpg?1431549843\", \"M\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/637/large/proflowers-logo.jpg?1431549843\", \"L\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/637/large/proflowers-logo.jpg?1431549843\", \"XL\": null, \"DETAIL\": null}, \"is_new\": 0, \"is_limited_time\": 0, \"tos_text\": \"\\nElectronic coupons are valid on online orders only; not\\nvalid in retail stores. Does not apply to previously placed\\norders. Does not apply to gift wrap or gift certificates.\\nEntire coupon value must be used in one transaction. May\\nnot be resold or combined with other offers; other\\nrestrictions may apply. Coupons may not be returned or\\ncancelled after purchase. Coupons will not be replaced\\nif lost or stolen. We reserve the right to refuse, cancel\\nor hold for review coupons and orders for suspected fraud,\\nfor coupons mistakenly issued in an incorrect denomination,\\nor for other violations of coupon policies.\\n                \", \"lp\": 70, \"text_to_share\": \"I just got a 15% off  from @lootsie by playing my favorite games! http://lootsie.com\", \"tos_url\": \"https://www.lootsie.com/#/rewards/47-e/terms-of-service\", \"rewards_remaining\": 1, \"id\": \"47-e\", \"description\": \" 15% off Birthday Flowers & Gifts at ProFlowers\"}, " +
                "{\"engagements\": [" +
                "{\"id\": \"12\", \"lp\": 5, \"name\": \"Facebook Share\"}, " +
                "{\"id\": \"14\", \"lp\": 5, \"name\": \"Twitter Share\"}], " +
                "\"name\": \"Get a $10 Aeropostale gift card when you spend $20 at Aeropostale\", \"redemptions_remaining\": 1, \"tags\": [], \"brand_name\": \"Aeropostale\", \"image_urls\": {\"S\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/615/thumb/aeropostale_logo__jpg_280x280_crop_q95.jpg?1431545158\", \"M\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/615/large/aeropostale_logo__jpg_280x280_crop_q95.jpg?1431545158\", \"L\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/615/large/aeropostale_logo__jpg_280x280_crop_q95.jpg?1431545158\", \"XL\": null, \"DETAIL\": null}, \"is_new\": 0, \"is_limited_time\": 0, \"tos_text\": \"\\nElectronic coupons are valid on online orders only; not\\nvalid in retail stores. Does not apply to previously placed\\norders. Does not apply to gift wrap or gift certificates.\\nEntire coupon value must be used in one transaction. May\\nnot be resold or combined with other offers; other\\nrestrictions may apply. Coupons may not be returned or\\ncancelled after purchase. Coupons will not be replaced\\nif lost or stolen. We reserve the right to refuse, cancel\\nor hold for review coupons and orders for suspected fraud,\\nfor coupons mistakenly issued in an incorrect denomination,\\nor for other violations of coupon policies.\\n                \", \"lp\": 30, \"text_to_share\": \"I just got a Get a $10 Aeropostale gift card when you spend $20 at Aeropostale from @lootsie by playing my favorite games! http://lootsie.com\", \"tos_url\": \"https://www.lootsie.com/#/rewards/51-e/terms-of-service\", \"rewards_remaining\": 1, \"id\": \"51-e\", \"description\": \"Get a $10 Aeropostale gift card when you spend $20 at Aeropostale!\"}, " +
                "{\"engagements\": [" +
                "{\"id\": \"12\", \"lp\": 5, \"name\": \"Facebook Share\"}, " +
                "{\"id\": \"14\", \"lp\": 5, \"name\": \"Twitter Share\"}], " +
                "\"name\": \"15% off\", \"redemptions_remaining\": 1, \"tags\": [], \"brand_name\": \"Cherry Moon Farms\", \"image_urls\": {\"S\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/894/thumb/open-uri20150601-4960-heqq5i?1433175615\", \"M\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/894/large/open-uri20150601-4960-heqq5i?1433175615\", \"L\": \"https://s3.amazonaws.com/rewardops_production/offers/images/000/000/894/large/open-uri20150601-4960-heqq5i?1433175615\", \"XL\": null, \"DETAIL\": null}, \"is_new\": 0, \"is_limited_time\": 0, \"tos_text\": \"\\nElectronic coupons are valid on online orders only; not\\nvalid in retail stores. Does not apply to previously placed\\norders. Does not apply to gift wrap or gift certificates.\\nEntire coupon value must be used in one transaction. May\\nnot be resold or combined with other offers; other\\nrestrictions may apply. Coupons may not be returned or\\ncancelled after purchase. Coupons will not be replaced\\nif lost or stolen. We reserve the right to refuse, cancel\\nor hold for review coupons and orders for suspected fraud,\\nfor coupons mistakenly issued in an incorrect denomination,\\nor for other violations of coupon policies.\\n                \", \"lp\": 30, \"text_to_share\": \"I just got a 15% off from @lootsie by playing my favorite games! http://lootsie.com\", \"tos_url\": \"https://www.lootsie.com/#/rewards/53-e/terms-of-service\", \"rewards_remaining\": 1, \"id\": \"53-e\", \"description\": \" 15% off Birthday Treats & Gift Baskets (min $29)\"}" +
                "]}";

        if (callback != null) {
            callback.restResult(response);
        }
    }
}