package com.lootsie.sdk;

import org.junit.runners.model.InitializationError;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.manifest.AndroidManifest;
import org.robolectric.res.FileFsFile;
import org.robolectric.res.FsFile;

/**
 * More dynamic path resolution.
 *
 * This workaround is only for Mac Users necessary and only if they don't use the $MODULE_DIR$
 * workaround. Follow this issue at https://code.google.com/p/android/issues/detail?id=158015
 */
public class CustomRobolectricRunner extends RobolectricGradleTestRunner {

    private static int debugLevel = 0;

    public CustomRobolectricRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    protected AndroidManifest getAppManifest(Config config) {
        AndroidManifest appManifest = super.getAppManifest(config);
        FsFile androidManifestFile = appManifest.getAndroidManifestFile();

        if (debugLevel > 0) System.out.println("CustomRobolectricRunner: getAppManifest");

        if (androidManifestFile.exists()) {
            if (debugLevel > 0) System.out.println("CustomRobolectricRunner: getAppManifest1: " + androidManifestFile.getPath());
            return appManifest;
        } else {
            String moduleRoot = getModuleRootPath(config);

            if (debugLevel > 0) System.out.println("CustomRobolectricRunner: getAppManifest2: moduleRootPath: " + moduleRoot);
            if (debugLevel > 0) System.out.println("CustomRobolectricRunner: getAppManifest2: getAndroidManifestFile: " + appManifest.getAndroidManifestFile().getPath());

            String hardCodedManifestPath = "build/intermediates/manifests/androidTest/debug/AndroidManifest.xml";
            if (debugLevel > 0) System.out.println("CustomRobolectricRunner: getAppManifest2: getAndroidManifestFile: " + hardCodedManifestPath);

            //androidManifestFile = FileFsFile.from(moduleRoot, appManifest.getAndroidManifestFile().getPath());
            androidManifestFile = FileFsFile.from(moduleRoot, hardCodedManifestPath);
            FsFile resDirectory = FileFsFile.from(moduleRoot, appManifest.getAndroidManifestFile().getPath().replace("AndroidManifest.xml", "res"));
            FsFile assetsDirectory = FileFsFile.from(moduleRoot, appManifest.getAndroidManifestFile().getPath().replace("AndroidManifest.xml", "assets"));

            if (debugLevel > 0) System.out.println("CustomRobolectricRunner: getAppManifest2: " + androidManifestFile.getPath());

            return new AndroidManifest(androidManifestFile, resDirectory, assetsDirectory);
        }
    }

    private String getModuleRootPath(Config config) {
        String moduleRoot = config.constants().getResource("").toString().replace("file:", "").replace("jar:", "");

        if (debugLevel > 0) System.out.println("CustomRobolectricRunner: getModuleRootPath");

        return moduleRoot.substring(0, moduleRoot.indexOf("/build"));
    }
}
