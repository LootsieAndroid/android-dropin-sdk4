package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.netutil.RestResult;

/**
 * Created by jerrylootsie on 7/10/15.
 */
public class MockedRestGetUserAccountTask implements IGenericAsyncTask<String> {

    IRESTCallback callback = null;

    public MockedRestGetUserAccountTask() {
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        System.out.println("MockedRestGetUserAccountTask: execute");

        if (params == null || params.length == 0) {
            return;
        }

        RestResult response = new RestResult();
        response.status = 200;
        response.content = "{\"registered_by_facebook\": false," +
                " \"last_name\": \"\", " +
                "\"accepted_tos\": false, " +
                "\"partner_optin\": false, " +
                "\"total_lp\": 14, " +
                "\"address\": \"\", " +
                "\"photo_url\": null," +
                "\"city\": \"\", " +
                "\"first_name\": \"\", " +
                "\"gender\": \"\", " +
                "\"confirmed_age\": 0, " +
                "\"zipcode\": null, " +
                "\"birthdate\": null, " +
                "\"lootsie_optin\": false, " +
                "\"state\": \"\", " +
                "\"overall_lp\": \"14\", " +
                "\"is_guest\": false, " +
                "\"email\": \"lootsietest+3@lootsie.com\"}";

        if (callback != null) {
            callback.restResult(response);
        }
    }
}