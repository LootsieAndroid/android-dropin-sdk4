package com.lootsie.sdk;

import com.lootsie.sdk.callbacks.ISignOutCallback;
import com.lootsie.sdk.device.Device;
import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.sequences.InitSequence;
import com.lootsie.sdk.lootsiehybrid.sequences.ResetUserSequence;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.net.Headers;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.netutil.MockedRestGenericStringResponseTask;
import com.lootsie.sdk.netutil.MockedRestGetAppTask;
import com.lootsie.sdk.netutil.MockedRestGetSessionTask;
import com.lootsie.sdk.netutil.MockedRestGetSettingsTask;
import com.lootsie.sdk.netutil.MockedRestGetUserAccountTask;
import com.lootsie.sdk.netutil.MockedRestGetUserAchievementsTask;
import com.lootsie.sdk.netutil.MockedRestGetUserRewardTask;
import com.lootsie.sdk.netutil.MockedRestPostUserSessionGuestTask;
import com.lootsie.sdk.netutil.MockedTrackingSessionAsyncTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Created by jerrylootsie on 7/14/15.
 */
@Config(constants = BuildConfig.class, sdk = 21)
@RunWith(CustomRobolectricRunner.class)
public class RobolectricTest_ResetUserSequence {

    static enum TestResultStatus {
        SUCCESS,
        PENDING,
        FAILURE;

        private TestResultStatus() {
        }
    }

    class SignOutCallback implements ISignOutCallback {

        public TestResultStatus resultStatus = TestResultStatus.PENDING;

        @Override
        public void onSignOutSuccess() {
            System.out.println("SignOutCallback: SignOutSuccess");
            resultStatus = TestResultStatus.SUCCESS;
        }

        @Override
        public void onSignOutFailure() {
            System.out.println("SignOutCallback: SignOutFailure");
            resultStatus = TestResultStatus.FAILURE;
        }

    }

    @Test
    public void testSequence_ResetUserSequence() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_ResetUserSequence");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            Device device = new Device(testInstance);
            // default location services off
            device.build(false);


            DataModel.user = new User();

            RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            RestClient.addHeader(Headers.SESSION_TOKEN, "d6d14d08-1152-4d42-b8c3-4d29aec4213a");
            RestClient.addHeader(Headers.USER_TOKEN, "656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214");

            MockedRestGenericStringResponseTask restDeleteUserSessionTask = new MockedRestGenericStringResponseTask();
            RestResult response = new RestResult();
            response.status = 204;
            response.content ="{}";
            restDeleteUserSessionTask.setRestResult(response);

            SignOutCallback signoutCallback = new SignOutCallback();

            ResetUserSequence.restDeleteUserSessionTask = restDeleteUserSessionTask;

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;
            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;


            ResetUserSequence.start(LootsieGlobals.APPKEY, signoutCallback);

            org.junit.Assert.assertTrue("ResetUserSequence callback should succeed", signoutCallback.resultStatus == TestResultStatus.SUCCESS);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_ResetUserSequence Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_ResetUserSequence finished");
    }

    @Test
    public void testSequence_ResetUserSequence_appSecret_invalid() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_ResetUserSequence_appSecret_invalid");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            Device device = new Device(testInstance);
            // default location services off
            device.build(false);


            DataModel.user = new User();

            RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            RestClient.addHeader(Headers.SESSION_TOKEN, "d6d14d08-1152-4d42-b8c3-4d29aec4213a");
            RestClient.addHeader(Headers.USER_TOKEN, "656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214");

            MockedRestGenericStringResponseTask restDeleteUserSessionTask = new MockedRestGenericStringResponseTask();
            // 403 Forbidden {"errors": [{"field": "app_secret", "message": "invalid"}]}
            RestResult response = new RestResult();
            response.status = 403;
            response.content ="{\"errors\": [{\"field\": \"app_secret\", \"message\": \"invalid\"}]}";
            restDeleteUserSessionTask.setRestResult(response);

            SignOutCallback signoutCallback = new SignOutCallback();

            ResetUserSequence.restDeleteUserSessionTask = restDeleteUserSessionTask;

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;
            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;


            ResetUserSequence.start(LootsieGlobals.APPKEY, signoutCallback);

            org.junit.Assert.assertTrue("ResetUserSequence callback should fail", signoutCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_ResetUserSequence_appSecret_invalid Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_ResetUserSequence_appSecret_invalid finished");
    }

}
