package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.netutil.RestResult;

import java.util.Map;

/**
 * Created by jerrylootsie on 7/10/15.
 */
public class MockedRestGetSettingsTask implements IGenericAsyncTask<Map<String, Object>> {

    IRESTCallback callback = null;

    public MockedRestGetSettingsTask() {
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(Map<String, Object>... params) {
        System.out.println("MockedRestGetSettingsTask: execute");

        if (params == null || params.length == 0) {
            return;
        }

        RestResult response = new RestResult();
        response.status = 200;
        response.content = "{\"settings\": {\"currency_name\": \"Lootsie Points\", \"sdk.enabled\": \"true\", \"currency_abbreviation\": \"LP\", \"sdk.api.rewards.page-size\": \"100\"}}";

        if (callback != null) {
            callback.restResult(response);
        }
    }
}