package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;

import org.json.JSONObject;

/**
 * Created by jerrylootsie on 7/15/15.
 */
public class MockedRestGenericJSONObjectResponseTask implements IGenericAsyncTask<JSONObject> {
    IRESTCallback callback = null;

    RestResult response = null;

    public MockedRestGenericJSONObjectResponseTask() {
        response = new RestResult();
        response.status = 200;
        response.content = "{}";
    }

    public void setRestResult(RestResult inputResponse) {
        response = inputResponse;
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(JSONObject... params) {
        System.out.println("MockedRestGenericStringResponseTask: execute");

        if (callback != null) {
            callback.restResult(response);
        }
    }
}