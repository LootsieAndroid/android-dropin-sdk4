package com.lootsie.sdk;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class MyTestRunner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(MockitoTest.class);
        System.out.println("MyTestRunner: MockitoTest result: runCount: " + result.getRunCount());
        for (Failure failure : result.getFailures()) {
            System.err.println(failure.toString());
        }
        System.out.println("*******************************************************************************");

        result = JUnitCore.runClasses(RobolectricTest.class);
        System.out.println("MyTestRunner: RobolectricTest result: runCount: " + result.getRunCount());
        for (Failure failure : result.getFailures()) {
            System.err.println(failure.toString());
        }
        System.out.println("*******************************************************************************");

        result = JUnitCore.runClasses(RobolectricTest_AchievementReachedSequence.class);
        System.out.println("MyTestRunner: RobolectricTest_AchievementReachedSequence result: runCount: " + result.getRunCount());
        for (Failure failure : result.getFailures()) {
            System.err.println(failure.toString());
        }
        System.out.println("*******************************************************************************");

        result = JUnitCore.runClasses(RobolectricTest_InitSequence.class);
        System.out.println("MyTestRunner: RobolectricTest_InitSequence result: runCount: " + result.getRunCount());
        for (Failure failure : result.getFailures()) {
            System.err.println(failure.toString());
        }
        System.out.println("*******************************************************************************");

        result = JUnitCore.runClasses(RobolectricTest_MergePointsSequence.class);
        System.out.println("MyTestRunner: RobolectricTest_MergePointsSequence result: runCount: " + result.getRunCount());
        for (Failure failure : result.getFailures()) {
            System.err.println(failure.toString());
        }
        System.out.println("*******************************************************************************");

        result = JUnitCore.runClasses(RobolectricTest_RedemptionSequence.class);
        System.out.println("MyTestRunner: RobolectricTest_RedemptionSequence result: runCount: " + result.getRunCount());
        for (Failure failure : result.getFailures()) {
            System.err.println(failure.toString());
        }
        System.out.println("*******************************************************************************");

        result = JUnitCore.runClasses(RobolectricTest_ResetUserSequence.class);
        System.out.println("MyTestRunner: RobolectricTest_ResetUserSequence result: runCount: " + result.getRunCount());
        for (Failure failure : result.getFailures()) {
            System.err.println(failure.toString());
        }
        System.out.println("*******************************************************************************");

        result = JUnitCore.runClasses(RobolectricTest_UpdateAchievementPageSequence.class);
        System.out.println("MyTestRunner: RobolectricTest_UpdateAchievementPageSequence result: runCount: " + result.getRunCount());
        for (Failure failure : result.getFailures()) {
            System.err.println(failure.toString());
        }
        System.out.println("*******************************************************************************");

        result = JUnitCore.runClasses(RobolectricTest_UpdateUserAccountSequence.class);
        System.out.println("MyTestRunner: RobolectricTest_UpdateUserAccountSequence result: runCount: " + result.getRunCount());
        for (Failure failure : result.getFailures()) {
            System.err.println(failure.toString());
        }
        System.out.println("*******************************************************************************");

        System.exit(0);
    }
}
