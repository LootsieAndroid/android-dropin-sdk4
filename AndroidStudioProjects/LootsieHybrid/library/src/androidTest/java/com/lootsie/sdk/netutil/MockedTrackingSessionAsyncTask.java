package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.netutil.RestResult;

/**
 * Created by jerrylootsie on 7/10/15.
 */
public class MockedTrackingSessionAsyncTask implements IGenericAsyncTask<String> {

    IRESTCallback callback = null;

    public MockedTrackingSessionAsyncTask() {
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        System.out.println("MockedTrackingSessionAsyncTask: execute");

        if (params == null || params.length == 0) {
            return;
        }

        RestResult response = new RestResult();
        response.status = 200;
        response.content = "{}";

        if (callback != null) {
            callback.restResult(response);
        }
    }
}