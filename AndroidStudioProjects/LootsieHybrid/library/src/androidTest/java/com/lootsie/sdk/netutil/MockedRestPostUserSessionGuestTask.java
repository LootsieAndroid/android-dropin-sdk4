package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.netutil.RestResult;

/**
 * Created by jerrylootsie on 7/10/15.
 */
public class MockedRestPostUserSessionGuestTask implements IGenericAsyncTask<String> {
    IRESTCallback callback = null;

    public MockedRestPostUserSessionGuestTask() {
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        System.out.println("MockedRestPostUserSessionGuestTask: execute");

        RestResult response = new RestResult();
        response.status = 200;
        response.content = "{\"X-Lootsie-User-Session-Token\": \"656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214\"}";

        if (callback != null) {
            callback.restResult(response);
        }
    }
}