package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.netutil.RestResult;

/**
 * Created by jerrylootsie on 7/13/15.
 */
public class MockedRestPostUserRewardEmailRedemptionTask implements IGenericAsyncTask<String> {
    IRESTCallback callback = null;

    public MockedRestPostUserRewardEmailRedemptionTask() {
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        System.out.println("MockedRestPostUserRewardEmailRedemptionTask: execute");

        RestResult response = new RestResult();
        response.status = 200;
        //response.content = "{“reward_redemption_id”: 1, \"lp\": 50, \"total_lp\": 150}";
        response.content = "{\"reward_redemption_id\": \"-1\", \"lp\": 50, \"total_lp\": 150}";

        if (callback != null) {
            callback.restResult(response);
        }
    }
}
