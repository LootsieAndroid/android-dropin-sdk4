package com.lootsie.sdk;

import android.test.InstrumentationTestCase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.util.Log;

/**
 * Created by jerrylootsie on 3/17/15.
 */
public class ExampleTest extends InstrumentationTestCase {

    private static String TAG = "Lootsie ExampleTest";

    static Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    private static boolean isEmailValid(String email) {
        Matcher m = emailPattern.matcher(email);
        return m.matches();
    }

    public void test_email1() throws Exception {
//        final int expected = 1;
//        final int reality = 5;
//        assertEquals(expected, reality);

        String testEmail = "lootsietest@lootsie.com";
        //Log.v(TAG, "testEmail: " + testEmail);
        //System.out.println("testEmail: " + testEmail);
        //assertTrue(isEmailValid(testEmail));
        assertEquals("testEmail: " + testEmail, isEmailValid(testEmail), true);

    }

    public void test_email2() throws Exception {
        String testEmail = "lootsietest+1@lootsie.com";
        //Log.v(TAG, "testEmail: " + testEmail);
        //System.out.println("testEmail: " + testEmail);
        //assertTrue(isEmailValid(testEmail));
        assertEquals("testEmail: " + testEmail, isEmailValid(testEmail), true);


    }
}