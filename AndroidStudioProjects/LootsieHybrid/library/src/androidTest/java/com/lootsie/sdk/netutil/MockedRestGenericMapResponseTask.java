package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by jerrylootsie on 7/15/15.
 */
public class MockedRestGenericMapResponseTask implements IGenericAsyncTask<Map<String, Object>> {
    IRESTCallback callback = null;

    RestResult response = null;

    public MockedRestGenericMapResponseTask() {
        response = new RestResult();
        response.status = 200;
        response.content = "{}";
    }

    public void setRestResult(RestResult inputResponse) {
        response = inputResponse;
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(Map<String, Object>... params) {
        System.out.println("MockedRestGenericStringResponseTask: execute");

        if (callback != null) {
            callback.restResult(response);
        }
    }
}