package com.dev.game;

/**
 * Created by jerrylootsie on 2/9/15.
 */
import android.util.Log;

import com.lootsie.sdk.callbacks.IInitializationCallback;
import com.lootsie.sdk.utils.Logs;

public class InitCallback implements IInitializationCallback
{
    private static final String TAG = "InitCallback";
    MyActivity activity ;

    void setActivity(MyActivity activityctx)
    {
        activity = activityctx;
    }

    @Override
    public void onInitSuccess() {
        Logs.i(TAG, "InitCallback: onInitSuccess");

        if(activity == null)
            return;

        activity.setEnable();


    }
    @Override
    public void onInitFailure() {
        Log.e(TAG, "InitCallback: onInitFailure");
        if(activity == null)
            return;

        // don't enable if the init failed!
        //activity.setEnable();
    }


}
