package com.dev.game;

import android.os.Bundle;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.utils.Logs;

import org.json.JSONObject;
import org.json.JSONException;

import java.util.Locale;

/**
 * Created by jerrylootsie on 3/6/15.
 */
/**
 * This class sends the location at the begining , if location fix is not available
 * then it will register for location update and unregisters after update
 * @author raghunandank
 *
 */

public class AppLocationUpdates {

    private boolean locationSent;

    private static double latitude;
    private static double longitude;
    public static final int LOCATION_REQUEST_MS = 120000; // 2 minutes

    private static final String TAG = "game AppLocationUpdates";


    private Context appContext = null;

    //private static LocationListener locationListener = null;
    LocationManager lm = null;


    public AppLocationUpdates(Context inputAppContext)
    {
        Logs.d(TAG,"ApplicationUpdates");

        appContext = inputAppContext;
        locationSent = false;
        latitude = -1.0;
        longitude = -1.0;
    }

    /**
     * Returns the longitude and latitude in string format
     * @return
     */
    public  String getLocation() {
        if (locationSent == true) {
            return String.format(Locale.getDefault(), "%f,%f", latitude,
                    longitude);
        }
        return null;
    }


    /**
     *  Sends the Location to the Webapp asynchronously, once we got the fix
     */
    private  void sendLocation() {
        Logs.d(TAG,"sendLocation");

        Lootsie.setLocation((float) latitude, (float) longitude);

    }

    void getCurrentLocation()
    {
        Logs.d(TAG,"LocationUpdates: getCurrentLocation");

        boolean gps_enabled =false ,network_enabled =false;

        try {
            lm = (LocationManager) appContext
                    .getSystemService(Context.LOCATION_SERVICE);
            if(lm == null)
            {
                Logs.e(TAG,"LocationUpdates: LocationManager NULL , return");
                return;
            }

            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

            String provider = null;

            if(network_enabled == true )
            {
                provider = LocationManager.NETWORK_PROVIDER;
            }else  if(gps_enabled == true)
            {
                provider = LocationManager.GPS_PROVIDER;
            }
            else {
                Logs.e(TAG,"LocationUpdates: Location Service Providers are DISABLED");
                return;
            }

            // Get the fix and send it once ready
            getLocationFixes(provider);


        }catch (Exception e) {

            e.printStackTrace();
        }

    }

    /**
     * Get the Location fixes for the current provider. Registers with Location
     * provider and unregisters it after being sent.
     * @param provider
     */
    void getLocationFixes(String provider)
    {
        Logs.d(TAG,"LocationUpdates: getLocationFixes");

        final LocationListener locationListener = new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {
                longitude = location.getLongitude();
                latitude = location.getLatitude();

                Logs.d(TAG, "LocationListener: onLocationChanged: " + latitude + "," + longitude);

                // Send it to webapp
                sendLocation();

                // Unregister location updates
                removeUpdates();
                locationSent = true;
                lm = null;
            }

            @Override
            public void onStatusChanged(String provider, int status,
                                        Bundle extras) {
                Logs.d(TAG, "LocationListener: onStatusChanged");
            }

            @Override
            public void onProviderEnabled(String provider) {
                Logs.d(TAG, "LocationListener: onProviderEnabled");
            }

            @Override
            public void onProviderDisabled(String provider) {
                Logs.d(TAG, "LocationListener: onProviderDisabled");
            }
        };

        lm.requestLocationUpdates(provider,
                LOCATION_REQUEST_MS, 10, locationListener);
    }

    /*
     *  Unregister the location service once being sent
     */
    private void removeUpdates() {
        Logs.d(TAG,"LocationUpdates: removeUpdates");

        if(lm == null)
        {
            Logs.e(TAG,"removeUpdates: LocationManager NULL , return");
            return;
        }

        //Logs.e(TAG, "Location updates have been unregistered");
        //lm.removeUpdates(locationListener);
    }


}
